import math
import matplotlib
matplotlib.use('PDF')
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
import pylab
import re

fontsize = 7

pylab.rc("font", family="serif")
pylab.rc("font", size=fontsize)

width = 3.2
height = width/1.3

# pylab.rc("figure.subplot", left=(30/72.27)/width)
# pylab.rc("figure.subplot", right=(width-10/72.27)/width)
pylab.rc("figure.subplot", bottom=(25/72.27)/height)
# pylab.rc("figure.subplot", top=(height-20/72.27)/height)

matplotlib.rcParams['lines.linewidth']=0.5
matplotlib.rcParams['axes.linewidth']=0.5

import matplotlib.pyplot as plt

fig = plt.figure(figsize=(width, height))
ax1 = fig.add_subplot(111)

fout = open('resumetime-vs-dumpsize.csv', 'w')
for line in open('/var/tmp/resume.log') :
    duration_sec = re.match('.*duration_sec=(\d+\.\d+) .*', line).group(1)
    try :
        size_mb = re.match('.*dump_size_mb=(\d+).*', line).group(1)
        print >> fout, size_mb, duration_sec
    except AttributeError :
        pass
fout.close()

xs = []
ys1 = []
for line in open('resumetime-vs-dumpsize.csv', 'r') :
    cols = map(float, line.strip().split())
    xs.append(cols[0])
    ys1.append(cols[1])

plt.plot(xs, ys1, '.', color='#222222')

# http://matplotlib.sourceforge.net/mpl_examples/pylab_examples/spine_placement_demo.py
for loc, spine in ax1.spines.iteritems():
    if loc in ['left','bottom']:
        pass
    elif loc in ['right','top']:
        spine.set_color('none') # don't draw spine
    else:
        raise ValueError('unknown spine location: %s'%loc)

# turn off ticks where there is no spine
ax1.xaxis.set_ticks_position('bottom')
ax1.yaxis.set_ticks_position('left')

plt.xlabel('dump size [MB]')
plt.ylabel('resume time [seconds]')

plt.savefig('resumetime-vs-dumpsize')
