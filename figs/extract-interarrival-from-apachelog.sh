#!/bin/bash

function extractFromApacheLog {
    filepattern="$1"
    short="$2"

    cat $filepattern | cut -d'[' -f2 |cut -d' ' -f1 > access-times-$short-fulldate.csv
    python epoch.py access-times-$short-fulldate.csv > access-times-$short-epoch.csv
    sort -n < access-times-$short-epoch.csv > access-times-$short-epoch-sorted.csv
    awk 'BEGIN{prev=-1}{if (prev!=-1) {print $1-prev} ; prev=$1}' < access-times-$short-epoch-sorted.csv > interarrival-$short.csv
    sort -n < interarrival-$short.csv |uniq -c > interarrival-$short-hist.csv
}

extractFromApacheLog '../wwwse.inf.tu-dresden.de-access_log' wwwse
extractFromApacheLog '../aoterra-wiki-access-logs/ssl_access.log*' wiki

# cat wwwse.inf.tu-dresden.de-access_log | cut -d'[' -f2 |cut -d' ' -f1 > figs/access-times-wwwse-fulldate.csv
# python epoch.py access-times-wwwse-fulldate.csv > access-times-wwwse-epoch.csv
# sort -n < access-times-wwwse-epoch.csv > access-times-wwwse-epoch-sorted.csv
# awk 'BEGIN{prev=-1}{if (prev!=-1) {print $1-prev} ; prev=$1}' < access-times-wwwse-epoch-sorted.csv > interarrival-wwwse.csv
# sort -n < interarrival-wwwse.csv |uniq -c > interarrival-wwwse-hist.csv

# awk 'BEGIN{thresh=60;sum=0} {if ($2>60) {sum+=($2-thresh)*$1}} END{print sum, "secs", sum/60., "mins", sum/(60.*60), "hours", sum/(24.*60*60), "days"}' < interarrival-wwwse-hist.csv

FN="wwwse-thresh-downtime.csv"
rm $FN
for thresh in $(seq 60 60 480) ; do
    awk "BEGIN{thresh=$thresh;sum=0} {if (\$2>thresh) {sum+=(\$2-thresh)*\$1}} END{print thresh, sum}" < interarrival-wwwse-hist.csv >> $FN
done
