#!/usr/bin/env python2.7

import fileinput
import subprocess as sp
import time
import glob

# Split monolithic access log into day-size files. Output files can be
# used as input to client simulator.

# line_count = 0
# for line in fileinput.input():
#     line_count += 1
#     cur = time.strptime(line.strip(), "%d/%b/%Y:%H:%M:%S")
#     filename = time.strftime('%Y-%m-%d', cur) + '.csv'
#     print >> open(filename, 'a+'), \
#         time.strftime('%H:%M:%S', cur)

#     if (line_count % 10000) == 0 :
#         print line_count

# sp.check_call('for f in ????-??-??.csv ; do mv $f $f.unsorted ; sort $f.unsorted > $f ; done', shell=True)

for f in glob.glob('????-??-??.csv') :
    t_prev = 0
    filename_interarrival = f.replace('.', '-interarrival.')
    fout = open(filename_interarrival, 'w')
    for line in open(f, 'r') :
        line = line.strip()
        t = time.strptime(line, '%H:%M:%S')
        seconds_since_midnight = t.tm_hour * 60 * 60 + t.tm_min * 60 + t.tm_sec
        interarrival = seconds_since_midnight - t_prev
        print >> fout, interarrival
        t_prev = seconds_since_midnight
        
sp.check_call('for f in *-interarrival.csv ; do sort -n $f | uniq -c > $(echo $f | sed "s/interarrival/hist/") ; done', shell=True)

# The following bash script was a bad idea because it creates a shell
# pipeline for every input line. This overhead kills the raw speed of
# cut and tr.

#/usr/bash

# while read line ; do
#   fn=$(echo $line | cut -d'[' -f2 |cut -d' ' -f1 | cut -d':' -f1 | tr / -)
#   timeofday=$(echo $line | cut -d'[' -f2 |cut -d' ' -f1 | cut -d':' -f2-)
#   echo $timeofday >> "$fn.csv"
# done
