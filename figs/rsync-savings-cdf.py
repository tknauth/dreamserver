import collections
import math
import matplotlib
matplotlib.use('PDF')
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
import pylab
import re
import fileinput
import numpy as np
from pylab import *
import scipy.stats

fontsize = 7

pylab.rc("font", family="serif")
pylab.rc("font", size=fontsize)

width = (3.2+3.2)/3.
height = width/1.3

pylab.rc("figure.subplot", left=(30/72.27)/width)
pylab.rc("figure.subplot", right=(width-10/72.27)/width)
pylab.rc("figure.subplot", bottom=(25/72.27)/height)
# pylab.rc("figure.subplot", top=(height-20/72.27)/height)

matplotlib.rcParams['lines.linewidth']=0.5
matplotlib.rcParams['axes.linewidth']=0.5

import matplotlib.pyplot as plt

fig = plt.figure(figsize=(width, height))
ax1 = fig.add_subplot(111)

fout = open('rsync-savings-cdf.csv', 'w')
for line in open('../rsync.log') :
    sent_bytes = int(re.match('.*sent_bytes=(\d+) .*', line).group(1))
    size_bytes = int(re.match('.*size_bytes=(\d+).*', line).group(1))
    fraction = sent_bytes/float(size_bytes)
    # Data has bugs where sent_bytes is greater than size bytes. This
    # should not happen, because the hdd and ram are in the megabyte
    # range.
    if fraction <= 1 :
        print >> fout, fraction
fout.close()

d = collections.defaultdict(float)
fractions = []
for line in open('rsync-savings-cdf.csv', 'r') :
    v = float(line.strip())
    d[v] += 1
    fractions.append(v)

print '80 percentile score:', scipy.stats.scoreatpercentile(fractions, 80)
print '0.2 score percentile:', scipy.stats.percentileofscore(fractions, 0.2)

xs = sort(d.keys())
ys1 = [d[x] for x in xs]
ys1 = np.array(ys1)
ys1 /= np.sum(ys1)
cys1 = np.cumsum(ys1)

plt.plot(xs, cys1, 'k--', color='#222222')

# http://matplotlib.sourceforge.net/mpl_examples/pylab_examples/spine_placement_demo.py
for loc, spine in ax1.spines.iteritems():
    if loc in ['left','bottom']:
        pass
    elif loc in ['right','top']:
        spine.set_color('none') # don't draw spine
    else:
        raise ValueError('unknown spine location: %s'%loc)

# turn off ticks where there is no spine
ax1.xaxis.set_ticks_position('bottom')
ax1.yaxis.set_ticks_position('left')

plt.xlabel('Sent data as fraction of total data')
plt.ylabel('CDF')

plt.savefig('rsync-savings-cdf')

# plot(X, Y)
# plot(X,CY,'r--')
