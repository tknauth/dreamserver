import collections
import math
import matplotlib
matplotlib.use('PDF')
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
import pylab
import re
import fileinput
import numpy as np
from pylab import *
import scipy.stats

fontsize = 7

pylab.rc("font", family="serif")
pylab.rc("font", size=fontsize)

width = (3.2+3.2)/3.
height = width/1.3

pylab.rc("figure.subplot", left=(30/72.27)/width)
pylab.rc("figure.subplot", right=(width-10/72.27)/width)
pylab.rc("figure.subplot", bottom=(25/72.27)/height)
# pylab.rc("figure.subplot", top=(height-20/72.27)/height)

matplotlib.rcParams['lines.linewidth']=0.5
matplotlib.rcParams['axes.linewidth']=0.5

import matplotlib.pyplot as plt

fig = plt.figure(figsize=(width, height))
ax1 = fig.add_subplot(111)
ax1.set_xscale('log')
tick_locations = [0.0001, 0.01, 1, 100, 10000]
# tick_labels = map(str, tick_locations)
# plt.xticks(tick_locations,
#           tick_labels)
ax1.set_xticks(tick_locations)

xs = []
for line in open('../rsync.log') :
    sent_bytes = int(re.match('.*sent_bytes=(\d+) .*', line).group(1))
    xs.append(sent_bytes/float(1024*1024))

xs.sort()
print xs[0:10], xs[-10:-1]

print '1 MB score:', scipy.stats.percentileofscore(xs, 1.0)
print '> 100 MB score:', 100-scipy.stats.percentileofscore(xs, 100.0)

d = collections.defaultdict(float)

for k in xs :
    d[k] += 1

xs = sort(d.keys())
ys1 = [d[x] for x in xs]
ys1 = np.array(ys1)
ys1 /= np.sum(ys1)
cys1 = np.cumsum(ys1)

plt.plot(xs, cys1, 'k--', color='#222222')

# http://matplotlib.sourceforge.net/mpl_examples/pylab_examples/spine_placement_demo.py
for loc, spine in ax1.spines.iteritems():
    if loc in ['left','bottom']:
        pass
    elif loc in ['right','top']:
        spine.set_color('none') # don't draw spine
    else:
        raise ValueError('unknown spine location: %s'%loc)

# turn off ticks where there is no spine
ax1.xaxis.set_ticks_position('bottom')
ax1.yaxis.set_ticks_position('left')

plt.xlabel('State transferred [MB]')
plt.ylabel('CDF')

plt.savefig('rsync-xfered-cdf')

# plot(X, Y)
# plot(X,CY,'r--')
