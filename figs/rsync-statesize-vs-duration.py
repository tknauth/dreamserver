import math
import matplotlib
matplotlib.use('PDF')
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
import pylab
import re
import fileinput
import numpy as np
from pylab import *

fontsize = 7

pylab.rc("font", family="serif")
pylab.rc("font", size=fontsize)

width = (3.2+3.2)/3.
height = width/1.3

pylab.rc("figure.subplot", left=(30/72.27)/width)
pylab.rc("figure.subplot", right=(width-10/72.27)/width)
pylab.rc("figure.subplot", bottom=(25/72.27)/height)
# pylab.rc("figure.subplot", top=(height-20/72.27)/height)

matplotlib.rcParams['lines.linewidth']=0.5
matplotlib.rcParams['axes.linewidth']=0.5

import matplotlib.pyplot as plt

fig = plt.figure(figsize=(width, height))
ax1 = fig.add_subplot(111)

fout = open('rsync-statesize-vs-duration.csv', 'w')
for line in open('../rsync.log') :
    try :
        duration_sec = int(re.match('.*duration_sec=(\d+).*', line).group(1))
        size_bytes = int(re.match('.*size_bytes=(\d+).*', line).group(1))
        size_gb = size_bytes / float(1024*1024*1024)
        print >> fout, size_gb, duration_sec
    except AttributeError :
        pass
fout.close()

xs = []
ys1 = []
for line in open('rsync-statesize-vs-duration.csv', 'r') :
    cols = map(float, line.strip().split())
    xs.append(cols[0])
    ys1.append(cols[1])

plt.plot(xs, ys1, '.', color='#222222')

# http://matplotlib.sourceforge.net/mpl_examples/pylab_examples/spine_placement_demo.py
for loc, spine in ax1.spines.iteritems():
    if loc in ['left','bottom']:
        pass
    elif loc in ['right','top']:
        spine.set_color('none') # don't draw spine
    else:
        raise ValueError('unknown spine location: %s'%loc)

# turn off ticks where there is no spine
ax1.xaxis.set_ticks_position('bottom')
ax1.yaxis.set_ticks_position('left')

plt.xlabel('State size [GB]')
plt.ylabel('Duration [seconds]')

plt.savefig('rsync-statesize-vs-duration')
