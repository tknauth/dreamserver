#!/bin/bash

URLS="ftp://ita.ee.lbl.gov/traces/clarknet_access_log_Aug28.gz \
ftp://ita.ee.lbl.gov/traces/clarknet_access_log_Sep4.gz \
ftp://ita.ee.lbl.gov/traces/usask_access_log.gz \
ftp://ita.ee.lbl.gov/traces/NASA_access_log_Jul95.gz \
ftp://ita.ee.lbl.gov/traces/NASA_access_log_Aug95.gz \
ftp://ita.ee.lbl.gov/traces/calgary_access_log.gz"

for f in $URLS ; do
    wget $f
done

for f in *.gz ; do gzip -d $f | python2.7 split.py | sort -n | awk '{if (prev) {print $1-prev} ; prev = $1}' > $(basename $f .gz)-interarrival ; done