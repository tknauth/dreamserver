#!/usr/bin/env python2.7

import fileinput
import subprocess as sp
import time
import sys

line_count = 0
lines_skipped = 0
prev = None
request_time = []
for line in fileinput.input():
    line_count += 1
    try :
        line = line.strip()
        date_string_begin = line.find('[')+1
        date_string_end   = date_string_begin + len('xx/xx/xxxx:xx:xx:xx')
        cur = time.strptime(line[date_string_begin:date_string_end+1], "%d/%b/%Y:%H:%M:%S")
        cur = time.mktime(cur)
    except ValueError as e :
        lines_skipped += 1
        print >> sys.stderr, '%d : %s'%(line_count, line)

    print cur

print >> sys.stderr, 'lines skipped: %d'%(lines_skipped)

# sp.check_call('for f in wwwse/* ; do mv $f $f.unsorted ; sort $f.unsorted > $f ; done', shell=True)

# The following bash script was a bad idea because it creates a shell
# pipeline for every input line. This overhead kills the raw speed of
# cut and tr.

#/usr/bash

# while read line ; do
#   fn=$(echo $line | cut -d'[' -f2 |cut -d' ' -f1 | cut -d':' -f1 | tr / -)
#   timeofday=$(echo $line | cut -d'[' -f2 |cut -d' ' -f1 | cut -d':' -f2-)
#   echo $timeofday >> "$fn.csv"
# done
