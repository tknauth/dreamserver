#!/bin/bash

function fail {
    rc="$?"
    if [ "$rc" != "0" ] ; then
        echo "failed with $rc"
        exit 1
    fi
}

cp mod_proxy_http.c /var/tmp/httpd-2.4.x-svn/modules/proxy || fail
make -C /var/tmp/httpd-2.4.x-svn/ || fail
cp /var/tmp/httpd-2.4.x-svn/modules/proxy/.libs/mod_proxy_http.so /tmp/apache/modules || fail

