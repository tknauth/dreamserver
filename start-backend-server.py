#!/usr/bin/env python2.7

import time
import subprocess as sp
import shlex
import sys

def start_server(domain, port) :
    target_host = 'ssh 192.168.2.2'
    cmd = "%s ls -la /var/tmp/%s-dump"%(target_host, domain)
    rc = sp.call(shlex.split(cmd))
    action = 'restore'
    begin = None
    dump_size_mb = 0

    if rc == 0 :
        # file exists; can restore from dump
        dump_filename = '/var/tmp/%s-dump'%(domain)
        cmd = '%s stat --format "%%s" %s'%(target_host, dump_filename)
        dump_size_byte = int(sp.check_output(shlex.split(cmd)).strip())
        dump_size_mb = int(round(dump_size_byte / (1024*1024)))
        
        begin = time.time()
        cmd = '%s virsh restore /var/tmp/%s-dump'%(target_host, domain)
        sp.check_call(shlex.split(cmd))

    else :
        # file does not exist ; start vm from scratch
        begin = time.time()
        action = 'start'
        cmd = '%s virsh start %s'%(target_host, domain)
        sp.check_call(shlex.split(cmd))

    server_up = False
    while not server_up :
        cmd = 'nc -w 1 -z %s %s'%(domain, port)
        rc = sp.call(shlex.split(cmd))
        if rc == 0 : server_up = True
        else : time.sleep(0.1)
    end = time.time()

    print >> open('/var/tmp/resume.log', 'a+'), \
        'epoch_time=%s'%(time.time()), \
        'local_time=%s'%(time.strftime('%Y-%m-%d:%H:%M:%S')), \
        'action=%s'%(action), \
        'duration_sec=%.4f'%(end-begin), \
        'dump_size_mb=%d'%(dump_size_mb), \
        'domain=%s'%(domain), \
        'port=%s'%(port)

if __name__ == '__main__' :
    start_server(sys.argv[1], sys.argv[2])
