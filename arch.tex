\begin{figure*}[t!]
  \centering
  \includegraphics{figs/arch-2}
\caption{System architecture.}
\label{fig:arch}
\end{figure*}

\section{Architecture}
\label{sec:arch}

The following section describes the {\sysname} architecture as well as the larger, global architecture of where we intend to deploy a system like {\sysname}.

%% \subsection{Zookeeper}

%% Zookeeper~\cite{hunt2010zookeeper},
%% originally developed by Yahoo, and now maintained by the Apache Foundation,
%% is a distributed coordination service.
%% To handle the computational challenges of modern times,
%% IT systems are distributed more often than not.
%% With distribution across many servers,
%% the need for coordinating the various pieces of soft- and hardware arises.
%% Instead of re-implementing the basic coordination primitives in an ad-hoc way for each new system,
%% Zookeeper exposes a library which can be used instead.
%% Clients manipulate a hierarchical name space and store small pieces of information within the name space.
%% The consistency model offered by Zookeeper,
%% allows to implement distributed locks, leader election, and barriers to coordinate processes in a distributed system.

\subsection{Local architecture}

{\sysname}'s initial focus is on transactional, web-based workloads.
With transactional we refer to request-and-response-style interactions between clients and a server.
This type of workload makes it easy to detect when the system is in use.
Whenever a request is processed, the system is busy.
After a request is processed and an answer has been sent out, the system is idle.
Whenever the system is idle, it can potentially be shut off without disrupting the service it is providing.
If a system is shut off, and a new request is arriving, the system must brought back online to process the request.
Figure~\ref{fig:arch} illustrates the basic concept.

The system providing a service in our scenario is a single server.
It can be either a physical or a virtual server.
Virtual machines have the benefit over being easier to deploy.
Also, multiple virtual machines can share a single physical server, e.g., to improve utilization.
For our investigation, we focus exclusively on web services deployed as a single virtual machine.
Extending {\sysname}'s concept to multi-tier web services is left for future work.

The scenario where we envision a system like {\sysname} to be deployed is a data center providing web hosting services to multiple customers.
Each customer has access to a virtual server.
The virtual server can be setup with any software stack the customer desires.
This is one benefit over, for example, platforms like Google App Engine or Amazon's BeanStalk:
the customer has full control over the stack and is not locked into the provider's platform.
Once everything is set up, the virtual machine enters its ``normal'' operational mode.
Requests are received and answers are sent over HTTP.

As is often the case with shared hosting providers,
multiple customers share the same IP address for their domain.
Resolution of which backend server to send the HTTP request to is done using the host header inside the HTTP request.
A single proxy will operate as the ``resolver'':
forwarding requests based on the host header to the appropriate backend server.
The setup involving a reverse proxy is one technique to combat the exhaustion of IPv4 addresses.
Resolving the destination for a request at the application layer (i.e., HTTP) is a convenient way to work around the lack of publicly available IPv4 addresses.
Once the backend server is done processing the request,
it sends the reply to the proxy which relays it to the client.

The proxy is essential to {\sysname} because the proxy is where requests are inspected.
Requests are not just forwarded by the proxy:
it additionally keeps track of which backend servers are on or off.
Before forwarding a request,
the proxy first determines whether the backend server is online.
If so, the proxy forwards the requests as usual.
Otherwise, the proxy wakes the backend, waits for the backend to become ready, and only then forwards the request.

The initial {\sysname} implementation focuses on the application level protocol HTTP.
The principles behind {\sysname} are, however, also applicable to other protocols and layers.
Any request-response style interaction can be interposed.
The difficulty lies in the engineering part.
Implementing the equivalent functionality for arbitrary TCP sessions, for example,
would increase the potential use cases.
The backend server would be woken, whenever a TCP session is established.
Once all TCP sessions to the backend are closed,
it would be shut down again.
We focus on HTTP as it offers a good trade-off between engineering effort and applicability of {\sysname}.

%% We use HTTP requests in this work to demonstrate the viability of our approach.
%% It is in no way restricted to only work with HTTP requests.
%% Any request-response style interaction can be interposed in some way or another.
%% The same principle of proxying requests can be done for other application-layer protocols or for protocols on lower levels of the ISO/OSI stack.
%% However,
%% proxying, for example, TCP sessions is more difficult in the engineering part.
%% Of course, having a similar system work for arbitrary TCP sessions also makes the system more widely applicable.
%% We have chosen an architecture which proxies HTTP requests because it eases the engineering aspect as well as the current trend to expose more and more services via RESTful HTTP APIs.

%% web server architecture: threaded vs event driven; Apache most
%% prominent threaded web server; one thread per connection; has
%% scalability problems; nginx represents an event driven web server;
%% single thread and non-blocking operations; the threaded or
%% event-driven nature of the web server influences the implementation;
%% if the server is event driven, we cannot stall the processing of a
%% request until the origin server is online again; this would completely
%% halt the processing of any other incoming requests during the wait
%% time; instead, the request would need to be put into a queue; the
%% wakeup of the origin server initiated; once the origin server finished
%% waking up, it would send an event to the reverse proxy; this reverse
%% proxy's event loop would handle the event by inspecting the queue of
%% suspended requests; if requests to the woken up origin server are
%% queued, they would then be forwarded to the respective server;

%% The system architecture is shown in Figure~\ref{fig:arch}.
%% Clients issue requests over HTTP.
%% Requests are sent to a (load-balancing) proxy server.
%% The proxy server enables us to detect which back-end servers are idle and which are busy.
%% It is also a convenient software component to extend because it already contains all the logic to process HTTP requests.
%% We use HTTP requests in this work to demonstrate the viability of our approach.
%% It is in no way restricted to only work with HTTP requests.
%% Any request-response style interaction can be interposed in some way or another.
%% The difficulty lies in the engineering part.
%% The same principle of proxying requests can be done for other application-layer protocols or for protocols on lower levels of the ISO/OSI stack.
%% However,
%% proxying, for example, TCP sessions is more difficult in the engineering part.
%% Of course, having a similar system work for arbitrary TCP sessions also makes the system more widely applicable.
%% We have chosen an architecture which proxies HTTP requests because it eases the engineering aspect as well as the current trend to expose more and more services via RESTful HTTP APIs.

%% The reverse proxy about the power state of the back-end servers.
%% If the back-end server for the current request is on,
%% just forward the requests as usual.
%% If the back-end is suspended,
%% resume the back-end before forwarding the request. 

\begin{figure*}
  \centering
  \includegraphics[width=1.0\textwidth]{figs/global-arch-2}
\caption{Global architecture.}
\label{fig:global-arch}
\end{figure*}

\subsection{When to suspend?}

When to suspend an idle virtual machine is determined by two important factors:
(1) what is the expected resume/suspend delay?
In the worst case scenario,
a request arrives shortly after a suspend action is triggered.
Because suspending a virtual machine cannot be aborted,
the request will be delayed for however long it takes to suspend and resume the virtual machine.
Granted, resuming straight after a suspend will be fast, because all relevant pages will still be in the host's page cache.
But, as we will see in the evaluation, suspending a virtual machine can take up to one minute.
(2) Although worst case response latencies are one important factor,
we argue that 99th percentile or 95th percentile latencies are more relevant.
Service level agreements often specify a target for the $n$th percentile response latency.
If the trade-off between the number of suspend/resume cycles is chosen wisely,
$n$th percentile latency will not change.
The maximum number of suspend/resume cycles which can be performed without impacting $n$th percentile latency,
can be calculated using the total number of requests served.
For example, serving 10000 requests per day,
a maximum of $0.01 * 10000 = 100$ requests may exhibit latencies above the 99th percentile threshold.
Knowing the number of served requests and the number of $n$th percentile violations,
this information can be integrated into the decision making mechanism of when to suspend the virtual machine.
If $n$th percentile latency is of no concern,
a more aggressive strategy can be applied:
always suspend after $s$ seconds of inactivity.
This strategy will maximize the suspend time,
in exchange for higher average response latencies.

\subsection{Fault-tolerance}

Surviving data center downtimes is an important selling point when it comes to service availability.
As demonstrated most recently with hurricane Sandy,
downtimes do happen and can last for several days.
Our strategy to provide high availability for virtual machines in the face of data center downtimes is to periodically copy the virtual machine's state to a remote, backup location.
A checkpoint is created by {\sysname} every time the virtual machine is suspended.
This checkpoint is copied to the backup location.
Because virtual machine checkpoints can be multiple gigabytes in size,
we only copy the changed state.
As we will see in the evaluation,
delta transfers significantly reduce the size of the data that must be transferred over the network.

We propose to use DNS updates to point clients to the backup location if a fail-over is necessary.
Figure~\ref{fig:global-arch} gives a visual impression of how {\sysname} deals with data center downtimes.
Because the backup data center has a copy of the virtual machine,
as soon as the outage of the original data center is detected,
the DNS records for the virtual machine are updated.
The new DNS entry will point to the IP of the reverse proxy for the backup site.
Future requests for the virtual machine will be sent to the backup data center.
The handling of requests in the backup data center is identical to the original data center.
If the backend server responsible for the request is sleeping,
it is resumed.
This will initially be the case for all failed-over virtual machines.

\subsection{Implementation}

We modified the Apache's proxy module, \emph{mod\_proxy\_html}, to implement our {\sysname} functionality.
Besides the Apache modifications, we also implemented two daemons vital to the operation of {\sysname}.
Our first implementation attempt involved Zookeeper,
the distributed coordination service,
to synchronize {\sysname}'s components.
We soon realized, however, that Zookeeper is the wrong tool in our case.
The Zookeeper workload would be write-dominated and incur multiple network round-trips \emph{on each HTTP request}.
We thus abandoned Zookeeper in favor of a simple locking scheme.
The processes we need to coordinate all run on a single machine and the state held by them can easily be recovered after a crash.
Distributed coordination and persistence would have been arguments in favor of Zookeeper.

Analogous to our initial Zookeeper attempt,
all information relevant to the correct operation of {\sysname} is kept as part of the host's file system.
The information is structured hierarchically.
Under a user-defined root node, r, there exist three files for each backend server:
(1) a lock file, named \texttt{backend-lock}, mediating state transitions of the backend server.
(2) A file, named \texttt{backend-on}, signaling whether the backend server is on (file exists) or off (file is absent).
(3) A file serving as the critical section counter, named \texttt{backend-critsec-count}.
The \texttt{backend} part of each file is replaced by a string uniquely identifying each backend server.
Currently, we use the backend server's private IP address and HTTP port pair.
For example, the lock file will be named \texttt{192.168.2.101:80-lock}.

The critical section counter is required to allow multiple requests,
destined for the same backend server,
to be processed in parallel.
Instead of just having a plain lock,
which is held as long as the backend server is processing a request,
we emulate a readers/writers lock.
Multiple requests can be processed in parallel, i.e.,
enter the critical section simultaneously.
Those are the ``readers''.
The role of the writer is played by the process suspending/resuming the backend server.
Whenever the backend server is performing a state transition,
either resuming or suspending,
no requests may be forwarded to it.
The process responsible for the state transition holds the exclusive writer's lock,
preventing any reader from entering the critical section.

Implementing the locking scheme on top of the file system relies on the atomicity of the ``create file'' operation.
A process acquires the lock by attempting to create the lock file.
If the operation succeeds, the lock acquisition was successful.
If the operation fails, the process has to retry.
We use a simple sleep and retry strategy to resolve contention.
Also, we have not found a performance impact so far by relying on the file system for our locking scheme.
If this turns out to be a problem in the future,
a quick fix is to place the root directory on a RAM disk.
Alternatively, process synchronization can be re-implemented using shared memory synchronization facilities.

Halting the request processing by locking is only possible because Apache is a threaded web server.
Each connection is served by a separate process or thread.
The one to one relationship between requests and threads is convenient for us,
because halting the processing of one request does not halt the processing of other, concurrent requests.
This would not have been possible in an event-driven server.
Implementing {\sysname} within an event-driven web server, e.g., nginx,
is possible but requires additional engineering effort.

{\sysname} is constructed in a way that there is no hard state.
This allows for easy bootstrapping and crash recovery.
If {\sysname} does not know about the state of the backend server (is it on or off),
it sends a probe to the backend.
It tries to establish a TCP session on the HTTP port.
If the session can be established,
the server is alive.
If no session can be established,
the server is assumed to be shut off.

Figure~\ref{lst:apache} summarizes the logic added to Apache's proxy module with pseudo code.
The actual implementation consists of less than 300 lines of C code.

\begin{lstlisting}[caption={Reverse proxy modification},label={lst:apache}]
enter_cs = False

# enter critical section
while enter_cs == False :
  grab_lock('/backend-lock')
  v = read('/backend-critsec-count')
  if v >= 0 : v++; enter_cs = True
  else release_lock('backend-lock')

write(v, 'backend-critsec-count')

alive = False
if bootstrap_backend() :
  alive = ping_backend()
else :
  alive = exists('/backend-on')


if not alive :
  start_and_wait_for_backend()

release_lock('backend-lock')

forward_request()

# leave critical section
grab_lock('/backend-lock')
v = read('/backend-critsec-count')
write(v-1, '/backend-critsec-count')
release_lock('backend-lock')

\end{lstlisting}

\subsection{Daemons}

In addition to the Apache server,
two more processes are part of {\sysname}.
One process, called \emph{suspender}, implements the policy for suspending idle virtual machines.
A second process, called \emph{synchronizer}, is responsible for the virtual machine state synchronization.
Our modifications to the Apache server only allow it to wake up backend servers in case they are suspended.
Implementing the suspend logic within Apache itself is possible,
but would require an additional thread of execution unrelated to the HTTP processing.
This is why we decided to implement the suspend and synchronization facilities outside of Apache.
This separation of concerns allows us to change the suspension policy without modifying or restarting the Apache proxy.
We are also freed of the burden to use C for our programming efforts.

\emph{suspender} periodically checks the modifications times of the file \texttt{backend-critsec-count}.
If the modification time is older than some predefined threshold the backend server is suspended.
The server is suspended by issuing the \emph{save} command through virsh.
virsh is the shell component of libvirt.
libvirt, in turn, provides a unified interface to interact with different hypervisors.
\emph{suspender} logs the resulting memory dump size as well as the time it takes to suspend the instance.

\emph{synchronizer} decouples the state synchronization aspect of {\sysname} from the suspend/resume functionality provided by the proxy.
It relies on the \emph{inotify} kernel subsystem,
allowing user-space processes to be notified of file system state changes.
In our case synchronizer watches for delete events on the \texttt{/backend-on} file.
The deletion of this file is the signal that the backend server was shut down.
Whenever the backend is down,
the virtual machine's state is eligible for synchronization.
The current implementation synchronizes the memory and disk state after every suspend.
Other policies (dynamic or static) can easily be implemented by adapting the synchronizer.

%% Each server has two Zookeeper nodes:
%% a lock node and a status node.
%% The lock node ensures mutual exclusion during status information updates.
%% Before updating the status information, the lock must be acquired.
%% Once the reverse proxy acquired the lock,
%% it probes whether the back-end server is running by trying to create a node \verb+/ip:port/on+.
%% If the creation succeeds,
%% the back-end server is not running and must be started before forwarding the request.
%% Otherwise,
%% the back-end server is already running.
%% At last, the current time is written to the node \verb+/ip:port+.
%% The time stamp is used by another Zookeeper client to determine when to shut down back-end servers.

%% In parallel, a second process,
%% responsible for shutting down back-end servers,
%% performs the following steps:
%% the functionality is split into two parts: a start-up part and a steady-state phase.
%% At start up,
%% the suspender registers a watch for the node \verb+/backends+.
%% Every back-end server participating in the system will register itself under this node.
%% Thus, the suspender is notified whenever a back-end server enters or leaves.
%% When a new back-end server enters the system,
%% suspender starts a timer which fires after XX minutes -- the idle interval timer.
%% The idle interval is the maximum allowed time of inactivity before a back-end server is shut down.
%% If a new request is processed by the server,
%% the timer will be reset.
%% This is achieved by setting a watch for each of the child nodes at \verb+/backends+.
%% The reverse proxy updates each child node with the current time before forwarding a request to the back-end server.

%% When a timer expires,
%% the suspender tries to acquire the back-end server lock.
%% It does so by executing \verb+create('/ip:port-lock')+.
%% If lock acquisition succeeds,
%% it shuts down the back-end server.
%% This change is reflected in Zookeeper by deleting the node \verb+/backends/ip:port/on+.
%% At last,
%% the lock is released.
%% If lock acquisition fails,
%% we assume the back-end server is currently processing a request,
%% and only rest the timer.

%% \subsection{Optimization}

%% Achieving distributed coordination does not come for free of course.
%% For each request,
%% we perform at three Zookeeper updates:
%% acquiring the lock,
%% writing the time stamp,
%% releasing the lock.
%% This does not scale with increasing numbers of requests.
%% Hence, we implemented a ``low-pass filter''.
%% Instead of updating the time stamp for every request,
%% the time stamp is updated periodically.
%% We trade accuracy for improved scalability.
%% The number of Zookeeper updates are decoupled from the number of application layer (e.g., HTTP) requests.
%% Only one Zookeeper update per period is required,
%% irrespective of the actual number of requests.
%% The limiting factor then becomes the number of back-end services Zookeeper is responsible for.

%% We may, of course, lose accuracy in the presence of failures.
%% Because the reverse proxy only updates Zookeeper once every period,
%% the information about which back-end server answered a requests during the current period is lost,
%% if the reverse proxy crashes.
%% As a consequence,
%% back-end servers may be shut down prematurely,
%% even though they have not yet been inactive for long enough to warrant shut down.
%% However, as crashes are rare, the rate of premature shutdowns is low.

%% The modifications to Apache are minimal with XXX lines of code.
%% Similarly,
%% the Python script to shutdown back-end servers at times of inactivity only has XXX lines of code.

\subsection{Resume and suspend times}

Suspend and resume times are important for two reasons:
first, they limit the exploitable idle intervals.
With sufficiently fast suspend/resume cycles,
we are able to harness even idle intervals in the  millisecond range.
If suspend/resume cycles are in the order of seconds, however,
idle times must also last for multiple seconds.

Mobile computing seems to also lead the way with respect to fast suspend/resume cycles.
Current generation laptops (e.g., MacBook Air) can resume from sleep in about 2 seconds.
Modern server and consumer grade PCs are not particularly tuned for fast resume/suspend.
Cold booting a machine easily takes 30 seconds and more.
Peripheral hardware and controllers must be initialized,
buses scanned for attached devices,
all contributing to the delay.
We are convinced though,
that there is ample potential to reduce boot times drastically~\cite{wright2011vision}.
Hardware changes, in consumer and the enterprise settings, are rare.
For example, scanning for added or removed peripheral devices is useless during most boots.
Not being able to customize the hardware (e.g., as with the MacBook Air) may even be a good thing with respect to predictability at boot time.
The system designers can assume the hardware configuration will never change,
and take shortcuts at boot time to improve the overall responsiveness of the system.
Improvements made in this area,
will be beneficial to all approaches transitioning servers between on and off states.

Research has shown that there is a correlation between response times and number of page views.
If a web site offers fast response times,
users interact with the site more frequently,
potentially allowing the web site owner to generate more income.
Hence, there is a viable business interest to achieve fast response times.
Search engines also incorporate response times into their ranking algorithm,
penalizing slow web sites by assigning a lower rank to them.

This study focuses exclusively on virtual machines.
The speed of resuming a virtual machines is dominated by the disk I/O bandwidth and can be as fast as couple of seconds (less than 10).
The time to suspend a virtual machine depends on the machine's used memory.
The process of writing the memory content to disk is CPU-bound.
The achievable suspend bandwidth is between 20-30 MB/s.
