\section{Background}

\begin{figure*}
   \includegraphics[width=\linewidth]{figs/access-times.pdf}
   \caption{Request pattern for three different services: a GIT source code repository, a Wiki, and our department web server. The GIT and Wiki traces have visible idle periods outright. The case is not obvious though for the web server.}\label{fig:access-frequency}
\end{figure*}

Our work lies at the confluence of four distinct changes in the IT landscape:

First is the proliferation of web services with Amazon as a prime example of the trend.
Services are increasingly offered over HTTP via REST(-like) interfaces.
The popularity of cloud computing and the re-emergence of centralized IT infrastructures are contributing to the growth of web services.
Offering access to services via HTTP has the benefit of building on a reliable and proved infrastructure.
The fundamental building blocks for HTTP and the web in general have matured for more than 20 years now.
Capitalizing on this solid foundation makes sense.

Second, cloud computing is taking over the computing industry in a storm.
The basic idea is to lease IT resource on-demand.
Customers gain flexibility in the face of uncertain changes in popularity.
No large cash investments into IT infrastructure must be made upfront.
The central administration of IT infrastructure leads to economies of scale which previously were only available to a select few big players.
With the advent of cloud computing even small to medium size enterprises can enjoy the same benefits as big companies.

Third, energy-aware and energy-proportional computing are hot research topics at the moment too.
The HPC community now adopted a power-envelope of 20~MW for new installations.
Traditional data centers improve their best practices to decrease their power utilization effectiveness~(PUE).
PUE is a measure for how much energy is consumed by the computing equipment in comparison to the total power consumption.
An ideal PUE value of 1 means there is no ``waste'': all the energy is consumed by the IT infrastructure.
The average value for data centers is about 1.8, with modern data centers achieving values of around 1.2~\cite{barroso2009datacenter}.

Fourth, virtualization enables multiple parties to share the same physical infrastructure.
Isolating the different workloads is essential for security and performance.
In recent years, virtualization technologies, e.g., Xen, VMWare, kvm, and HyperV,
have gone mainstream.
With virtualization many instances of potentially different operating systems can run time-shared on the same physical infrastructure.
An added benefit of virtualization is the ability to move virtual machine instances between physical machines.
By migrating the entire operating system stack,
there are no residual dependencies to the source machine like there used to be with process migration.
One application of migration is to vacate the host and shut it down at times of ebbing demand.

To summarize, (1)~a growing number of services are offered over HTTP/REST APIs (2)~compounded by the popularity of cloud computing.
(3)~Energy consumption and energy-proportionality are of concern to computing providers and users because the associated costs are increasing.
(4)~Virtualization is mainstream and the features it brings are valuable building blocks for data center operators~\cite{vogels2008beyond}.

To the best of our knowledge, {\sysname}, is the first \emph{practical} system aimed at suspending and resuming compute resources on demand (compared with, e.g., \cite{meisner2011power, meisner2009powernap}).
We have evaluated {\sysname} extensively in our laboratory.
During the evaluation period,
hundreds of virtual machine instances were suspended, resumed, and their state synchronized to a backup location.
As a result of the continued suspend and resume, hundreds of virtual machine hours were saved.
{\sysname}'s operation is completely transparent to the clients.
Occasionally, clients experience longer-than-usual latencies for their first response,
if the backend server was sleeping.
This is the cost at which {\sysname} enables resource-conservation at the operator's site.
We will show that,
allowing for a little flexibility with respect to response latencies,
virtual machines can be turned off for 10 hours and more per day.
If the trade-off is made sensibly,
it may not even change the 99th percentile latency of the overall service.
This is noteworthy because service level agreements often use 99th percentile response times to signify the responsibility of the provider.

Our work is motivated by the fact that data centers are idle most of the time~\cite{barroso2009datacenter}.
For desktop systems, implementations of consolidation schemes using virtualization already exist~\cite{agarwal2010sleepserver, bila2012jettison}.
Similar schemes for the data center are, however, missing.
One explanation may be the believe, that there is nothing to be gained from applying similar tactics at the data center.
A recent study concluded, that simple power on/power off schemes will not work for Google-style workloads~\cite{meisner2011power}.
However, we argue that Google-style workloads are only one part of the picture.
Another recent study, analyzing a cluster workload trace, concluded that overall resource utilization is dominated by a minority of jobs (``boulders''),
whereas the majority of jobs (75\%) contribute very little (``sand'')~\cite{reiss2012trace}.
This can be seen as an argument for our hypothesis,
that there is indeed something to be gained from temporarily switching off unused virtual machines.
The majority of those virtual machines will contribute little to the overall utilization, i.e.,
be idle for most of the time.

To support our standpoint we begin by presenting three utilization traces of exemplary web services.
Figure~\ref{fig:access-frequency} shows the access frequency of three web services.
They serve as the motivating example for our work.
The sample includes a GIT code hosting repository, a Wiki, and our group's web server.
The GIT data was collected between Sep 16 to Oct 14 2012,
the Wiki data between Jan 16 to Oct 14 2012,
and the group's website~\footnote{\censor{\url{http://wwwse.inf.tu-dresden.de/}}} data dates from May 7 2007 to Mar 17 2008.
We do not have more recent data because we stopped managing and running the web server ourselves.
Instead, it is now operated by the university's IT department.
As is typical with web services all three show variation in their access frequency over the course of a day.
For the GIT and wiki services it can be visually determined that there exist hour-long periods of complete inactivity.
The services are used by a small, geographically co-located team of developers.
Periods of high activity coincide with ``normal'' business hours.
The case of our group's web server is less clear-cut.
The aggregated data shows a diurnal pattern,
but there are no apparent hours of total inactivity as is the case for the other two services.

Looking at the data in this aggregated form, however,
presents a false impression that there is nothing to be gained.
What we are actually interested in,
and is hidden in Figure~\ref{fig:access-frequency},
is the inter-arrival time between requests.
The inter-arrival time for the same data set is shown in Figure~\ref{fig:interarrival-cdf}.
We observe inter-arrival times of seconds up to several minutes for each service.

The potency of our proposed approach to switch off services during idle times becomes apparent in the next figure.
We consider a timeout value of one minute and focus exclusively on the department web server.
If the inter-arrival is larger than 60 seconds,
we transition the server to a state where it does not actively consume any resource.
The results are presented in Figure~\ref{fig:wwwse-thresh-downtime}.
Accumulating the off time over the whole 319 day period,
the server could have been offline for more than 140 days.
As we increase the threshold, say, to 2 minutes, the off time decreases to around 80 days.
The relationship is not linear though,
indicating that inter-arrival times are not equally distributed.
While our group's web site is by no means the busiest in the world,
we argue that it is a typical example and stands representative for a plurality of sites deployed and running in today's data centers.

At last, to show that the seemingly high degree of inactivity is not the result of a freak multi-day network outage,
we also plot the daily inactivity time in Figure~\ref{fig:downtime-per-day-thresh}.
Consistent with the data for the whole period,
using a 60 second idle timeout,
the server could have been switched off for about 11 hours each day (on average).

\begin{figure}
   \includegraphics{figs/interarrival-cdf}
   \caption{Distribution of inter-arrival times plotted as a cumulative distribution function. X-axis scale is logarithmic. Each service has inter-arrival times in the seconds to minutes range.}
   \label{fig:interarrival-cdf}
\end{figure}

\begin{figure}
   \includegraphics{figs/wwwse-thresh-downtime}
   \caption{Potential of inactivity-based power-saving technique.
     Accumulated days of inactivity in relation to timeout values.
   Frequent, minute-long idle-periods allow for significant savings.}
   \label{fig:wwwse-thresh-downtime}
\end{figure}

%% Application parking in itself is not a new concept.
%% Werner Vogels referred to application parking in his 2008 ACM Queue article~\cite{vogels2008beyond}.
%% In the article application parking is one use case enabled by the widespread adoption of virtualization technology.
%% During times of inactivity virtualized applications are explicitly put on hold.
%% The Decommissioning frees up resources, most importantly RAM, which would otherwise remain occupied but unused.
%% Among the main compute resources, RAM, CPU, disk, and I/O bandwidth,

%% \begin{figure*}
%%   \begin{tabular}{cc}
%%     \begin{minipage}[t]{0.5\textwidth}
%%       \includegraphics{figs/wwwse-thresh-downtime}
%%       \caption{Potential of inactivity-based power-saving technique.
%%         Accumulated days of inactivity in relation to timeout values.
%%         Frequent, minute-long idle-periods allow for significant savings.}
%%       \label{fig:wwwse-thresh-downtime}
%%     \end{minipage}
%%     &
%%     \begin{minipage}[t]{0.5\textwidth}
%%       \includegraphics{figs/downtime-per-day-thresh}
%%       \caption{Accumulated potential downtime on a per-day basis for timeout values of 60, 120, and 240 seconds.
%%         Switching the server off after 60 seconds yields around 11 hours of downtime per day (on average).}
%%       \label{fig:downtime-per-day-thresh}
%%     \end{minipage}
%%   \end{tabular}
%% \end{figure*}

%% Virtual machine hours is the metric we use to measure the success of technique.
%% It is the billing unit of all infrastructure cloud providers we know of.
%% Active and passive virtual machine time.
%% Active is when the VM is actually running.
%% Passive, on the other hand, is the time when the VM is parked and not consuming any RAM, CPU, and I/O resources.
%% Resources are typically accounted for on a time basis.
%% There is no difference with respect to incurred costs between an idle and a fully utilized instance.
%% It is in the customer's interest to maximize the utilization of the rented resources.
%% Accounting on the basis of actual consumption has been proposed~\cite{ibrahim2011towards} but is used only for some resources such as in-/outbound network traffic.

%% Applications for load-balancing are also plentiful.
%% Virtual machines can be load-balanced between different servers in the same rack or data center, e.g.,
%% to alleviate data center hot spots.
%% Or, the virtual machines can be load-balanced between data centers.
%% If there are two data centers, one of which is slightly more than 50\% utilized, and the second data center slightly less than 50\%,
%% shifting the load may enable both data centers to shut down operation for half of its servers.
%% Switching of entire sections of a data center saves more energy than is consumed by the servers because network equipment and cooling facilities for the entire section can be disabled as well.

\begin{figure}
   \includegraphics{figs/downtime-per-day-thresh.pdf}
   \caption{Accumulated potential downtime on a per-day basis for timeout values of 60, 120, and 240 seconds.
   Switching the server off after 60 seconds yields around 11 hours of downtime per day (on average).}
   \label{fig:downtime-per-day-thresh}
\end{figure}
