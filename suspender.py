#!/usr/bin/env python2.7

import errno
import shlex
import subprocess
import glob
import os
import sys
import time

# If modification time of /tmp/192.168.2.*-critsec-count is older than
# some threshold value, suspend the virtual machine instance to disk.

timeout_sec = 60.0

def kill_process(domain, port) :
  cmd = 'pkill -f %s:%d'%(domain, port)
  subprocess.call(shlex.split(cmd))

def kill_vm(domain, port) :
  cmd = 'virsh shutdown %s'%(domain)
  subprocess.call(shlex.split(cmd))

def save_vm(domain, port) :
  begin = time.time()
  dump_filename = '/var/tmp/%s-dump'%(domain)
  cmd = 'ssh 192.168.2.2 virsh save %s %s'%(domain, dump_filename)
  subprocess.call(shlex.split(cmd))
  end = time.time()
  cmd = 'ssh 192.168.2.2 ls -la %s'%(dump_filename)
  rv = subprocess.check_output(shlex.split(cmd))
  dump_size_byte = int(rv.split()[4])
  dump_size_mb = int(round(dump_size_byte / (1024*1024)))
  print >> open('suspender.log', 'a+'), time.time(), time.strftime('%Y-%m-%d:%H:%M:%S'), \
      'save_vm', 'domain=%s'%domain, 'port=%s'%port, 'duration_sec=%.4f'%(end-begin), \
      'dump_size_mb=%d'%(dump_size_mb)

# @return True, if lock acquisition successful. False, otherwise.
def try_lock(dir) :
  try :
    fd = os.open(dir+'-lock', os.O_CREAT|os.O_EXCL)
    os.close(fd)
    return True
  except OSError as e :
    print 'dir', dir, 'e', e
    if e.filename.endswith('-lock') : return False
    assert False, "unreachable"

def unlock(dir) :
  os.remove(dir+'-lock')

# @return True, if entered critical section. False, otherwise.
def try_enter_critical_section(dir) :
  if not try_lock(dir) : return False
  m = 0

  f = open(dir+'-critsec-count', 'w+')
  line = f.readline()
  if len(line) > 0 :
    m = int(line.strip())

  if m == 0 :
    print >> f, -1, 

  f.close()
  unlock(dir)
  return (m == 0)

def leave_critical_section(dir) :
  while not try_lock(dir) :
    # busy wait
    pass
  print >> open(dir+'-critsec-count', 'w'), 0,
  unlock(dir)

def main(argv=None):
  now_sec = time.time()
  for dir in glob.glob('/tmp/192.168.2.*') :
    try :
      if not os.path.exists(dir+'/on') : continue
      stat = os.stat(dir+'-critsec-count')
      if now_sec - stat.st_mtime > timeout_sec :
        print dir
        domain = dir.split('/')[2]
        port = 80

        if not try_enter_critical_section(dir) :
          continue

        if domain.rfind(':') :
          port = domain.split(':')[1]
        save_vm(domain.split(':')[0], port)
        os.remove(dir+'/on')

        leave_critical_section(dir)

    except OSError as e :
      if e.errno == errno.ENOENT and e.filename.endswith('-critsec-count') :
        pass
      else :
        raise e

if __name__ == '__main__':
  while True :
    main()
    time.sleep(5)
