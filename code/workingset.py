#!/usr/bin/env python2.7

import subprocess as sp
from shlex import split
import time

virsh_name = 'dreamservertest'
ip='192.168.122.239'

def wait_for_http(ipaddr) :
    server_up = False
    while not server_up :
        cmd = 'nc -w 1 -z %s %s'%(ipaddr, '80')
        rc = sp.call(split(cmd))
        if rc == 0 : server_up = True
        else : time.sleep(0.1)

def wait_shutdown(virsh_name) :
    while not sp.call('virsh list | grep %s'%(virsh_name), shell=True) :
        time.sleep(1)

def wait_process_dead(pid) :
    while 0 == sp.call(split('ls /proc/%s/status'%(pid))) :
        print 'wait'
        time.sleep(0.1)
    

def single_run(run) :
    sp.check_call(split('virsh save %s %s.virsh'%(virsh_name, virsh_name)))
    stap_process = sp.Popen(split('stap pfaults.stp -o %03d-stap-raw'%(run)))
    time.sleep(2)
    sp.check_call(split('virsh restore %s.virsh'%(virsh_name)))
    wait_for_http(ip)
    stap_process.kill()
    stap_process.wait()

    sp.check_call(split('pkill stap'))
    sp.check_output(split('pgrep stap'))

    for line in sp.check_output(split('pgrep stap')).split() :
        line = line.strip()
        wait_process_dead(line)

    pid = sp.check_output(split('pgrep -f qemu-system-x86_64'))
    pid = pid.strip()
    output = sp.check_output(split('sudo grep -A10 suspend.img /proc/%s/smaps'%(pid)))
    lines = output.split('\n')
    host_virt_base_addr = lines[0].split()[0].split('-')[0]
    print host_virt_base_addr, pid, lines[8]

    
    # sp.check_call(split('virsh shutdown %s'%(virsh_name)))
    # wait_shutdown(virsh_name)

    cmd = './stap.py %s 0x%s < %03d-stap-raw > %03d-stap.csv'%(pid, host_virt_base_addr, run, run)
    print cmd
    sp.check_call(cmd, shell=True)

def global_setup() :
    sp.check_call(split("virsh restore %s.virsh"%(virsh_name)))
    wait_for_http(ip)

def main() :

    global_setup()
    for run in range(0,1) :
        single_run(run)

if __name__ == "__main__" :
    main()
