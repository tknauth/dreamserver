#!/usr/bin/python2.7 -W all

import pdb
import time
import subprocess as sp
import shlex
import sys
import numpy as np
import itertools
import os
import os.path
import multiprocessing
from collections import defaultdict
from xml.dom.minidom import parse
import workingset

base_ip = 122

# vars = {'disk' : ['ssd', 'hdd'],
#         'storage' : ['direct', 'nbd'],
#         'qemu' : ['stock', 'lazyws'],
#         'ws' : ['present'],
#         'action' : ['resume'],
#         'cache' : ['cold'],
#         'appliance' : ['django'],
#         'ramcfg_mb' : [1024*i for i in [2]],
#         # 'ramuse_mb' : [1] +[512*i for i in range(1,4)],
#         'ramuse_mb' : [1]+[512*i for i in range(1,4)],
#         'concurrent' : [1],
#         'runs' : range(1,11)}

vars = {'disk' : ['hdd', 'ssd'],
        'storage' : ['direct', 'nbd'],
        'qemu' : ['stock', 'lazy', 'lazyws'],
        'ws' : ['present'],
        'action' : ['resume'],
        'cache' : ['cold'],
        'appliance' : ['django', 'jenkins', 'owncloud', 'trac', 'wordpress', 'rubis', 'mediawiki'],
        'ramcfg_mb' : [1024*i for i in [2]],
        'ramuse_mb' : [1]+[512*i for i in range(1,1)],
        'concurrent' : [1],
        'runs' : range(1,4)}

key_order = ['disk', 'storage', 'qemu', 'ws', 'action', 'cache', 'appliance', 'ramcfg_mb', 'ramuse_mb', 'concurrent', 'runs']

appliance2url_path = {
'django' : '/',
'rubis' : 'PHP/SearchItemsByCategory.php?category=7&categoryName=Computers+',
'jenkins' : 'jenkins/login',
'wordpress' : 'wordpress/',
'trac' : 'trac/',
'owncloud' : 'owncloud/',
'mediawiki' : 'mediawiki/index.php/Main_Page'
}

appliance2image_name = {
'django' : 'django.qcow2',
'rubis' : '192.168.2.122.qcow2',
'jenkins' : 'bitnami-jenkins-1.533-0-ubuntu-12.04.qcow2',
'wordpress' : 'bitnami-wordpress-3.6.1-2-ubuntu-12.04.qcow2',
'trac' : 'bitnami-trac-1.0.1-1-ubuntu-12.04.qcow2',
'owncloud' : 'bitnami-owncloud-5.0.12-1-ubuntu-12.04.qcow2',
'mediawiki' : 'mediawiki.qcow2'
}

script_start_timestamp = time.strftime("%Y%m%dT%H%M%S")

def wait_for_http_server(ip, port=80, url_path='') :
    count = 0
    url = "http://%s:%d/%s"%(ip,port,url_path)
    while 0 != sp.call(shlex.split("curl --fail %s"%(url)), stdout=None) :
        count += 1
        time.sleep(1)
        if count >= 40 :
            print 'failing ... web service did not come up'
            sys.exit(1)

def try_tcp_connect(ip, port) :
    rc = sp.call(shlex.split("nc -w 1 -z %s %s"%(ip, str(port))))
    return rc

def wait_till_shutdown(virsh_name) :
    while True :
        rc = sp.call("virsh list | grep %s"%(virsh_name),
                     stdout=None, stderr=None, shell=True)
        if rc != 0 :
            break
        time.sleep(1)

def run_mediawiki(v, virsh_name) :
    experiment_cfg = ''
    for k in key_order :
        experiment_cfg += k+'__'+str(v[k])+'__'
    experiment_cfg = experiment_cfg.strip()

    sp.check_call('sed "s/192.168.2.201/%s/" < ~/phd/dreamserver/code/urls.txt > /tmp/urls.txt'%(virsh_name),
                  shell=True)

    stdout_filename = '%s-siege-%s'%(script_start_timestamp, experiment_cfg)
    # run for 5 minutes (-t5m), hitting random URLs (-i) stored in a
    # file (-f urls.txt) with one simulated user (-c 1)
    sp.check_call('siege -i -c 1 -f /tmp/urls.txt -t1m -b > %s'%(stdout_filename), shell=True)

class Virsh() :
    def start(self, id, p) :
        return 'virsh start %s'%(id)
    def restore(self, id, p) :
        return 'virsh restore /mnt/%s.virsh'%(id)
    def suspend(self, id, p) :
        return 'virsh save %s /mnt/%s.virsh'%(id, id)
    def stop(self, id, p) :
        return 'virsh shutdown %s'%(id)
    def dumpws(self, id, p) :
        return 'virsh qemu-monitor-command --hmp %s "dumpws %s"'%(virsh_name, working_set_type)

class Qemu() :

    start_cmd = 'sudo /home/thomas/bin/kvm -enable-kvm -M pc-1.0 -m %d -smp 1,sockets=1,cores=1,threads=1 \
-name %s -uuid 2b5027ab-e19b-3530-0116-9d56c463667a -nographic -nodefconfig -nodefaults \
-rtc base=utc -no-shutdown -boot c -drive file=%s,if=none,id=drive-ide0-0-0,format=qcow2 \
-monitor telnet:localhost:7001,server,nowait,nodelay \
-device ide-drive,bus=ide.0,unit=0,drive=drive-ide0-0-0,id=ide0-0-0 \
-device e1000,netdev=net0,mac=%s -netdev tap,id=net0,script=/etc/ovs-ifup,downscript=/etc/ovs-ifdown \
-daemonize'
    mac = '52:54:00:37:4d:16'

    def start(self, id, p) :
        assert id == '192.168.2.122'
        disk_path = '/mnt/%s'%(appliance2image_name[p['appliance']])
        cmd = self.start_cmd%(p['ramcfg_mb'], id, disk_path, self.mac)
        if p['qemu'] in ['lazy', 'lazyws'] :
            cmd += ' -suspend-image /mnt/%s-suspend-image'%(id)

        return cmd

    def restore(self, id, p) :
        assert id == '192.168.2.122'
        disk_path = '/mnt/%s'%(appliance2image_name[p['appliance']])
        cmd = self.start_cmd%(p['ramcfg_mb'], id, disk_path, self.mac)
        cmd += ' -incoming "exec: cat /mnt/%s.virsh"'%(id)
        if p['qemu'] in ['lazy', 'lazyws'] :
            cmd += ' -suspend-image /mnt/%s-suspend-image'%(id)

        return cmd

    def suspend(self, id, p) :
        suspend_file = '/mnt/%s.virsh'%(id)
        cmd = '''echo "stop\n\
migrate_set_speed 1g\n\
migrate \\"exec: cat > %s\\"\n\
quit" | nc 127.0.0.1 7001'''
        cmd = cmd%(suspend_file)
        return cmd

    def stop(self, id, p) :
        cmd = 'echo quit | nc 127.0.0.1 7001'
        return cmd

    def dumpws(self, id, p) :
        cmd = 'echo dumpws %s | nc 127.0.0.1 7001'%(p['ws'])
        return cmd

vmbackend = Qemu()

other_benchmarks = defaultdict(lambda : (lambda x,y : 0))
other_benchmarks['mediawiki'] = run_mediawiki

def try_tcp(virsh_name) :
    import socket
    try :
        s = socket.create_connection((virsh_name, 80), 0.1)
        s.close()
        return True
    except socket.error as e :
        print e
        return False

def run(v, virsh_name, time_to_response) :

    begin = time.time()
    if v['action'] == 'start' :
        p = sp.Popen(shlex.split(vmbackend.start(virsh_name, v)))
    elif v['action'] == 'resume' :
        p = sp.Popen(shlex.split(vmbackend.restore(virsh_name, v)))

    # time.sleep(0.1)
    while try_tcp(virsh_name) :
        pass

    while True :
        url_path = appliance2url_path[v['appliance']]
        rc = sp.call(shlex.split("curl --fail http://%s/%s"%(virsh_name, url_path)), stdout=None)
        if rc == 0 :
            break
    end = time.time()

    time_to_response.value = (end-begin)

#    other_benchmarks[v['appliance']](v, virsh_name)

def setup_nbd(disk, mount_point='/mnt') :
    sp.check_call(shlex.split('sudo nbd-client -N %s 192.168.2.3 /dev/nbd0'%(disk)))
    sp.check_call(shlex.split('sudo mount /dev/nbd0 %s'%(mount_point)))

def teardown_nbd(mount_point='/mnt') :
    sp.check_call(shlex.split("sudo umount %s"%(mount_point)))
    sp.check_call(shlex.split('sudo nbd-client -d /dev/nbd0'))
    while 0 == sp.call('lsblk | grep -q nbd0', shell=True) :
        time.sleep(1)
    time.sleep(3)

def setup_disk(v) :
    if v['disk'] == 'hdd' and v['storage'] == 'direct' :
        sp.check_call(shlex.split("sudo mount /dev/disk/by-id/scsi-SATA_SAMSUNG_HD204UIS2H7J1CB117088-part4 /mnt"))
    elif v['disk'] == 'ssd' and v['storage'] == 'direct' :
        sp.check_call(shlex.split("sudo mount /dev/disk/by-id/scsi-SATA_INTEL_SSDSC2CT1CVMP244301DC120BGN-part1 /mnt"))
    if v['storage'] == 'nbd' :
        setup_nbd(v['disk'])

def teardown_disk(v) :
    if v['storage'] == 'direct' :
        sp.check_call(shlex.split("sudo umount /mnt"))
    elif v['storage'] == 'nbd' :
        teardown_nbd()
    else :
        assert False

def setup_qemu_binary(v) :
    if v['qemu'] == 'stock' :
        sp.check_call(shlex.split("rm /home/thomas/bin/kvm"))
        sp.check_call(shlex.split("ln -s /home/thomas/bin/qemu-system-x86_64v1_4 /home/thomas/bin/kvm"))
    elif v['qemu'] in ['lazy', 'lazyws'] :
        sp.check_call(shlex.split("rm /home/thomas/bin/kvm"))
        sp.check_call(shlex.split("ln -s /home/thomas/bin/qemu-system-x86_64-remap /home/thomas/bin/kvm"))
    else :
        assert False, 'not implemented'

def teardown_qemu_binary(v) :
    if v['qemu'] in ['lazy', 'lazyws'] :
        sp.check_call(shlex.split("rm /home/thomas/bin/kvm"))
        sp.check_call(shlex.split("ln -s /home/thomas/bin/qemu-system-x86_64v1_4 /home/thomas/bin/kvm"))
    elif v['qemu'] == 'stock' :
        pass

def setup_cache(v) :
    if v['cache'] == 'hot' :
        virsh_name = '192.168.2.%d'%(base_ip)
        suspend_image_file = '/mnt/%s-suspend-image'%(virsh_name)
        sp.check_call(shlex.split('sudo chmod a+r /mnt/%s-suspend-image'%(virsh_name)))
        with open(suspend_image_file, 'r') as f :
            while f.read(1024*1024) :
                pass
    elif v['cache'] == 'cold' :
        print 'setup_cache(): umount/mount'
        teardown_disk(v)
        setup_disk(v)
        # also clear remote's buffer cache
        if v['storage'] == 'nbd' :
            sp.check_call(shlex.split('ssh 192.168.2.3 "echo 1 | sudo tee /proc/sys/vm/drop_caches"'))

def setup_action(v) :
    for i in range(v['concurrent']) :
        virsh_name = '192.168.2.%d'%(base_ip+i)
        if v['action'] == 'resume' :

            
            # if v['runs'] != 1 : return

            sp.check_call(shlex.split(vmbackend.start(virsh_name, v)))
            wait_for_http_server(virsh_name, url_path=appliance2url_path[v['appliance']])
            if v['appliance'] == 'django' :
                wait_for_http_server(virsh_name, 80)
                sp.check_call(shlex.split('curl http://%s:80/ram/%d'%(virsh_name, v['ramuse_mb'])))

            # do some more warmup
            with open("/dev/null") as devnull :
                for i in xrange(100) :
                    url = "http://%s:%d/%s"%(virsh_name, 80, appliance2url_path[v['appliance']])
                    # -s Silent or quiet mode. Don't show progress meter or error messages.  Makes Curl mute.
                    sp.call(shlex.split('curl -s %s'%(url)), stdout=devnull)

            sp.check_call(vmbackend.suspend(virsh_name, v), shell=True)

            if v['qemu'] != 'lazyws' : return
            # The following is only necessary to determine the guest's
            # working set.
            
            # repeats = 1
            # workingset.main(virsh_name, virsh_name, repeats)
            # sp.check_call('sort -n < 000-stap.csv | uniq > /mnt/%s-suspend-image-pages'%(virsh_name), shell=True)
            # sp.check_call('cat *-stap.csv | sort -n | uniq -c | awk \'{if($1>=%d) print $2}\' | sort -n > /mnt/%s-suspend-image-pages'%(1, virsh_name), shell=True)
            # Do one more resume/suspend cycle to append working set
            # to the suspend image.
            sp.check_call(shlex.split(vmbackend.restore(virsh_name, v)))
            url_path = appliance2url_path[v['appliance']]
            wait_for_http_server(virsh_name, url_path=url_path)
            sp.check_call(vmbackend.dumpws(virsh_name, v), shell=True)
            sp.check_call(shlex.split('sudo chmod a+r /mnt/%s-suspend-image-pages-new'%(virsh_name)))
            sp.check_call(vmbackend.suspend(virsh_name, v), shell=True)

        elif v['action'] == 'start' :
            pass

def teardown_action(v) :
    for i in range(v['concurrent']) :
        virsh_name = '192.168.2.%d'%(base_ip+i)
        if v['action'] == 'resume' :
            # Is this the last run of the current parameter
            # combination? Then shutdown VM instance instead of
            # suspending it.
            if v['runs'] == vars['runs'][-1] :
                sp.check_call(vmbackend.stop(virsh_name, v), shell=True)
                wait_till_shutdown(virsh_name)
                sp.check_call(shlex.split("rm --force /mnt/%s.virsh"%(virsh_name)))

                if v['qemu'] in ['lazy', 'lazyws'] :
                    sp.check_call(shlex.split("rm --force /mnt/%s-suspend-image"%(virsh_name)))
                if v['qemu'] == 'lazyws' :
                    sp.check_call(shlex.split("rm --force /mnt/%s-suspend-image-pages-new"%(virsh_name)))
                    sp.check_call(shlex.split("rm --force /mnt/%s-suspend-image-pages-old"%(virsh_name)))
                    # sp.check_call('rm *-stap.csv', shell=True)
            else :
                if v['qemu'] == 'lazyws' :
                    sp.check_call(vmbackend.dumpws(virsh_name, v), shell=True)
                    sp.check_call(shlex.split('sudo chmod a+r /mnt/%s-suspend-image-pages-new'%(virsh_name)))
                    configuration = '-'.join([k+'='+str(v[k]) for k in key_order])
                    ws_pages_filename = '%s-%s-wspages'%(script_start_timestamp, configuration)
                    sp.check_call(shlex.split('cp /mnt/%s-suspend-image-pages-new %s'%(virsh_name, ws_pages_filename)))
                sp.check_call(vmbackend.suspend(virsh_name, v), shell=True)
                wait_till_shutdown(virsh_name)

        elif v['action'] == 'start' :
            sp.check_call(vmbackend.stop(virsh_name, v), shell=True)
            wait_till_shutdown(virsh_name)

def teardown_cache(v) :
    pass

def replace_text_tag(dom, tag, text) :
    e = dom.getElementsByTagName(tag)
    assert len(e) == 1, 'tag not unique'
    e[0].childNodes[0].replaceWholeText(text)

def update_disk_source_file(dom, source_file) :
    elm_disk = dom.getElementsByTagName('disk')
    assert len(elm_disk) == 1, 'element not unique'
    elm_source = elm_disk[0].getElementsByTagName('source')
    assert len(elm_source) == 1, 'element not unique'
    elm_source[0].setAttribute('file', source_file)

def setup_ramcfg_mb(v) :
    for i in range(v['concurrent']) :
        virsh_name = '192.168.2.%d'%(base_ip+i)
        dom = parse('/tmp/%s.xml'%(virsh_name))

        replace_text_tag(dom, 'memory', str(v['ramcfg_mb']*1024))
        replace_text_tag(dom, 'currentMemory', str(v['ramcfg_mb']*1024))

        if v['qemu'] == 'stock' :
            pass
        elif v['qemu'] in ['lazy', 'lazyws'] :
            if not dom.getElementsByTagName('qemu:commandline') :
                e = dom.createElement('qemu:commandline')
                e.appendChild(dom.createElement('qemu:arg'))
                e.childNodes[0].setAttribute('value', '-suspend-image')
                e.appendChild(dom.createElement('qemu:arg'))
                e.childNodes[1].setAttribute('value', '/mnt/%s-suspend-image'%(virsh_name))
                assert len(dom.childNodes) == 1
                dom.childNodes[0].appendChild(e)
                dom.childNodes[0].setAttribute('xmlns:qemu', 'http://libvirt.org/schemas/domain/qemu/1.0')
                dom.getElementsByTagName('emulator')[0].childNodes[0].replaceWholeText('/home/thomas/bin/kvm')

        with open('/tmp/%s.xml'%(virsh_name), 'w') as f :
            f.write(dom.toxml())

def teardown_ramcfg_mb(v) :
    for i in range(v['concurrent']) :
        virsh_name = '192.168.2.%d'%(base_ip+i)
        dom = parse('/tmp/%s.xml'%(virsh_name))

        replace_text_tag(dom, 'memory', str(v['ramcfg_mb']*1024))
        replace_text_tag(dom, 'currentMemory', str(v['ramcfg_mb']*1024))

        if v['qemu'] in ['lazy', 'lazyws'] :
            e = dom.getElementsByTagName('qemu:commandline')
            assert len(e) == 1
            e[0].parentNode.removeChild(e[0])

        with open('/tmp/%s.xml'%(virsh_name), 'w') as f :
            f.write(dom.toxml())

def setup_guest_disk(v) :
    virsh_name = '192.168.2.%d'%(base_ip)
    dom = parse('/tmp/%s.xml'%(virsh_name))
    image_name = appliance2image_name[v['appliance']]
    sp.check_call(shlex.split('qemu-img create -f qcow2 -b /mnt/%s /mnt/vm.qcow2'%(image_name)))
    update_disk_source_file(dom, '/mnt/vm.qcow2')
    with open('/tmp/%s.xml'%(virsh_name), 'w') as f :
        f.write(dom.toxml())

def teardown_guest_disk(v) :
    virsh_name = '192.168.2.%d'%(base_ip)
    dom = parse('/tmp/%s.xml'%(virsh_name))
    update_disk_source_file(dom, '/mnt/192.168.2.%d.qcow2'%(base_ip))
    with open('/tmp/%s.xml'%(virsh_name), 'w') as f :
        f.write(dom.toxml())
    sp.check_call(shlex.split('rm /mnt/vm.qcow2'))

def get_qemu_maj_page_faults(v) :
    maj_faults = []
    for i in range(v['concurrent']) :
        virsh_name = '192.168.2.%d'%(base_ip+i)
        pid = sp.check_output(shlex.split('pgrep -f %s'%(virsh_name))).strip()
        v = sp.check_output(shlex.split('ps -o maj_flt= %s'%(pid))).strip()
        maj_faults.append(int(v))

    return maj_faults

def get_qemu_min_page_faults(v) :
    maj_faults = []
    for i in range(v['concurrent']) :
        virsh_name = '192.168.2.%d'%(base_ip+i)
        pid = sp.check_output(shlex.split('pgrep -f %s'%(virsh_name))).strip()
        v = sp.check_output(shlex.split('ps -o min_flt= %s'%(pid))).strip()
        maj_faults.append(int(v))

    return maj_faults

def get_workingset_size_byte(v) :

    if not (v['qemu'] == 'lazyws' and v['action'] == 'resume') : return [0]

    virsh_name = '192.168.2.%d'%(base_ip)
    fn = '/mnt/%s-suspend-image-pages-new'%(virsh_name)
    nr_pages = sp.check_output('wc -l %s | cut -d" " -f1'%(fn), shell=True)
    page_size_byte = 4096

    return [int(nr_pages) * page_size_byte]

def dump_virsh_xml(v) :
    virsh_name = '192.168.2.%d'%(base_ip)
    sp.check_call('virsh dumpxml %s > /tmp/%s.xml'%(virsh_name, virsh_name), shell=True)

def define_virsh_xml(v) :
    virsh_name = '192.168.2.%d'%(base_ip)
    sp.check_call(shlex.split('virsh define /tmp/%s.xml'%(virsh_name)))

def run_experiment(v, measurements) :
    dump_virsh_xml(v)
    setup_ramcfg_mb(v)
    setup_disk(v)
    setup_qemu_binary(v)
    setup_guest_disk(v)
    define_virsh_xml(v) # setup_action depends on virsh definition
    setup_action(v)
    setup_cache(v)

    measurements['ws_size_byte'] = get_workingset_size_byte(v)

    time_to_respond = []
    ps = []
    for i in range(v['concurrent']) :
        time_to_respond.append(multiprocessing.Value('d', 0.0))
        virsh_name = '192.168.2.%d'%(base_ip+i)
        p = multiprocessing.Process(target=run, args=(v, virsh_name, time_to_respond[i]))
        p.start()
        ps.append(p)

    for p in ps : p.join()

    time.sleep(2)

    measurements['dumpsize_byte'] = []
    if v['action'] == 'resume' :
        for n in range(v['concurrent']) :
            virsh_name = '192.168.2.%d'%(base_ip+n)
            measurements['dumpsize_byte'].append(os.stat('/mnt/%s.virsh'%(virsh_name)).st_size)

    measurements['maj_pg_faults'] = get_qemu_maj_page_faults(v)
    measurements['min_pg_faults'] = get_qemu_min_page_faults(v)

    teardown_cache(v)
    teardown_action(v)
    teardown_guest_disk(v)
    teardown_qemu_binary(v)
    teardown_disk(v)
    teardown_ramcfg_mb(v)
    define_virsh_xml(v)
    virsh_name = '192.168.2.%d'%(base_ip)
    sp.check_call(shlex.split('rm /tmp/%s.xml'%(virsh_name)))

    return [x.value for x in time_to_respond]

def get_report_filename() :
    script_basename = os.path.basename(__file__)
    return "%s-%s.csv"%(script_start_timestamp, script_basename)

def global_setup_guest_disk_direct(virsh_name) :
    sp.check_call(shlex.split("sudo mount /dev/disk/by-id/scsi-SATA_SAMSUNG_HD204UIS2H7J1CB117088-part4 /mnt/hdd"))
    sp.check_call(shlex.split("sudo mount /dev/disk/by-id/scsi-SATA_INTEL_SSDSC2CT1CVMP244301DC120BGN-part1 /mnt/ssd"))

    for disk in vars['disk'] :
        for appl in vars['appliance'] :
            image_name = appliance2image_name[appl]
            sp.check_call(shlex.split('rsync -a /home/thomas/%s /mnt/%s'%(image_name, disk)))

    sp.check_call(shlex.split("sudo umount /mnt/hdd"))
    sp.check_call(shlex.split("sudo umount /mnt/ssd"))

def global_setup_guest_disk_nbd(virsh_name) :
    sp.check_call(shlex.split('ssh 192.168.2.3 sudo service nbd-server restart'))
    for disk in ['hdd', 'ssd'] :
        setup_nbd(disk)
        for app in  vars['appliance'] :
            image_name = appliance2image_name[app]
            sp.check_call(shlex.split('rsync -a /home/thomas/%s /mnt'%(image_name)))
        teardown_nbd()

def global_setup_guest_disk(virsh_name) :
    if 'direct' in vars['storage'] : global_setup_guest_disk_direct(virsh_name)
    if 'nbd' in vars['storage'] : global_setup_guest_disk_nbd(virsh_name)

def global_teardown_guest_disk_direct(virsh_name) :
    sp.check_call(shlex.split("sudo mount /dev/disk/by-id/scsi-SATA_SAMSUNG_HD204UIS2H7J1CB117088-part4 /mnt/hdd"))
    sp.check_call(shlex.split("sudo mount /dev/disk/by-id/scsi-SATA_INTEL_SSDSC2CT1CVMP244301DC120BGN-part1 /mnt/ssd"))

    for disk in vars['disk'] :
        for app in vars['appliance'] :
            image_name = appliance2image_name[app]
            sp.check_call(shlex.split('rm --force /mnt/%s/%s'%(disk, image_name)))

    sp.check_call(shlex.split("sudo umount /mnt/hdd"))
    sp.check_call(shlex.split("sudo umount /mnt/ssd"))

def global_teardown_guest_disk_nbd(virsh_name) :
    for disk in vars['disk'] :
        setup_nbd(disk)
        for app in vars['appliance'] :
            image_name = appliance2image_name[app]
            sp.check_call(shlex.split('rm --force /mnt/%s'%(image_name)))
        teardown_nbd()

def global_teardown_guest_disk(virsh_name) :
    if 'direct' in vars['storage'] : global_teardown_guest_disk_direct(virsh_name)
    if 'nbd' in vars['storage'] : global_teardown_guest_disk_nbd(virsh_name)

def global_setup(virsh_name) :
    global_setup_guest_disk(virsh_name)

def global_teardown(virsh_name) :
    global_teardown_guest_disk(virsh_name)

if __name__ == '__main__' :

    resume_execution = False
    completed_configurations = 0
    openmode = 'w'

    filename = get_report_filename()
    if len(sys.argv) > 1 and os.path.exists(sys.argv[1]) :
        filename = sys.argv[1]
        completed_configurations = int(sp.check_output('wc -l < %s'%(sys.argv[1]), shell=True))
        openmode = 'w+'
        resume_execution = True

    f = open(filename, openmode)

    if not resume_execution :
        for k in key_order :
            print >> f, k, 
        print >> f, 'time_s', 'dumpsize_byte', 'maj_pg_faults', 'min_pg_fautls', 'ws_size_byte'

    global_setup('192.168.2.%d'%(base_ip))

    for (cfg_count, tuple) in enumerate(itertools.product(*[vars[k] for k in key_order])) :
        parameters = {}
        for i in range(len(tuple)) : parameters[key_order[i]] = tuple[i]

        # skip illegal parameter configurations
        if parameters['ramuse_mb'] > (parameters['ramcfg_mb'] - 512) : continue
        if parameters['action'] == 'start' and parameters['qemu'] != 'stock' : continue

        if cfg_count < completed_configurations : continue

        for k in key_order :
            print >> f, parameters[k],

        measurements = {}
        took = run_experiment(parameters, measurements)
        print >> f, \
            ','.join(map(lambda x : '%.2f'%x, took)), \
            ','.join(map(lambda x : '%d'%x, measurements['dumpsize_byte'])), \
            ','.join(map(lambda x : '%d'%x, measurements['maj_pg_faults'])), \
            ','.join(map(lambda x : '%d'%x, measurements['min_pg_faults'])), \
            ','.join(map(lambda x : '%d'%x, measurements['ws_size_byte']))
        f.flush()

    f.close()

    global_teardown('192.168.2.%d'%(base_ip))
