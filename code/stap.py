#!/usr/bin/env python2.7

import fileinput
import sys

pid = sys.argv[1]
host_virt_base = int(sys.argv[2], 16)

page_size = 4096

def process_line(l) :
    l = line.strip()
    fields = line.split(':')

    # skip lines not belonging to our pid
    if not fields[1] == pid : return

    fields[2] = int(fields[2], 16)
    # BUG: was: not addr >= base and addr - base <= ram_size
    # should have been: not (addr >= base and addr - base <= ram_size)
    if not (fields[2] >= host_virt_base and \
            (fields[2] - host_virt_base) <= 1024 * 1024 * 1024) : return

    # BUG: must round-down to nearest page boundary
    guest_phy = fields[2] - host_virt_base
    guest_phy_page_aligned = guest_phy - (guest_phy % page_size)
    print guest_phy_page_aligned


for line in sys.stdin :
    process_line(line)

