#!/bin/bash

CPUS=$(grep -c processor /proc/cpuinfo)
BASEDIR=$(pwd)

cd /var/tmp
svn checkout http://svn.apache.org/repos/asf/httpd/httpd/branches/2.4.x httpd-2.4.x-svn
cd httpd-2.4.x-svn
svn co http://svn.apache.org/repos/asf/apr/apr/trunk srclib/apr
./buildconf
./configure --with-included-apr --prefix=/tmp/apache --enable-debugger-mode --enable-maintainer-mode CFLAGS="-g" MOD_PROXY_HTTP_LDADD="-lm"
cp $BASEDIR/mod_proxy_http.c modules/proxy
make -j$CPUS
make install

cp $BASEDIR/httpd.conf /tmp/apache/conf/
bash $BASEDIR/httpd.sh >> /tmp/apache/conf/httpd.conf
cp $BASEDIR/start-backend-server.py /tmp/apache/bin/

cd /var/tmp
wget http://nginx.org/download/nginx-1.2.4.tar.gz
tar xfz nginx-1.2.4.tar.gz
cd nginx-1.2.4
./configure --prefix=/tmp/nginx
make -j$CPUS
make install

for i in $(seq 0 2) ; do
    port=8$(printf %03d $i)
    cp /tmp/nginx/conf/nginx.conf /tmp/nginx/conf/127.0.0.1:$port-nginx.conf
    sed -i -r "s/listen\s+80;/listen $port;/" /tmp/nginx/conf/127.0.0.1:$port-nginx.conf
done
