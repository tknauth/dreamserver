\section{Evaluation}
\label{sec:evaluation}

In our evaluation we focus on the following questions:

\begin{itemize}
\item How fast can virtual machines be suspended? How can suspend times be decreased?
\item How fast can virtual machines be resumed? How can resume times be improved?
\item How well does virtual machine synchronization perform? How much data is transferred? How long does it take?
\item What is the impact on nth percentile request latency? Does suspend/resume change nth percentile latency? What can be done to improve nth percentile latency?
\end{itemize}

\subsection{Setup}

We use three physical machines for conducting our experimental evaluation:
one machine generates client requests and runs the Apache reverse proxy.
A second machine hosts all virtual machines.
The third machine serves as the migration destination.
All machines have two one Gigagbit network interfaces.
One interface is connected to the public network (Internet),
while the second interface is a private network.
Machines do not have access to shared storage.

Our virtual machine instances are based on Ubuntu's ``virtual'' flavor.
We used Ubuntu's vmbuilder to set the instances up.
In addition to the standard package selection we also install the openssh server and acpid.
Two types of virtual machines are used:
the first type is a plain and simple web server serving static content.
This instance type uses nginx as its web server.
A second type represents a more heavy weight installation with the classical LAMP stack:
Linux, Apache, mysql, and PHP.
We use RUBiS~\cite{rubis} as the PHP application.

The virtual machines are configured with a single core and varying amounts of RAM.
We ran four virtual machines of type one with 256, 512, 1024, and 2048~MB RAM, respectively.
For VM instances of the second type, we excluded configurations with 256 MB because performance was unreasonable with this little memory.
The host system was equipped with a four core processor (AMD Phenom II) and 12 GB of RAM.
All virtual machines fit comfortably into main memory (7.5~GB).
The virtual machine host was running Ubuntu 12.04.
We used kvm as our virtualization technology of choice.

We use the real-world traces presented at the beginning to drive our experimental evaluations.
For the simple web servers,
simulated clients issue requests following the inter-arrival time recorded in the department web server traces.
RUBiS comes with its own client simulator.
For each RUBiS virtual machine,
we periodically start a RUBiS client simulator every 1 to 9 minutes.
The RUBiS simulator emulates multiple clients, i.e., it establishes a variable number of TCP sessions to the RUBiS server.
Each RUBiS simulator run lasts for 7 minutes: 1 minute ramp up, 5 minutes simulation, 1 minute ramp down.

\subsection{Virtual machine suspend}

\begin{figure}
  \includegraphics{figs/suspendtime-vs-dumpsize}
\caption{The virtual machine suspend time as a function of the memory dump size. The two are linearly correlated. The correlation coefficient is $0.96$.}
\label{fig:suspendtime-vs-dumpsize}
\end{figure}

The suspend time is important because it is one factor contributing to the latency perceived by clients.
A fast suspend mechanism ensures that host resources are freed up quickly to be re-used by other tasks, e.g., other virtual machines just starting on this host.
As is visible in Figure~\ref{fig:suspendtime-vs-dumpsize},
the time it takes to suspend a virtual machine depends mainly on the virtual machine's \emph{used memory}.
We emphasize the word used here because even though a virtual machine may be configured with 2 GB of RAM, it may only use a fraction of the 2 GB.
The suspend time grows linearly with the memory dump size.
One noteworthy fact here is that the suspend time seems to be CPU rather than I/O bound:
writing the memory dump to a much faster SSD instead of a conventional spinning disk did not improve the suspend times at all.
The memory dump throughput is low at around 25 MB/s (e.g., 60 seconds for 1400 MB),
further suggesting the CPU-bound nature of the suspend process.

\subsection{Virtual machine resume}

\begin{figure}
  \includegraphics{figs/resumetime-vs-dumpsize}
\caption{Virtual machine resume time as a function of memory dump size.
  Observed resume times depend on the page cache's hottness.
  If pages are not in main memory, the bandwidth of the backing store bounds the resume time.}
\label{fig:resumetime-vs-dumpsize}
\end{figure}

After a virtual machine was suspended it must be woken up whenever a request for it arrives.
Figure~\ref{fig:resumetime-vs-dumpsize} illustrates the observed resume latencies.
The resume time includes the time for spinning up the virtual machine until connections on port 80 can be established.
Resume times can be as low as 2-3 seconds if the pages belonging to the memory dump are still present in the host's main memory.
Resume times with a hot page cache are in the single digit second range even for large memory dumps of up to around 1300 MB.
Conversely, if the required pages are not cached by the host the virtual machine's memory dump must be read in parts or entirely from disk.
The fractional presence of pages in the cache explain the vertical bands in Figure~\ref{fig:resumetime-vs-dumpsize}.
Also, other processes may be competing for disk bandwidth.
We observe worst case resume latencies of up to 110 seconds.

Because the resume process is disk I/O bound there are two straightforward remedies to decrease worst case resume times:
(1) if the host has multiple disks they can be configured as a RAID to improve throughput.
(2) Alternatively, spinning disk can be replaced with with solid state disk~(SSD).
The best case throughput of spinning disks peaks at around 120 MB/s.
SSDs can easily provide twice the performance.
We also measured the resume time where the virtual machine's memory dump was written/read to/from an SSD.
Even for cases with a cold page cache and a memory dump size of 1400 MB,
the resume time was around 10 seconds only.

SSDs are already deployed in modern data centers.
If not to replace conventional spinning disks,
then at least in addition to them~\footnote{\url{http://www.wired.com/wiredenterprise/2012/06/flash-data-centers/}}.
As the prices for solid state drives continue to fall and their capacity increases,
we expect their widespread adoption.
Figure~ref{fig:resumetime-vs-dumpsize} should thus be interpreted as worst case resume times \emph{on old hardware}.
Using solid state drives, the observed resume times would be reduced drastically (50\%-90\%).

\subsection{Latency}

\begin{figure}
  \includegraphics{figs/percentile-transitions}
\caption{The maximum number of requests which can be delayed each day without violating 99th or 90th percentile latency requirements. On average 133 and 1331 delays are allowed to meet 99th percentile and 90th percentile requirements, respectively.}
\label{fig:percentile-transitions}
\end{figure}

Figure~\ref{fig:percentile-transitions} shows the number of requests which may be delayed without violating either the 99th or 90th percentile latency.
The number solely depends on the total number of requests processed each day.
For our department web server the average number of transitions which can be performed without violating the 99th percentile latency is 133 per day.
If the SLA leaves more room for latency variations, say, 90th percentile,
the number of violations per day increases to 1331.
However, if we look at the number of possible transitions when employing a timeout based strategy,
we discover that 133 transitions per day are insufficient to exploit every idle interval.
Figure~\ref{fig:percentile-transitions2} plots the number of possible transitions for 99th percentile compliance together with the number of 60 second idle intervals.
We can determine, that 99th percentile compliance does not nearly allow to exploit every 60 second idle interval.
Actually, for our trace, only on 25 days out of 319 would the 99th percentile not be violated if the backend would be suspended after each 60 second idle interval.
In this paper, we only want to point out the trade-offs involved when faced with nth percentile latency requirements.
We leave devising an effective strategy for future work.

\begin{figure}
  \includegraphics{figs/percentile-transitions2}
\caption{Contrasting possible transitions as dictated by 99th percentile requirements and available transitions as defined by 60 second timeout interval. Many more idle periods exist than could be exploited with a stringent 99th percentile SLA.}
\label{fig:percentile-transitions2}
\end{figure}

\subsection{State synchronization}

State synchronization is required for fault-tolerance.
If one machine, rack or a whole data center crashes execution can be resumed transparently on a different machine, rack or data center.
To synchronize the state between the source and destination the virtual machines disk and memory state must be copied.
Because the disk and memory state can easily comprise multiple gigabytes,
a straightforward state copy may waste precious network bandwidth.
Only copying the changed state will reduce the required network bandwidth dramatically, as we will see.
The Linux utility \emph{rsync} is used to synchronize directory trees between two locations.
It also features a delta copy mode for large files.
Per block checksums are calculated at the source and destination.
Only if the checksum for a block differes between source and destination is the block actually copied.
The reduction in network bandwidth is bought at the expense of CPU cycles and disk bandwidth at the source and destination.
However, if network bandwidth is scarce, for example, when synchronizing virtual machines between data centers, the conserved network bandwidth may justify the trade-off.

\begin{figure*}
  \begin{tabular}{ccc}
    \begin{minipage}[t]{0.33\textwidth}
      \includegraphics{figs/rsync-savings-cdf}
      \caption{Effectiveness of rsync to reduce the data sent over the network.
        In 80\% of the cases only 1\% of the original state must be transferred.}
      \label{fig:rsync-savings-cdf}
    \end{minipage}
    &
    \begin{minipage}[t]{0.33\textwidth}
      \includegraphics{figs/rsync-xfered-cdf}
      \caption{Traffic in gigabyte incurred by synchronizing virtual machine state with backup location. 71\% of the transfers transmit 1 MB or less of data. Less than 3\% of synchronizations transmit more than 100 MB.}
      \label{fig:rsync-xfered-cdf}
    \end{minipage}
    &
    \begin{minipage}[t]{0.33\textwidth}
      \includegraphics{figs/rsync-statesize-vs-duration}
      \caption{Duration of state synchronization operation as a function of state size.
      Time needed for synchronization does not just depend on state size, but also on page cache hotness.}
      \label{fig:rsync-statsize-vs-duration}
    \end{minipage}
  \end{tabular}
\end{figure*}

Figure~\ref{fig:rsync-savings-cdf} demonstrates the effectiveness of our delta state transfer.
The fraction of data actually sent over the wire is less than 20\% of the original data in 96\% of the cases.
In 80\% of the cases as little 1-2\% of the original data must be transferred.
In the case of a multi-gigabyte virtual machine disk 1-2\% amount to a couple of megabytes ($<$ 100~MB) instead of a couple of gigabytes.
We can conclude that for our workloads delta state transfer is effective and yields in a massive reduction of network traffic.

The actual amount of data transferred is depicted in Figure~\ref{fig:rsync-xfered-cdf}.
About 71\% of the transfers involve only up to one megabyte.
Transfers of 100 megabyte or more are rare at only 3\%.
For example, if a virtual machine is synchronized for the first time,
the entire state must be copied.
The simple web servers serving static content generally require less state synchronization between suspend/resume cycles.
The virtual machines running a full LAMP stack, dirty more pages during execution.
This requires more state to be copied following a subsequent suspend.
If the state transfer takes up to much of the available data center network bandwidth,
the frequency of transfers can be reduced.
For example, instead of synchronizing the state after each suspend,
synchronization may only happen every other suspend/resume cycle.
The trade-off here is made between data freshness at the destination site in case of a fail-over and consumed network bandwidth.

Last, Figure~\ref{fig:rsync-statsize-vs-duration} shows the time it takes to synchronize the state.
The duration depends on multiple factors:
(1) available I/O bandwidth for disk and network, (2) page cache hit rate, (3) available CPU resources.
The synchronization process reads the state to synchronize from disk and computes per-block checksums.
This happens on the source and destination machine.
Checksums are compared and blocks transferred if the checksums do not match.
The spread of runtimes visible in Figure~\ref{fig:rsync-statsize-vs-duration} is due to interference from other system activities impacting the synchronization process.
The vertical bands are again a manifestation of the page cache's impact on synchronization performance.
If the pages are already present in main memory, this circumvents the disk I/O bottleneck.
As with resume times, storing the virtual machine state on flash memory will speed up the synchronization process in cases were the page cache is cold.

\section{Discussion}
\label{sec:discussion}

We briefly highlight issues related to the idea of resuming/suspending virtual machines and servers:

\begin{itemize}
\item Timed actions, e.g., cron jobs will no longer work as expected.
To support them, sleeping machines may be woken up by ``fake'' requests.
Introducing a ``do-not-suspend-me''-mode may also be required to not shut the machine down while such a periodic job is running.
\item Infrastructure monitoring software will report ``downtimes'' and issue alerts whenever a server is no longer reachable.
If the infrastructure monitoring software supports a ``scheduled downtime''-mode, this needs to be activated whenever the server is suspended.
\item Further research must evaluate the impact of frequent power cycling operations on physical hardware.
The expected lifetime of some components, e.g., spinning disks, may be reduced as a result.
\end{itemize}
