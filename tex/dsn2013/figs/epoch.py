import fileinput
import time

# Convert Apache access log time stamp into Unix epoch time.

for line in fileinput.input():
    cur = time.strptime(line.strip(), "%d/%b/%Y:%H:%M:%S")
    print time.mktime(cur)
