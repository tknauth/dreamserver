import collections
import math
import matplotlib
matplotlib.use('PDF')
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
import pylab
import re
import fileinput
import numpy as np
from pylab import *

fontsize = 7

pylab.rc("font", family="serif")
pylab.rc("font", size=fontsize)
pylab.rc("font", weight="medium")

width = 3.2
height = width/1.3

pylab.rc("figure.subplot", left=(30/72.27)/width)
pylab.rc("figure.subplot", right=(width-10/72.27)/width)
pylab.rc("figure.subplot", bottom=(25/72.27)/height)
# pylab.rc("figure.subplot", top=(height-20/72.27)/height)

matplotlib.rcParams['lines.linewidth']=0.5
matplotlib.rcParams['axes.linewidth']=0.5

import matplotlib.pyplot as plt

fig = plt.figure(figsize=(width, height))
ax1 = fig.add_subplot(111)
ax1.set_autoscaley_on(False)
ax1.set_ylim([0.0,1.0])

def plot_cdf(file, linestyle, color) :
    d = collections.defaultdict(float)
    for line in open(file, 'r') :
        d[float(line.strip())] += 1

    xs = sort(d.keys())
    ys1 = [d[x] for x in xs]
    ys1 = np.array(ys1)
    ys1 /= np.sum(ys1)
    cys1 = np.cumsum(ys1)

    plt.plot(xs, cys1, linestyle, color=color)

plot_cdf('interarrival-wwwse.csv', 'k--', '#222222')
plot_cdf('interarrival-git.csv', 'k-', '#555555')
plot_cdf('interarrival-wiki.csv', 'k', '#888888')

# http://matplotlib.sourceforge.net/mpl_examples/pylab_examples/spine_placement_demo.py
for loc, spine in ax1.spines.iteritems():
    if loc in ['left','bottom']:
        pass
    elif loc in ['right','top']:
        spine.set_color('none') # don't draw spine
    else:
        raise ValueError('unknown spine location: %s'%loc)

# turn off ticks where there is no spine
ax1.xaxis.set_ticks_position('bottom')
ax1.yaxis.set_ticks_position('left')

ax1.set_xscale('log')

plt.xlabel('Inter-arrival time [seconds]')
plt.ylabel('CDF')
legend = plt.legend(['department', 'git', 'wiki'], loc=4)
frame = legend.get_frame()
plt.setp(frame, linewidth=0.5)

plt.savefig('interarrival-cdf')

# plot(X, Y)
# plot(X,CY,'r--')
