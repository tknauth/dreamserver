import math
import matplotlib
matplotlib.use('PDF')
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
import pylab
import glob

fontsize = 7

pylab.rc("font", family="serif")
pylab.rc("font", size=fontsize)

width = 3.2
height = width/1.3

pylab.rc("figure.subplot", left=(30/72.27)/width)
pylab.rc("figure.subplot", right=(width-10/72.27)/width)
pylab.rc("figure.subplot", bottom=(25/72.27)/height)
# pylab.rc("figure.subplot", top=(height-20/72.27)/height)

matplotlib.rcParams['lines.linewidth']=0.5
matplotlib.rcParams['axes.linewidth']=0.5

import matplotlib.pyplot as plt

fig = plt.figure(figsize=(width, height))
ax1 = fig.add_subplot(111)
ax1.set_autoscalex_on(False)
ax1.set_xlim([0, 320])

linestyle=['k-', 'r-', 'm-']
color=['#222222', '#999999', '#555555']

# One line per request; count lines to determine number of request
# Probably totally inefficient but works.
requests_per_day = []
for f in glob.glob('../wwwse/????-??-??.csv') :
    line_count = 0
    for line in open(f, 'r') :
        line_count += 1
    requests_per_day.append(line_count)

percentiles = [99, 90]
for (i, percentile) in enumerate(percentiles) :
    transitions = map(lambda x : x*((100-percentile)/100.),
                      requests_per_day)
    print '%dth percentile : %2.2f avg'%(percentile, np.average(transitions))
    plt.plot(transitions, linestyle[i], color=color[i])

# http://matplotlib.sourceforge.net/mpl_examples/pylab_examples/spine_placement_demo.py
for loc, spine in ax1.spines.iteritems():
    if loc in ['left','bottom']:
        pass
    elif loc in ['right','top']:
        spine.set_color('none') # don't draw spine
    else:
        raise ValueError('unknown spine location: %s'%loc)

# turn off ticks where there is no spine
ax1.xaxis.set_ticks_position('bottom')
ax1.yaxis.set_ticks_position('left')
# ax1.set_yticks([6,12,18,24])
# ax1.set_ylim(0, 24)
ax1.set_yscale('log')

plt.xlabel('day')
plt.ylabel('max. number of transitions')

# ax2 = ax1.twinx()
# ax2.plot(xs, ys2, 'r.')
# ax2.set_ylabel('transitions')

plt.legend(map(lambda x : '%dth'%(x), percentiles), loc=2)
leg = plt.gca().get_legend()
leg.draw_frame(False)
ltext = leg.get_texts()
plt.setp(ltext, fontsize=fontsize)
plt.setp(leg.get_frame(), linewidth=0.5)
plt.savefig('percentile-transitions')
