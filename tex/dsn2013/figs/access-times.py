import math
import matplotlib
matplotlib.use('PDF')
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
import pylab

fontsize=7

pylab.rc("font", family="serif")
pylab.rc("font", size=fontsize)

width = 7.0
height = 2.0
pylab.rc("figure.subplot", left=(30/72.27)/width)
pylab.rc("figure.subplot", right=(width-10/72.27)/width)
pylab.rc("figure.subplot", bottom=(25/72.27)/height)
pylab.rc("figure.subplot", top=(height-20/72.27)/height)

matplotlib.rcParams['lines.linewidth']=0.5
matplotlib.rcParams['axes.linewidth']=0.5

import matplotlib.pyplot as plt

ind = np.arange(3)
fig = plt.figure(figsize=(width, height), linewidth=0.0)
# fig.subplots_adjust(hspace=0.2, wspace=0.1)

# grep 'Accepted publickey for git' ../109-auth-log/auth.log* |awk '{print $3}'|sort -n > access-times-git.csv
# cat ../wwwse.inf.tu-dresden.de-access_log| awk '{print $4}' |cut -d':' -f2- > access-times-wwwse.csv

def myplot(idx, filename, title) :

    ax = fig.add_subplot(idx)
    ax.set_xlabel('hour of day')
    if idx == 131 :
        ax.set_ylabel('number of requests')

    if idx == 133 :
        ax.yaxis.set_major_formatter(matplotlib.ticker.FuncFormatter(lambda x, pos : str(int(x/1000))+'k'))

    x = map(lambda x : int(x.split(':')[0]), map(str.strip, open(filename, 'r').readlines()))
    plt.hist(x, bins=24, range=(0,23), histtype='bar', facecolor='none', linewidth=0.5)

    # http://matplotlib.sourceforge.net/mpl_examples/pylab_examples/spine_placement_demo.py
    for loc, spine in ax.spines.iteritems():
        if loc in ['left','bottom']:
            pass
        elif loc in ['right','top']:
            spine.set_color('none') # don't draw spine
        else:
            raise ValueError('unknown spine location: %s'%loc)

    # turn off ticks where there is no spine
    ax.xaxis.set_ticks_position('bottom')
    ax.yaxis.set_ticks_position('left')

    ax.set_xticks([0, 6, 12, 18, 24])
    # x_labels=
    # ax.set_xticklabels(x_labels)
    ax.set_title(title, fontsize=fontsize)
    
    ax.set_xlim(0,24)
    # ax.set_ylim(0,23)

myplot(131, 'access-times-git.csv', 'GIT repository')
myplot(132, 'access-times-wiki.csv', 'Wiki')
myplot(133, 'access-times-wwwse.csv', 'Departmental web server')

plt.savefig('access-times')
