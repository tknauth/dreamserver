\section{Introduction}

%% What is the problem?
Overall data center utilization is notoriously low.
On average, servers have utilization values of less than 50\%,
often as low as 10-20\%~\cite{barroso2009datacenter, meisner2009powernap}.
Low utilization values paired with the bad energy-proportionality of today's servers wastes a lot of energy.
While energy saving techniques based on transitioning between on and off states are not possible for some workloads~\cite{meisner2011power},
we will show that there still exists ample opportunity for this technique to be useful.
While energy saving techniques applicable to Google-style workloads are interesting,
they are not the only workloads running in today's data centers and cloud environments.

%% Why is it interesting and important?
Only occupying resources for when they are actually used is more efficient in terms of money.
Releasing idle resources frees them up for other purposes.
The authors are big proponents of cloud computing.
Cloud computing, at its core, promotes the idea of renting resources from a provider for a fee.
Fees are based on usage and lease duration.
In such an environment, customers have a strong incentive to only lease the resources when they are actually using them.
Every minute, that the customer can free his resources, saves him money.

Encouraging the customer to be resource-wary on his side,
enables the provider to only turn on additional servers, if they are actually required.

%% Why hasn't it been solved before? (Or, what's wrong with previous
%% proposed solutions? How does mine differ?)
Previous proposals have either focused on explaining why current generation hardware is incapable of exploiting brief (millisecond range) idle periods~\cite{meisner2011power, meisner2009powernap} or considered environments consisting of desktop machines only~\cite{bila2012jettison, agarwal2010sleepserver}.
Instead of dismissing the idea of conserving energy by switching resource on and off based on their utilization,
we instead focus on a different workload class.

{\sysname} tackles the problem of low server utilization by switching off services after a prolonged period of inactivity.
Our primary concern is with web services,
because their transaction-style workload make it easy to detect periods of inactivity.
Whenever the service is not processing a request, it is deemed idle.
We will see that even for moderately popular web sites,
idle periods are frequent and on the order of minutes.

%% What are the key components of my approach and results? Also
%% include any specific limitations.
{\sysname} exploits this by suspending or ``parking'' the (virtual) server belonging to the service.
A parked application no longer actively occupies resources (notably RAM and CPU) on the host machine.
The parked application consumes additional disk space because the memory content of the virtual machine is written to disk.
The disk space overhead will be minimal though,
especially considering that stable storage is cheap and plentiful these days.
The challenge with our approach is to provide a fast ``unparking'' mechanism, i.e.,
when a request is destined for a parked application,
the resume has to be fast to keep the request latency low.

%% Our work lies at the confluence of four distinct changes in the IT landscape:

%% mostly sand, only little boulders~\cite{reiss2012trace}

%% present the first long running implementation and evaluation of an online virtual machine consolidation system.
%% not every company serves google-style workloads.
%% potential for energy optimization with web workloads is present for many small to medium sized companies.
%% according to the EPA, small to medium sized data centers make up XX\% of the total data center population.
%% instead of optimizing a small number of big data centers for energy efficiency,
%% our approach could yield the same benefits by optimizing a large number of small data centers.

%% new feature for infrastructure providers:
%% customer can safe money by having his appliances switched off at times of inactivity.
%% Vogels referred to this application of virtualization technology in a 2008 article as ``application parking''~\cite{vogels2008beyond}.
%% Instead of leaving an idle virtual machine instance running,
%% a snapshot of the instance is made and stored for future use.
%% When there is a demand for this particular instance,
%% it is re-instantiated from the snapshot.
%% Current virtualization technologies, including VMWare, kvm, and, Xen, all support snapshotting of instances.
%% Cloud provider can re-sell spare capacity,
%% increasing his profit.
%% The switched-off appliance will still generate money for the provider,
%% while also safely allowing him to sell the same compute capacity to a second customer.

%% energy efficiency;
%% consolidating virtual machines, containers, applications;
%% low-frequency web services
%% application layer proxy already exists

%% high consolidation ratio; cpus can be time-sliced arbitrarily; ram cannot;
%% typical commodity machine with 8 cores and 32 gb ram may host 30 instances with 1 gb ram each;
%% 2 gb are left for the host OS;

%% Virtualization overhead is reduced, enabling ever higher consolidation ratios~\cite{gordon2012eli, gordon2012elvis}.

%% Although our main interest is in energy efficiency,
%% this work focuses on the aspects of load balancing and availability enabled by suspend/resume of virtual machines.

\subsection{Contributions}

\begin{itemize}
\item {\sysname} presents the first practical system of its kind.
Virtual machines are suspended and resumed on-demand, conserving resources while being switched off.
The whole process is transparent to clients: no connections are denied or interrupted even if the backend server is sleeping when the request arrives.
\item {\sysname} synchronizes virtual machine state periodically to allow fast fail-over in case of data center downtimes.
The transferred state is a fraction, often as low as 1\%, of the virtual machine's state.
Only the changed state is transferred, saving gigabytes of network bandwidth.
\item Using a mix of trace-based and synthetic request patterns,
we evaluated {\sysname} extensively.
This work reports on the gathered results.
\item We share with the scientific community all artifacts (e.g., code and measurements) to encourage others to replicate and improve our findings.
\end{itemize}
