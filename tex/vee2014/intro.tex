\section{Introduction}

%% What is the problem?
Cloud computing, i.e., renting IT resources for indefinite time spans,
offers many benefits:
businesses and other institutions gain the flexibility to add or release resources quickly based on their computing needs.
However, we posit that current cloud providers lack flexibility for low-end customers,
especially considering interactive services.
Cloud providers should allow resources to be acquired and released even for time frames of a couple of minutes.
While the current practice of cloud providers is to charge users by the hour,
there is no fundamental technical reason preventing the adoption of more fine grained billing cycles of, say minutes, or even seconds.
{\sn} is a prototype to demonstrate the possibilities using today's technologies in this regard.

%% \cite{das2010litegreen}

%% Why is it interesting and important?
Low-end customers want to use the cloud to deploy their services,
but do not require the services to run 24/7.
The request distribution of our target services have frequent inter-arrival times of several minutes.
Disabling the services during those idle periods would allow the customer to save money by cutting costs.
On the provider side, it may enable more flexible scheduling as resources are explicitly transitioned between on and off states.

%% Why hasn't it been solved before? (Or, what's wrong with previous
%% proposed solutions? How does mine differ?)
One challenge is to reactivate the service quickly, i.e., within seconds, in response to incoming requests.
As we expect humans to interact with these services,
fast response times are essential.
Currently, it takes several minutes to start a new instance on, e.g., Amazon EC2,
while resuming from a checkpoint is not even supported by Amazon's API.
However, even existing implementations to resume execution from a checkpoint suffer a high initial delay.
This delay grows linearly with the checkpoint size, reaching into tens of seconds.
%% We build on previous work~\cite{zhang2013esx, zhang2011workingset} to cut resume times to a minimum.
{\sn} utilizes fast VM resurrection by applying results from previous work~\cite{zhang2013esx, zhang2011workingset, knauth2013fast-resume}.

%% What are the key components of my approach and results? Also
%% include any specific limitations.
With {\sn}, we propose a system architecture that enables the deployment of virtualized resources within seconds.
To this end, we modified existing open-source components, Apache and qemu/kvm,
to support the on-demand provisioning of virtual machines.
Our Apache proxy is aware of the backend server's on/off state.
The proxy enables the backend if a request for a suspended backend server arrives.
Resuming an entire operating system virtual machine within a few seconds is possible by adapting qemu's default resume implementation.
We implemented a \emph{lazy resume} mechanism that drastically cuts the time to reactivate a suspended virtual machine;
sometimes as low as 1.1 seconds.

%% \begin{enumerate}
%% \item When to transition service into idle state?
%% Deterministic timeout or prediction based?
%% \item Architecture: load balancer keeps state of which instance is up or down.
%% \item How fast can we resume service from idle state?
%% \item How much money/energy can we save?
%% \end{enumerate}

In summary, our contributions are as follows:
\begin{itemize}
\item We motivate the need and potential of truly on-demand cloud services where services are suspended even for minute-long idle intervals.
\item To resume services on incoming requests in a matter of seconds,
we lazily resume virtual machines.
We implemented lazy resume as part of the open-source emulator qemu/kvm.
\item We extended the Apache web server to proxy requests for potentially suspended origin servers.
\item We evaluated {\sn} in the context of on-demand cloud services with different storage technologies, connections, and applications.
\item We share all code and data with the scientific community at \url{http://bitbucket.org/censored}.
\end{itemize}
