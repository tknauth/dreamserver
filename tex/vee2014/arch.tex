\section{Architecture}
\label{sec:architecture}

{\sn} depends on two central building blocks:
(1)~a suspension-aware reverse proxy as well as
(2)~a mechanism to quickly resume inactive virtualized services.
Infrastructure providers routinely use operating system level virtual machines to isolate different users from each other.
Hence, when referring to virtualized services, we mean services running inside of an OS-level virtual machine.

\subsection{Reverse-Proxy}

The reverse-proxy is the central component strategically positioned between the end user and the backend services.
A reverse-proxy, as opposed to a forward-proxy, is located close to the origin servers.
%% The reverse-proxy, for example, distributes requests among a set of equivalent backend servers which would make it a load-balancing proxy.
A common use case for a reverse-proxy is load balancing by
distributing requests among a set of functionally equivalent backend servers.

\begin{figure*}
\begin{center}
  \includegraphics{figs/arch}
  \caption{General architecture.
Requests pass through a proxy (1) which is aware of the on/off state of backend servers.
If the backend server is up (2) the request immediately forwarded to it.
In case of requests to a suspended backend server (3),
the proxy triggers a resume (4) and waits until the server is up again (5) before it forwards the request (6).
The entire process is transparent to the client.}
  \label{fig:arch}
\end{center}
\end{figure*}

Another responsibility, often assumed by a reverse proxy,
is forwarding of requests based on the domain name.
This type of forwarding is required whenever multiple domain names share a single IP address.
This setup is, for example, common with web hosting providers.
As IPv4 addresses are a rare commodity,
the hosting provider can serve multiple customers with only a single IP address.
However, individual customers still want to use their own HTTP server.
This is where the reverse proxy helps.
The reverse proxy listens on port 80 of the shared IP address, i.e.,
it sees every request destined for any of the domains serviced by this IP address.
The proxy inspects the \emph{Host} field of the HTTP request header
to determine which backend server should receive the current request.
The proxy has a mapping of domain names to (private) backend IP addresses.
While all domains handled by the proxy share a single public IP address,
each backend server has a unique private IP address.

{\sn} extends this setup by making the proxy aware of the backend servers' states.
Instead of unconditionally forwarding the request,
the proxy ensures that a customer's backend server is up before forwarding the request.
To this end, the proxy keeps a small amount of soft state.
For each domain, the proxy tracks whether the backend server is running or suspended.
Bootstrapping the proxy is as easy as trying to contact each server.
Each responding server is considered awake
while the others are suspended.

%% In addition to the on/off state of backend servers,
%% the proxy also keeps tracks whether servers have been merely suspended or actually shut down.
%% This is important when it comes to reactivating the server again.
%% If the server was suspended it can be resumed,
%% skipping the potentially long initialization.
%% If, on the other hand, it was shut down the virtual machine must be started from scratch.
%% As the evaluation will show later on,
%% resuming is much faster than booting a virtual machine from scratch.
%% The swiftness of activating the backend server directly impacts the user-perceived latency.

The proxy resumes backend servers as soon as a requests for a suspended backend arrives.
Idle backend servers are suspended by a separate process independent of the proxy.
In our {\sn} prototype we implemented a process called \emph{suspender}.
The suspender runs on the same machine as the reverse proxy.
It makes direct use of the statistics collected by the proxy.
For example, for each backend server, the proxy records the time stamp of the most recent request.
Based on time stamp and the current time, we can easily determine how long the backend has been idle.
A straightforward strategy to suspend idle servers is to wait for a certain amount of time, say 30 seconds,
before suspending the backend.
Naturally, more elaborate schemes are possible, but we do not investigate them further here.
The more aggressive the strategy is, i.e., the more frequent a backend server is deactivated,
the higher the savings.
However, more frequent suspend/resume cycles, caused by more aggressive strategies,
may negatively impact the service's latency and, ultimately, user experience.

\subsection{Fast Resume}

To realize our goal of provisioning virtualized resources within only a few seconds,
we extend previous work~\cite{zhang2013esx, zhang2011workingset, knauth2013fast-resume}.
The basic idea behind speeding up the resume of virtual machines is to read only selective portions of the on-disk state into memory.
While the ideas have been previously explored and implemented in the context of a commercial virtualization product,
the fruits of that work are not available to the general public.
Hence, we decided to adapt the ideas and implement them as part of the open source virtual machine emulator \emph{qemu}.
We first present some background information on how suspend/resume is implemented in qemu,
before we detail our modifications.

\subsubsection{qemu Suspend and Resume}

By default, qemu implements an \emph{eager} resume strategy.
Before the guest continues execution,
its entire memory content is restored.
This requires to sequentially read the entire memory image from stable storage.
Hence, the time to eagerly resume the guest linearly depends on the speed at which the system is able to read the suspend image from disk~\cite{knauth2013fast-resume, zhang2013esx, zhang2011workingset}.

The suspend image's size depends on two factors: (1)~the guest's configured memory size,
and (2)~the memory's compressibility.
%Both dependencies are straightforward.
The more memory a guest is configured with, the longer it will take to resume,
as more data must be read from disk.
The compressibility of the image depends on the activity level of the applications running inside the guest.
qemu employs a very simple strategy to decrease the suspend image's size:
during the suspend process, qemu walks the guest's entire physical address space.
For each page, typically 4~KiB, qemu checks whether the page consists of a single byte pattern.
If this is the case, qemu writes the byte pattern together with the page offset to the suspend image.
Otherwise, the entire page is stored as-is on disk.
Although the check works for all possible single byte patterns,
in our tests the most prevalent compressible pages are 0-pages,
i.e., pages only containing zeros.
Due to compression, for example, a freshly booted Ubuntu 12.04 guest only takes up between 200--300~MB on disk, even though it is configured with 2~GiB of memory.
The suspend image's compressibility, however, will decrease over time as applications usually make no effort to scrub memory before releasing it.
Compressing zero-pages is qemu's heuristic approach to the problem of detecting which pages are actually used by the guest.

Resuming a virtual machine is analogous to suspending it.
Instead of walking the guest's physical address range,
qemu sequentially reads the suspend image.
Compressed pages must be ``uncompressed'' by \verb+memset()+ing the appropriate page with the byte pattern stored in the suspend image.
Uncompressed pages are simply copied from disk into memory.
An eager resume can take anywhere from a few seconds for small checkpoints up to tens of seconds for multi-gigabyte checkpoints (cf. Figure~\ref{fig:resumetime-vs-statesize}).

\subsubsection{Lazy Resume in qemu}

Our idea to break the linear relationship between suspend image size and resume time was to implement a \emph{lazy resume} mechanism to replace the default eager resume.
Instead of reading the entire memory image before resuming the execution,
we let the guest's memory access pattern decide which pages are important and should be read from disk.

qemu guests are just normal processes from the host operating system's point of view.
This means the process running the guest has access to all libraries and host OS APIs as every other process.
A natural candidate to implement lazy resume was to leverage Linux's \verb+mmap()+ system call.
\verb+mmap()+ allows a program to blend files (or part thereof) into the calling process's address space.
Whenever the process accesses memory within the mapped region,
the OS will read the appropriate block from the underlying file.
This on-demand reading behavior of \verb+mmap()+ forms the basic mechanism of our lazy resume implementation.

%% We changed the way by which qemu allocates memory for the guest's RAM with a call to \verb+mmap()+.
A potential limitation of \verb+mmap()+ is the linear relationship between the backing file's content and the memory mapped region.
That is, accessing the $n$th page of the mapped region results in reading the $n$th block from disk.
Due to compression, however,
there is no one-to-one relationship between the on-disk image and the guest's physical address space.
One possible solution is to disable compression.
Without compression, the suspend image's size is identical to the guest's configured memory size.
The downside of disabling compression is obvious:
suspending the virtual machine takes longer because more data is written to disk.
Furthermore, the suspend image would take up more disk space.
However, this need not be true.
Modern file systems, e.g., ext3, support \emph{sparse files}.

The idea behind sparse files is that their logical and actual size can be different.
For example, while a sparse file's size may be reported as 2~GiB,
it occupies significantly less than 2~GiB of actual disk space.
When reading a sparse file, the logical ``holes'' are filled with zeroes.
This aligns nicely with the observation made earlier
that 99\% of the compressible pages contain only zeroes.
Forgoing compression and utilizing sparse files,
we gain the ability to use \verb+mmap()+ to lazily resume the guest.

As was noted previously by~\citet{zhang2013esx}, a key factor to quickly restore a guest is disk access locality.
With eager restore, the image is read sequentially from disk,
resulting in maximum I/O bandwidth.
A lazy resume, where data is read selectively on access,
will likely have much less spatial locality for the disk accesses.
The reduced locality and I/O performance is most visible when resuming from conventional spinning disks,
as they have a stringent limit on the number of I/O operations per second.

After a few discouraging benchmarks involving spinning disks,
we therefore decided to abandon our pure lazy resume implementation in favor of a hybrid approach.
The hybrid approach reads the guest's working set sequentially before starting the execution.
The remaining pages are read lazily while the guest is running.
When suspending a guest, the working set is appended to the guest's image.
The working set consists of the pages accessed during the first few seconds after resuming the guest.
On resume, we map the first $n$ bytes of the image into the qemu process,
where $n$ is the guest's memory size in bytes.
We then use the \verb+remap_file_pages()+ system call to break the linear mapping.
The pages belonging to the guest's working set are mapped to their respective location as part of the contiguously mapped region.
Figure~\ref{fig:remap} illustrates this.
After remapping the working set pages, the mapped region is still contiguous in the qemu process's address space.
% However, the pages belonging to the not necessarily contiguous working set,
% are also contiguous on disk.
% Before starting the guest's execution we pre-fault the working set pages,
% to make sure they are already loaded in memory.
During the remap, we also access each page to force the host operating system to read the page from disk.

\begin{figure}
  \includegraphics{figs/remap}
  \caption{WS denotes the virtual machine's working set, which is appended to the suspend image.
The benefit of storing the working set out-of-band of the image is faster resume times from conventional spinning hard disk.}
  \label{fig:remap}
\end{figure}

While the guest's working set is resumed eagerly,
the non-working set is read lazily, i.e., only when and if it is accessed.
Lazy resume reduces the time until the guest starts executing again,
but may have a negative effect on the continued execution.
Therefore, we also added an option to trigger an eager resume of the guest's memory \emph{in the background}.
%% Pre-faulting pages in the background forces Linux to load the guest's memory image from disk.
A thread walks the guest's memory sequentially,
forcing data to be read from disk sequentially as well.
This way, we combine the best of both eager and lazy resume:
lazy resume reduces the time until the guest can service its first request after a resume,
while eagerly reading the image in the background reduces the impact on the guest's continued execution.

\subsubsection{Implementation Details}

While \verb+mmap()+ allows us to implement lazy resume easily,
\verb+mmap()+ has some interesting idiosyncrasies on Linux.
Every mapping is either shared or private
where the pages of a shared mapping may be visible and accesible by multiple processes at the same time.
As the names suggest, shared mappings allow the mapping to exist in
more than one process, e.g., to be used for shared memory communication.
While we are not interested in sharing pages among multiple processes,
the ``sharedness'' also dictates how modifications to the mapped region are propagated to the backing file.

While modification to a private mapping are never automatically written to the backing file,
a shared mapping periodically forces updates to disk.
The automatic propagation of updates to disk for shared mappings is interesting
as it essentially implements the functionality required for writing a new checkpoint.
However, we do not need \emph{periodic} updates, i.e., by default modifications are written to disk every 30 seconds.
Ideally, it should be possible to write out modification only once, when suspending the guest again.
This is, for example, possible on FreeBSD, where \verb+mmap()+ has a \emph{nosync} option.
Specifying \emph{nosync} for a file-backed memory mapped region prevents intermediate disk updates.
The updated regions of the mapping are only written back to disk when the mapping is dismantled or the user explicitly requests it.

% A possible work around in Linux may be to declare the mapping as private and use other means to determine which pages must be written back to disk on a subsequent suspend.
% On Linux dirty information for each frame is available in user-space by examining the contents of the virtual file \verb+/proc/kpageflags+.
% However, usually only root has permissions to read this file.
Regarding the issues raised above,
our prototype uses a private mapping.
We found that the additional, gratuitous disk I/O of a shared mapping noticeably degraded the guest's performance.

% \begin{itemize}
% \item \verb+mmap()+ MMAP\_SYNC vs. MMAP\_NOSYNC
% \item custom suspend file format
% \end{itemize}

% \begin{itemize}
% \item don't have to touch the kernel/kvm module
% \item done entirely in user-space
% \end{itemize}

% suspend time also decreases thanks to \verb+mmap()+; provide a figure detailing decreased suspend time
