import math
import matplotlib
matplotlib.use('PDF')
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
import pylab
import re
import glob
import sys

fontsize = 7

pylab.rc("font", family="serif")
pylab.rc("font", size=fontsize)
matplotlib.rcParams['ps.useafm'] = True
matplotlib.rcParams['pdf.use14corefonts'] = True
matplotlib.rcParams['text.usetex'] = True

width = 3.2
height = width/1.3

# pylab.rc("figure.subplot", left=(30/72.27)/width)
# pylab.rc("figure.subplot", right=(width-10/72.27)/width)
pylab.rc("figure.subplot", bottom=(25/72.27)/height)
# pylab.rc("figure.subplot", top=(height-20/72.27)/height)

matplotlib.rcParams['lines.linewidth']=0.5
matplotlib.rcParams['axes.linewidth']=0.5

import matplotlib.pyplot as plt

def parse_input(input) :
    rows = []
    for line in open(input, 'r') :
        cols = line.strip().split()
        rows.append(cols)
    return rows

def create_figure(input, storage) :
    fig = plt.figure(figsize=(width, height))
    ax1 = fig.add_subplot(111)

    rows = parse_input(input)
    # memory_sizes_mb = set([row[0] for row in rows])
    # memory_sizes_mb = map(int, memory_sizes_mb)
    # memory_sizes_mb = sorted(memory_sizes_mb)

    # filter out 8 GB instances
    # memory_sizes_mb = memory_sizes_mb[0:3]

    marker = ['^-', 'o-', 'x-', 'v-']

    legend = []

    i = 0
    for (disk, qemu) in [('hdd', 'eager'), ('hdd', 'lazy'), ('ssd', 'eager'), ('ssd', 'lazy')] :
        xs = [1, 512, 1024, 1536]
        ys = []
        for ramuse_mb in xs :
            r = rows
            r = filter(lambda r : r[0] == disk, r)
            r = filter(lambda r : r[1] == storage, r)
            r = filter(lambda r : r[2] == qemu, r)
            r = filter(lambda r : r[3] == 'resume', r)
            r = filter(lambda r : r[6] == str(ramuse_mb), r)

            print r
            assert(len(r) == 3)

            ys.append(np.average([float(row[9]) for row in r]))
            
        # legend.append(str(int(memory_size_mb)/1024)+' GB')
        legend.append('%s/%s'%(disk, qemu))
        plt.plot(xs, ys, marker[i], color='#222222', markersize=4.0, label='', clip_on=False)

        # for (x,y) in zip(xs, ys) :
        #     ax1.text(x, y, '%.1f'%(y))

        i += 1

    # http://matplotlib.sourceforge.net/mpl_examples/pylab_examples/spine_placement_demo.py
    for loc, spine in ax1.spines.iteritems():
        if loc in ['left','bottom']:
            pass
        elif loc in ['right','top']:
            spine.set_color('none') # don't draw spine
        else:
            raise ValueError('unknown spine location: %s'%loc)

    # turn off ticks where there is no spine
    ax1.xaxis.set_ticks_position('bottom')
    ax1.yaxis.set_ticks_position('left')

    plt.xlabel('dump size [MB]')
    plt.ylabel('resume time [seconds]')
    #plt.ylim([0,20])
    plt.legend(legend, loc='upper left', numpoints=1, title='')

    leg = plt.gca().get_legend()
    leg.draw_frame(False)
    ltext = leg.get_texts()
    plt.setp(ltext, fontsize=fontsize)
    plt.setp(leg.get_frame(), linewidth=0.5)

    output_fn = input.replace('.py.csv', '-%s.pdf'%(storage))
    plt.savefig(output_fn, transparent=True)

create_figure('20131011T143059-time-to-first-request.py.csv', 'direct')
create_figure('20131016T174833-time-to-first-request.py.csv', 'nbd')
