import math
import matplotlib
matplotlib.use('PDF')
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
import pylab
import sys
import subprocess as sp

fontsize = 7

pylab.rc("font", family="serif")
pylab.rc("font", size=fontsize)
matplotlib.rcParams['ps.useafm'] = True
matplotlib.rcParams['pdf.use14corefonts'] = True
matplotlib.rcParams['text.usetex'] = True

# width = 6.5
# height = 2.0
width = 3.2
height = width/3*(1.3)

pylab.rc("figure.subplot", left=(32/72.27)/width)
pylab.rc("figure.subplot", right=(width-10/72.27)/width)
pylab.rc("figure.subplot", bottom=(25/72.27)/height)
pylab.rc("figure.subplot", top=(height-5/72.27)/height)

matplotlib.rcParams['lines.linewidth']=0.5
matplotlib.rcParams['axes.linewidth']=0.5

import matplotlib.pyplot as plt

def parse_input(filename) :
    vals = []
    headers = []
    for line in open(filename) :
        line = line.strip()
        fields = line.split()
        
        if len(headers) == 0 :
            headers = fields
            continue

        kv = {}
        for (i, field) in enumerate(fields) :
            kv[headers[i]] = field
        vals.append(kv)
    return vals

def get_block_count(block_file) :
    p = sp.Popen('wc -l %s | cut -f1 -d" "'%(block_file), shell=True, stdout=sp.PIPE)
    (out, err) = p.communicate()
    return int(out.strip())

plot_order = ['lazyws']
appliance_order = ['jenkins', 'owncloud', 'rubis', 'trac', 'wordpress']
method2color={'stock' : '#222222', 'lazy' : '#555555', 'lazyws' : '#777777'}
hatch=['', '/', '\\', 'x', '']
# xs may be a list of list. This creates a multi-plot figure.
def plot_single_figure(bars, disk, storage, filetag, xlabel='', ylabel='') :
    fig = plt.figure(figsize=(width, height))
    ax1 = fig.add_subplot(111)

    # ind = 0.2 + np.arange(len(bars.values()[0]))
    # bar_width = 0.2
    # rects = []
    markers = ['o', 'v']
    xs = range(1,6)
    plt.barh(xs, bars, height=0.4, align='center',
             color='#FFFFFF', linewidth=0.5)

    (xmin, xmax) = ax1.get_xlim()
    print xmax
    format = {'resumetime' : '%.1f', 'pagefaults' : '%d'}
    for (x,y) in zip(xs, bars) :
        ax1.text(y+0.01*xmax, x, format[filetag]%(y),
                 verticalalignment='center', horizontalalignment='left')
        

    # turn off ticks where there is no spine
    ax1.xaxis.set_ticks_position('none')
    ax1.yaxis.set_ticks_position('none')
    # 1.5 because there is three bars! in general: 0.5 * number of
    # bars
    # ax1.set_xticks(np.arange(5)+0.5*0.2)
    # ax1.set_xticklabels(appliance_order)
    # ax1.set_yticks([6,12,18,24])
    ax1.set_ylim(0.5,5.5)
    # ax1.set_xlim(0, 33)
    # ax1.set_yscale('log')

    plt.xlabel(xlabel)
    plt.ylabel(ylabel)

    # http://matplotlib.sourceforge.net/mpl_examples/pylab_examples/spine_placement_demo.py
    for loc, spine in ax1.spines.iteritems():
        if loc in ['left','bottom']:
            pass
        elif loc in ['right','top']:
            spine.set_color('none') # don't draw spine
        else:
            raise ValueError('unknown spine location: %s'%loc)

    # plt.legend(bbox_to_anchor=(0.5, 1.10), loc="upper center", scatterpoints=1, ncol=3)
    # leg = plt.gca().get_legend()
    # leg.draw_frame(False)
    # ltext = leg.get_texts()
    # plt.setp(ltext, fontsize=fontsize)
    # plt.setp(leg.get_frame(), linewidth=0.5)

    plt.savefig('20131021T181830-'+filetag+'-'+disk+'-'+storage)

# rsync-1374153582
vals = parse_input('20131021T181830-time-to-first-request.py.csv')

for storage in ['direct', 'nbd'] :
    for disk in ['hdd', 'ssd'] :
        vals_filtered = filter(lambda kv : kv['disk'] == disk, vals)
        vals_filtered = filter(lambda kv : kv['storage'] == storage, vals_filtered)
        vals_filtered = filter(lambda kv : kv['action'] == 'resume', vals_filtered)
        vals_filtered = filter(lambda kv : kv['appliance'] == 'wordpress', vals_filtered)
        vals_filtered = filter(lambda kv : kv['qemu'] == 'lazyws', vals_filtered)

        ys = [float(kv['maj_pg_faults']) for kv in vals_filtered]
        plot_single_figure(ys, disk, storage, 'pagefaults', 'major page faults', 'run')
        ys = [float(kv['time_s']) for kv in vals_filtered]
        plot_single_figure(ys, disk, storage, 'resumetime', 'resume time [s]', 'run')
