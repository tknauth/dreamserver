import math
import matplotlib
matplotlib.use('PDF')
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
import pylab
import sys
import subprocess as sp

fontsize = 7

pylab.rc("font", family="serif")
pylab.rc("font", size=fontsize)
matplotlib.rcParams['ps.useafm'] = True
matplotlib.rcParams['pdf.use14corefonts'] = True
matplotlib.rcParams['text.usetex'] = True

# width = 6.5
# height = 2.0
width = 3.2
height = width/1.3

pylab.rc("figure.subplot", left=(32/72.27)/width)
pylab.rc("figure.subplot", right=(width-10/72.27)/width)
pylab.rc("figure.subplot", bottom=(25/72.27)/height)
pylab.rc("figure.subplot", top=(height-22/72.27)/height)

matplotlib.rcParams['lines.linewidth']=0.5
matplotlib.rcParams['axes.linewidth']=0.5

import matplotlib.pyplot as plt

def parse_input(filename) :
    vals = []
    headers = []
    for line in open(filename) :
        line = line.strip()
        fields = line.split()
        
        if len(headers) == 0 :
            headers = fields
            continue

        kv = {}
        for (i, field) in enumerate(fields) :
            kv[headers[i]] = field
        vals.append(kv)
    return vals

def get_block_count(block_file) :
    p = sp.Popen('wc -l %s | cut -f1 -d" "'%(block_file), shell=True, stdout=sp.PIPE)
    (out, err) = p.communicate()
    return int(out.strip())

plot_order = ['eager', 'lazy', 'lazyws']
appliance_order = ['jenkins', 'owncloud', 'rubis', 'trac', 'wordpress']
method2color={'eager' : '#888888', 'lazy' : '#DDDDDD', 'lazyws' : '#FFFFFF'}
hatch=['', '/', '\\', 'x', '']
# xs may be a list of list. This creates a multi-plot figure.
def plot_single_figure(bars, disk, storage, xlabel='', ylabel='') :
    fig = plt.figure(figsize=(width, height))
    ax1 = fig.add_subplot(111)

    ind = 0.2 + np.arange(len(bars.values()[0]))
    bar_width = 0.2
    rects = []
    for (i, k) in enumerate(plot_order) :
        plt.bar(ind+bar_width*i, bars[k], bar_width, color=method2color[k], label=k,
                hatch=hatch[i], linewidth=0.5)
        (ymin, ymax) = ax1.get_ylim()
        for (x, y) in zip(ind+bar_width*i, bars[k]) :
            ax1.text(x+0.04, y+0.015*ymax, '%.1f'%(y), rotation=270, verticalalignment='bottom')

        if k != 'eager' :
            for (x, y1, y2) in zip(ind+bar_width*i, bars['eager'], bars[k]) :
                ax1.text(x+0.04, 0.015*ymax, '%d\\%%'%((y2-y1)/y1*100), rotation=270, verticalalignment='bottom')

    # ylims = {'direct' : (0,15), 'nbd' : (0,0)}

    # turn off ticks where there is no spine
    ax1.xaxis.set_ticks_position('none')
    ax1.yaxis.set_ticks_position('none')
    # 1.5 because there is three bars! in general: 0.5 * number of
    # bars
    ax1.set_xticks(ind+1.5*bar_width)
    ax1.set_xticklabels(appliance_order)
    # ax1.set_yticks([6,12,18,24])
    # ax1.set_ylim(ylims[storage])
    # ax1.set_xlim(0, 33)
    # ax1.set_yscale('log')

    plt.xlabel(xlabel)
    plt.ylabel(ylabel)

    # http://matplotlib.sourceforge.net/mpl_examples/pylab_examples/spine_placement_demo.py
    for loc, spine in ax1.spines.iteritems():
        if loc in ['left','bottom']:
            pass
        elif loc in ['right','top']:
            spine.set_color('none') # don't draw spine
        else:
            raise ValueError('unknown spine location: %s'%loc)

    plt.legend(bbox_to_anchor=(0.5, 1.17), loc="upper center", numpoints=1, ncol=3)
    leg = plt.gca().get_legend()
    leg.draw_frame(False)
    ltext = leg.get_texts()
    plt.setp(ltext, fontsize=fontsize)
    plt.setp(leg.get_frame(), linewidth=0.5)

    plt.savefig('resumetime-bar-'+disk+'-'+storage)

# rsync-1374153582
vals = parse_input('20131021T181830-time-to-first-request.py.csv')

for storage in ['direct', 'nbd'] :
    for disk in ['hdd', 'ssd'] :
        vals_filtered = filter(lambda kv : kv['disk'] == disk, vals)
        vals_filtered = filter(lambda kv : kv['storage'] == storage, vals_filtered)
        vals_filtered = filter(lambda kv : kv['action'] == 'resume', vals_filtered)

        bars = {}
        for qemu in set([kv['qemu'] for kv in vals_filtered]) :
            bars[qemu] = filter(lambda kv : kv['qemu'] == qemu,
                                  vals_filtered)
            synctimes_per_blockfile = []
            for appliance in sorted(set([kv['appliance'] for kv in bars[qemu]])) :
                synctime_sec = filter(lambda kv : kv['appliance'] == appliance,
                                      bars[qemu])
                synctime_sec = [float(kv['time_s']) for kv in synctime_sec]
                synctime_sec = np.mean(synctime_sec)
                synctimes_per_blockfile.append(synctime_sec)
                print disk, qemu, appliance, synctime_sec
            bars[qemu] = synctimes_per_blockfile

        plot_single_figure(bars, disk, storage, '', 'time [s]')
