\section{Evaluation}
\label{sec:eval}

The evaluation focuses on answering the following questions:

\begin{enumerate}
\item How does lazy resume compare to qemu's default eager resume for varying checkpoint sizes?
\item How does lazy resume affect the guest's overall execution performance?
\item How well does lazy resume combined with working set prediction perform?
\item How does resuming from a remote storage device affect the resume time?
\end{enumerate}

Before we present our results,
we describe the hardware and software setup used during the experiments.

\subsection{Hardware Setup}

Our virtual machine host has a 4-core AMD Phenom II processor, 12~GB of main memory, and two hard drives.
One drive is a 120~GB SSD from Intel (model SSDSC2CT12),
while the second drive is a conventional 2~TB spinning disk from Samsung (model HD204UI).
The host operating system is Ubuntu 12.04.03,
while the guests run a variant of Ubuntu 12.04 adapted for virtualized environments.
In addition to the VM host, we have a second machine with a 6-core AMD Phenom II processor, 8~GB RAM,
and the same hard drive models.
The second machine serves as the networked storage target for experiments which involve networked storage.
The two machines are connected via a single switched Gigabit Ethernet line.

Some measurements involve a third machine,
which hosts the reverse-proxy (cf. Figure~\ref{fig:arch}) and runs the RUBiS workload generator.
This setup most closely resembles the overall {\sn} architecture.
%% Measuring the time it takes the requests to pass through the proxy on their way to the backend server gives the complete end-to-end latency.
%% However, as the time to successfully answer the request is dominated by the time it takes to resume the instance,
%% most of the measurements do not make use of the full setup.
We use the reverse-proxy setup only to measure the impact of lazy resume in the VM's continued execution performance (Section~\ref{sec:effect-on-continued-execution}).
Otherwise, we issue resume commands and measure request latency directly on the virtual machine host.
%% The network latency added by going through the reverse proxy is in the range of single digit milliseconds,
%% while the resume time is measured in seconds.

\subsection{Software Setup}

%% virtual machine configuration
The backend virtual machines~(VM) we use run Ubuntu 12.04 tailored for virtual environments.
Each VM contains the full RUBiS software stack, i.e.,
a MySQL database, PHP, and a web server~(nginx).
RUBiS~\cite{rubis} is a web application, modelled after eBay,
where users can buy and sell items.
The RUBiS server-side components are complemented by a client-side load generator.
Using a complete web application, instead of only static content,
gives us a more realistic picture of the expected performance.
RUBiS is one example for the kind of applications we expect customer to run on an infrastructure managed by {\sn}.

In addition to the RUBiS benchmark, we also downloaded four additional,
representative appliances from the self-proclaimed ``app store for server software'' Bitnami~\footnote{\url{http://bitnami.com/}}.
Bitnami's business is to bundle and configure popular open-source software packages.
Users download virtual machine images, sometimes also called bundles or appliances,
from Bitnami and can easily deploy them on their own infrastructure.
Very little technical knowledge is required to setup a private instance of, e.g.,
the well-known blogging platform Wordpress.
Out of the hundreds of appliances offered by Bitnami,
we chose the following four for our evaluation:

\begin{enumerate}
\item Wordpress, a popular blogging platform (version 3.6.1-2).
\item Trac, a lean software project management environment, including a bug tracker, wiki, and ticketing system (version 1.0.1-1).
\item OwnCloud, a data synchronization, file sharing, and cloud storage web application (version 5.0.12-1).
\item Jenkins, a continuous integration framework (version 1.533).
\end{enumerate}

Each appliance is distributed in VMware's native virtual machine disk format~(vmdk).
We converted it to qemu's qcow2 format before running our benchmarks.
Additionally, we installed an acpid daemon to be able to shutdown the virtual machines using libvirt.
In the compressed qcow2 format, the VM disks take up between 1.1~GB (Trac) and 1.5~GB (Jenkins).

The resume times presented in the following represent the average of five runs.
Each run is executed with a cold buffer cache on the VM host, i.e.,
all the checkpointed data is read from disk.
For scenarios involving remote storage,
we also clear the cache of the remote storage target.
Instead of wiping the host's cache entirely,
we re-mount the partition, which stores the guest's virtual disk as well as the memory checkpoint.
The unmount/mount cycle only clears the data pertaining to the virtual machine guest's from cache,
preserving other important data such as, e.g., the qemu binary and libraries.

%% \begin{itemize}
%% \item We clear the cache by unmounting and mounting the partition (as opposed to echo 1/2/3 | tee /proc/sys/.../drop\_cache)
%% \item the guest disk and memory image are stored on the same disk/partition (and are also flushed)
%% \end{itemize}

\subsection{Time to First HTTP Response}
\label{sec:eval:django-direct}

\begin{figure}
  \includegraphics{figs/20131011T143059-time-to-first-request-direct}
  \caption{Time to answer the first HTTP request.
Lower is better.
The VM hosts a Django/nginx server and uses locally attached storage.
Eager resume times increase linearly with checkpoint size
while lazy resume times stay constant.}
  \label{fig:resumetime-vs-statesize}
\end{figure}

We start our evaluation by establishing a baseline for how fast we can re-activate suspended services.
To this end, we measure how much time passes between triggering the resume and receiving the first reply.
Figure~\ref{fig:resumetime-vs-statesize} illustrates the relationship between resume time and checkpoint size.
The resumed VM runs an nginx web server together with a simple Django (Python) application talking with nginx via fast CGI.
We observe two things:
First, the time to answer an HTTP request stays approximately the same for growing checkpoint sizes.
While the default eager resume takes time proportional to the checkpoint size,
lazy resume is clearly independent of the total checkpoint size.
Second, solid state drives offer a definite advantage over spinning disks in this scenario.
The main advantage of SSDs is their capability to complete significantly more I/O operations per second~(IOPS).
While spinning disks may complete hundreds of IOPS,
solid state disks offer tens of thousands of IOPS.
For example, the data sheet of the SSD we use for our evaluation lists 25000 random reads per seconds~\footnote{\url{http://ark.intel.com/products/67287/Intel-SSD-330-Series-120GB-SATA-6Gbs-25nm-MLC}}.
With 60 random 4~KiB reads measured for the HDD~\footnote{\url{http://www.storagereview.com/samsung_spinpoint_f4eg_review_hd204ui}} that is a difference of more than two orders of magnitude.
Lazy resume benefits from the higher IOPS count,
because the previously sequential I/O pattern of eager resume is replaced by an I/O pattern with significantly less spatial locality.
In addition, the SSD's higher sequential read bandwidth is apparent in the graph.
The \emph{hdd/eager} curve has a steeper slope than the \emph{ssd/eager} curve,
indicating that the resume time increases faster for hdd/eager than it does for ssd/eager with larger checkpoints.
For example, it takes 9.5 seconds to resume a 1.5~GB checkpoint stored on an SSD.
Restoring the same checkpoint from HDD takes 15.9 seconds.

\begin{figure}
  \includegraphics{figs/20131031141316-latency-direct-cdf}
  \caption{Latency distribution for different resume strategies and storage media.
Lazy resume from HDD has a significant negative effect on the latency.
SSDs are not as sensitive to a lazy resume as HDDs.
}
  \label{fig:rubis-latency-cdf}
\end{figure}

We argue that storing memory checkpoints on SSD is a perfect example how SSDs can be selectively deployed within the data center.
Unconditionally replacing HDDs with SSDs is very costly as the capacity/price ratio is still much more favorable for traditional spinning disks.
However, even a single 128~GB SSD provides enough storage to hold the memory image of 32 virtual machines with 4~GB per VM.

%% It takes less time to sequentially read a checkpoint from SSD than from HDD because of the SSD's higher throughput.
%% Again, based on the spec sheet, the SSD offers 500 MB/s in sequential read speed,
%% compared to 140 MB/s.
%% The time to resume depends on other factors too,
%% which is why it still takes 9.5 seconds to resume from a checkpoint sized 1.5~GB and stored on an SSD.

%% {\color{red}{\bf Check if Fig 5 uses lazy or lazyws!}}

\subsection{Effect on Continued Execution}
\label{sec:effect-on-continued-execution}

The reduction in the time to answer the first HTTP request demonstrates the main benefit of lazy resume.
The improvement is enabled by deferring less important work.
Instead of a large penalty at the beginning, there are many smaller penalties distributed over a larger time frame.
The smaller penalties are page faults occurring whenever the guest accesses a region of its memory that has not been read from disk yet.

To gauge the prolonged effect of lazily resuming guests,
we measured HTTP request latencies over longer time frames (minutes).
We resumed a guest running the RUBiS application and subjected it to 7 minutes of traffic generated by RUBiS' own workload generator.
The generator models the typical behavior of users on an auction web site, e.g.,
browse items, post comments, and bid for items.
We repeated this experiment for different resume strategies and storage technologies.
Figure~\ref{fig:rubis-latency-cdf} shows the results.

%% Lazy resume effectively reduces the time to resume a virtual machine.
%% Instead of reading the entire guest's memory before starting the execution,
%% only the most crucial parts are.
%% The remaining in-memory state is read piecemeal and on-demand; stretched out over time, or maybe never.
%% The resume penalty, which eager resume pays at the beginning of resuming a VM,
%% is stretched over time for lazy resume.
%% Instead of paying a huge penalty once,
%% lazy resume trades this for many smaller penalties over time.

%% To measure the effect of continued page faults as the guest executes after a resume,
%% we used the following experimental setup:
%% We start our RUBiS virtual machine and subject it to one run of the workload generator.
%% This makes sure that the VM's page cache is warmed up.
%% Afterward, we suspend the virtual machine, clear the host's cache, and resume the VM.
%% A second run of the RUBiS workload generator follows,
%% for which we measure the request latency for every single HTTP request.
%% The request latency is recorded by the reverse proxy,
%% as opposed to the workload generator.

%% hdd/eager & 1.5 & 21.9 & 65.2 & 11630.0 \\
%% hdd/lazy & 11.3 & 187.8 & 6050.4 & 52847.0 \\
%% hdd/lazybg & 1.6 & 11.1 & 16.4 & 173.0 \\
%% ssd/eager & 1.4 & 6.9 & 11.6 & 34.5 \\
%% ssd/lazy & 1.6 & 7.6 & 11.9 & 41.6 \\
%% ssd/lazybg & 1.6 & 7.8 & 12.0 & 42.0 \\

As can be seen from the latency distribution, an entirely lazy resume from HDD severely hurts the performance.
The 95th percentile latency for hdd/eager is 65~ms, compared to 6000~ms for hdd/lazy, i.e.,
an increase of two orders of magnitude.
Hence, we also evaluated a third strategy where we resume the guest lazily, but have a background thread pre-fault the guest's memory.
This improves the performance because a page is already present in memory when a guest eventually accesses it.
This strategy is labeled \emph{lazybg}.
The positive effect is very noticeable as the latency distribution for hdd/lazybg is much improved compared to hdd/lazy.
When storing the checkpoint on SSD,
there is not such a big difference between the strategies.
Even an entirely lazy resume only minimally changes the latency distribution.
For example, the 99th percentile for ssd/eager is 35~ms,
while for ssd/lazy it is 42~ms.

%% Figure~\ref{fig:rubis-latency-cdf} shows four cumulative distribution functions~(CDF) of response latencies for the above setup.
%% We show one CDF for each of the combinations of eager/lazy resume and HDD/SSD backed VMs.
%% {\color{red}
%% The experimental data confirms our intuition about the impact of lazy resume on the continued execution.
%% The curves for eager resume (hdd/eager and ssd/eager) show a slightly better latency distribution than for lazy resume,
%% especially so for latencies above 10 milliseconds.
%% In the 10~ms to 200~ms range, eagerly resuming from SSD shows the best latency distribution, i.e.,
%% the CDF curve ``is ahead'' of all the others;
%% followed by eagerly resuming from HDD.
%% Third comes lazily resuming from SSD,
%% while lazily resuming from HDD has the least favorable latency distribution.
%% This perception is confirmed when we look at the $n$th percentile latency which we list in Table~\ref{tab:percentile-rubis-hdd}.

%% As noted previously by others too,
%% changes and optimizations to a system typically have a greater affect on the tail latency,
%% for better or worse, than they do on the average.
%% This also seems to be the case here.

%% While the effect of lazy resume is visible in our measurements,
%% we argue that it is within acceptable bounds.
%% Studies have shown that end-user perceived latencies when interacting with web services routinely exceed one second or more~\cite{meenan2013howfast}.
%% A few additional milliseconds will hardly matter in these cases.
%% \texttt{TODO: plot latency of VM with cold cache.
%% would be good to demonstrate benefit of start vs resume guest.}
%% }

\begin{figure}
  \includegraphics{figs/20131016T174833-time-to-first-request-nbd}
  \caption{Time to answer the first HTTP request.
The VM hosts a Django/nginx server.
The checkpoint and guest disk are stored on the network.
Resume times are slightly higher than for locally attached storage.
%% Lazy resume is independent of checkpoint size.
}
  \label{fig:resumetime-vs-statesize-remote-nbd}
\end{figure}

\subsection{Resume from Remote Storage}

When it comes to designing a compute infrastructure, the designer has to choose between whether to deploy direct attached or remote storage.
Direct attached storage~(DAS) has the benefit of typically being cheaper than remote storage solutions.
No additional network capacity must be allocated to serve reads and writes to stable storage.
In the context of virtual machines and their migration, however,
a centralized storage pool makes VM migrations easier.
Instead of transferring (potentially) hundreds of gigabytes of on-disk data between the migration source and destination,
only the comparatively small in-memory state must be copied.
The reduced migration overhead stemming from a centralized storage architecture allows to migrate more often and at a lower cost.

%% \begin{table}
%%   \begin{center}
%%     \begin{tabular}{r|r|r|r|r}
%% Percentile & 50 & 90 & 95 & 99\\
%%            & [ms] & [ms] & [ms] & [ms]\\
%%       \hline
%% hdd/eager & 1.4 & 6.9 & 11.8 & 44.8 \\
%% hdd/lazy & 11.1 & 198.9 & 6106.1 & 39925.9 \\
%% hdd/lazybg & 1.6 & 8.2 & 12.7 & 66.5 \\
%% ssd/eager & 1.4 & 6.9 & 11.7 & 43.7 \\
%% ssd/lazy & 1.6 & 7.8 & 12.2 & 44.0 \\
%% ssd/lazybg & 1.6 & 9.7 & 13.3 & 66.4 \\
%%     \end{tabular}
%%   \end{center}
%%   \caption{Latency percentiles for different resume strategies when resuming over the network.}
%%   \label{tab:percentile-rubis-hdd}
%% \end{table}

Due to the attractive benefits of a centralized storage pool,
we also investigate lazily resuming a VM over the network.
Among the many technologies available to provide remotely accessible storage,
e.g., AoE, iSCSI, and nbd,
we chose the latter for our evaluation.
The network block device~(nbd) was introduced into the Linux kernel in 1998 and is considered a mature technology.
%% One noteworthy fact about nbd is its use of TCP as a transport protocol.
%% Lighter alternatives exist, e.g., AoE uses plain Ethernet frames,
%% but we do not expect the higher packet processing overhead to noticeably impact our results.

\begin{figure}
  \includegraphics{figs/20131031141316-latency-nbd-cdf}
  \caption{Latency distribution for RUBiS workload.
Checkpoint and VM disk are accessed over the network.
Lazy resume negatively affects the guest's performance.}
  \label{fig:rubis-latency-cdf-nbd}
\end{figure}

We repeated the same set of experiments as described in Section~\ref{sec:eval:django-direct}.
The only difference is our use of remotely attached storage instead of directly attached SATA disks.
As can be seen in Figure~\ref{fig:resumetime-vs-statesize-remote-nbd},
resuming our nginx/Django service takes 1.3 seconds in the best case, i.e.,
when the state is stored on SSD.
As a general observation, we find that resume times increase across the board as we move from directly attached disks to networked storage.
This was expected because of the additional hardware- and software components introduced into the setup.
We also notice that resuming large checkpoints, e.g., 1.5~GB,
takes proportionally longer over the network because the networking equipment caps the bandwidth at 1 Gigabit per second.
With directly attached disks, the throughput, even for spinning disks,
was higher than what a single Gigabit link is able to transfer.
This is also the reason why the curves for eager resume diverge less with increasing checkpoint sizes than they did for directly attached storage.
At 1.5~GB, the difference between eagerly resuming from HDD and SSD is only 2.9 seconds,
compared to 6.4 seconds for directly attached disks.
Following the same reasoning, lazy resume is even more worthwhile, relatively speaking,
for networked storage.
Lazy resume from HDD only takes 3.4 seconds compared to 20.5 seconds for eager resume;
less than 1/5 of the time.
Lazy resume from an SSD is even more impressive:
it takes less than 1/10 of the time of eager resume.

\begin{figure}
  \includegraphics{figs/resumetime-bar-hdd-direct}
  \caption{Resume time for five appliances. Guests are stored on local HDD. Lower is better.}
  \label{fig:resumetime-bar-hdd-direct}
\end{figure}

Looking at the continued execution performance in Figure~\ref{fig:rubis-latency-cdf-nbd}, we notice that a lazy resume from HDD greatly increases the request processing latency.
The other strategies all show a similar performance.
The 99th percentile latency for all strategies except hdd/lazy is around 50~ms.
This is even better than for the hdd/eager and hdd/lazybg with direct attached storage.
We attribute this to the fact that the nbd server does a better job at buffering reads and writes than Linux for directly attached storage.
Confirming this assumption would need a more thorough investigation.

\subsection{Resuming Appliances}

So far, our evaluation focused exclusively on the web application RUBiS.
Although web applications typically consist of three components,
web server, application logic, and database,
there is enough variety to warrant a look at other applications.
In addition to RUBiS, we investigated the following four applications:
Wordpress, OwnCloud, Trac, and Jenkins.
The measurements show the time passed between resuming the application and receiving a successful answer for the login page.
%% The four applications are more demanding than our previous RUBiS example in the sense that we now also look at the difference between the two resume strategies presented in the implementation section.
We present results for the default eager resume, lazy resume, and lazy working set (\verb+lazyws+) resume.
Lazy working set resume reads the guest's working set eagerly,
while the non-working set pages are read lazily.
%% As will become apparent shortly,
%% resuming the appliances lazily from HDD actually prolongs the resume time compared to eager resume.
As before, we evaluate different disk types (HDD/SSD) and connection technologies (direct/networked storage).

\subsubsection{Appliances on Direct Attached Storage}

%% Before we discuss how lazy resume compares to eager resume,
%% let us first look at the baseline resume time for each appliance.
We start by looking at the eager resume times for each appliance~(Figure~\ref{fig:resumetime-bar-hdd-direct}).
%% The difference between the appliances is already apparent:
While it takes longest to eagerly resume the Jenkins appliance (5.9 seconds),
RUBiS, Trac, and Wordpress have similar resume times, around 4.5 seconds.
OwnCloud resumes fastest at 4.0 seconds.
As the eager resume time is closely related to the checkpoint size,
we also list the checkpoint size for each appliance:
Trac 230~MB, OwnCloud 238~MB, RUBiS 273~MB, Wordpress 314~MB, and Jenkins 487~MB.

% \begin{table}[h!]
% %% \begin{center}
% \begin{tabular}{r|r}
% Trac & 230 MB\\
% OwnCloud & 238 MB\\
% RUBiS & 273 MB\\
% Wordpress & 314 MB\\
% Jenkins & 487 MB\\
% \end{tabular}
% %% \end{center}
% \end{table}

% \begin{table}
% \begin{tabular}{r|r|r|r|r}
% Trac & OwnCloud & RUBiS & Wordpress & Jenkins\\
% \hline
% 230 & 238 & 273 & 314 & 487
% \end{tabular}
% \end{table}

The checkpoint size explains why Jenkins takes longest to resume,
as it has the largest checkpoint size (by a margin of 170~MB).
Also note that this is a fresh instance of each appliance.
Over time, the checkpoint size will grow as a result of normal appliance operation.
Larger checkpoints translate into longer resume times and the savings due to lazy resume will also increase.
%% The savings of the two lazy resume variants presented next,
%% will be even higher for guests that have accumulated more in-memory state.

\begin{figure}
  \includegraphics{figs/resumetime-bar-ssd-direct}
  \caption{Resume time for five appliances. Guests are stored on local SSD. Lower is better.}
  \label{fig:resumetime-bar-ssd-direct}
\end{figure}

Switching from eager to lazy resume on HDDs, in Figure~\ref{fig:resumetime-bar-hdd-direct}, we see
that it actually takes longer to resume.
Although lazy resume drastically reduces the volume of data that must be read from disk,
the spatial non-locality of the data reverses the gains we achieve by reducing the volume.
A naive lazy resume more than doubles the resume time for the Jenkins, OwnCloud, and Wordpress appliance,
clearly not the desired result.
However, naive lazy resume from SSD (Figure~\ref{fig:resumetime-bar-ssd-direct}) decreases the resume time for each appliance.
For example, the previous three appliances, which saw the highest increase for lazy resume,
have their resume times cut by almost 40\%.
In other cases, e.g., RUBiS, a naive resume from SSD only decreases the resume time marginally by 0.3 seconds.

However, even SSDs benefit from sequential access.
With lazy working set resume (\verb+lazyws+), we combined the best of both eager and lazy resume.
Lazy working set resume improves the initial response time for each appliance regardless of the disk type.
On spinning disks, the reduction varies between 20\% for Jenkins (4.7 seconds),
up to 57\% for RUBiS (1.9 seconds).
For appliances on SSD, the lazy working set resume drops by up to 60\%,
e.g., resuming Wordpress drops from 4.8 seconds to 1.7 seconds.

%% \begin{itemize}
%% \item measured working set size; working set size also determines how
%%   by how much the checkpoint size increases. That information is
%%   basically stored twice.
%% \end{itemize}

\subsubsection{Appliances on Network Attached Storage}

The results for resuming from spinning disks are shown in Figure~\ref{fig:resumetime-bar-hdd-nbd}.
Compared to the resume times for locally attached spinning disks,
the resume times are longer across all appliances and resume strategies.
The remote storage adds between 0.7 (OwnCloud) and 0.9 seconds (Jenkins) to the resume time.
As noticed before, switching to a strategy where the entire memory is read lazily
actually increases the resume time.
This negative effect is only amplified by remotely attached storage.
All appliances take longer to resume lazily compared to qemu's standard eager resume.
The Jenkins appliance experiences the worst case increase among the five investigated appliances.
Its resume time increases from 6.8 seconds to 16.9 seconds,
demonstrating that a simple lazy resume has a detrimental effect.
However, even in the best case scenario, Trac,
the resume time still increases by 1.7 seconds to 7.0 seconds.

%% {\color{red}Text about lazy working set resume.
%% Must somehow incorporate the fact that working set resume shows variable performance.
%% First resume is slow, second resume slowish, etc.
%% Stabilizes after 3-4 suspend/resume cycles.
%% Which numbers do we show? Average? Minimum? Range?
%% }

\begin{figure}
  \includegraphics{figs/resumetime-bar-hdd-nbd}
  \caption{Resume time for five appliances. Guests are stored on remote HDD. Lower is better.}
  \label{fig:resumetime-bar-hdd-nbd}
\end{figure}

Switching to \verb+lazyws+ positively impacts the resume time.
VMs resume faster with the lazy working set strategy than they do with eager resume.
%% The best improvement, in relative terms is for the RUBiS guest,
RUBiS benefits most, in relative terms:
its time to resume is more than halved from 5.3 seconds (eager) to 2.5 seconds (\verb+lazyws+).
As we use average values over five runs, it hides the fact that the \verb+lazyws+ strategy actually improves over time.
The working set estimation improves over time
as more pages are added to the working set.
For example, while the initial resume time for Jenkins is 9.7 seconds for hdd/nbd,
subsequent resumes finish in less than 5 seconds.

Figure~\ref{fig:resumetime-jenkins-hdd-direct} illustrates how the resume time for a single guest changes over time.
As the working set estimation becomes better,
more pages are added to the working set,
the time drops from initially 7.3 seconds to 1.5 seconds.
Related to the improved resume time is the number of major page faults serviced by the guest.
As with the resume time,
we expect the number of major page faults to decrease over time.
As more pages are added to the working set,
the fewer page accesses will result in a major page fault.
Major page faults are expensive because data must be read from disk.
Hence, resume time is correlated with the number of major page faults.
Figure~\ref{fig:pagefaults-jenkins-hdd-direct} shows the number of major page faults for subsequent resumes.
Each data point shows the number of major page faults serviced just after the first answered request.
The number drops from a maximum of 541 page faults to just 32.
It increases again for the 5th run as there is inherent non-determinism in the resume process to prevent perfect prediction.
Nonetheless, working set prediction reduces the resume time significantly.
It may be worthwhile to experiment with other prediction techniques~\cite{zhang2011workingset}, but we found the current technique sufficiently good for our workloads.

\begin{figure}
  \includegraphics{figs/resumetime-bar-ssd-nbd}
  \caption{Resume time for five appliances. Guests are stored on remote SSD. Lower is better.}
  \label{fig:resumetime-bar-ssd-nbd}
\end{figure}

\begin{figure}
  \includegraphics{figs/20131021T181830-resumetime-hdd-direct}
  \caption{Resume time for subsequent runs.
The first resume is worst as the working set does not yet contain all relevant pages.}
  \label{fig:resumetime-jenkins-hdd-direct}
\end{figure}

One interesting question that we leave for future work to address
is how to best curate the working set.
Pages must be evicted from the resume working set eventually to keep it from growing indefinitely.
We have to trade-off working set size with the time it takes to read the working set from disk.
Without a purging mechanism,
the working set may degenerate to the point where all the guest's pages belong to its working set.
Heuristic working set estimation is a well studied problem in computer science~\cite{denning1968working}.
Improved estimation will decrease resume times even further~\cite{jiang2005clock}.
We leave investigating more advanced prediction techniques for future work.

\begin{figure}
  \includegraphics{figs/20131021T181830-pagefaults-hdd-direct}
  \caption{Number of page faults serviced by the guests during the first few seconds after resume.
Subsequent runs have fewer page faults as a result of a growing working set.}
  \label{fig:pagefaults-jenkins-hdd-direct}
\end{figure}

\begin{table}
  \begin{center}
    \begin{tabular}{r|r}
      Appliance  & Working Set Size [MB]\\
      \hline
       Wordpress & 40 -- 55  \\
       Trac      & 45 -- 140 \\
       OwnCloud  & 28 -- 40  \\
       Jenkins   & 27 -- 135 \\
       RUBiS     & 33 -- 67  \\
    \end{tabular}
  \end{center}
  \caption{Typical working set sizes for the investigated appliances.}
  \label{tab:working-set-size}
\end{table}
