import collections
import math
import matplotlib
matplotlib.use('PDF')
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
import pylab
import re
import fileinput
import numpy as np
from pylab import *
import scipy.stats as stats

fontsize = 7

pylab.rc("font", family="serif")
pylab.rc("font", size=fontsize)
pylab.rc("font", weight="medium")

width = 3.2
height = width/1.3

pylab.rc("figure.subplot", left=(30/72.27)/width)
# pylab.rc("figure.subplot", right=(width-10/72.27)/width)
pylab.rc("figure.subplot", bottom=(25/72.27)/height)
# pylab.rc("figure.subplot", top=(height-20/72.27)/height)

matplotlib.rcParams['lines.linewidth']=0.5
matplotlib.rcParams['axes.linewidth']=0.5

import matplotlib.pyplot as plt

fig = plt.figure(figsize=(width, height))
ax1 = fig.add_subplot(111)

def plot_cdf(file, linestyle, color) :
    d = collections.defaultdict(float)
    for line in open(file, 'r') :
        d[float(line.strip())] += 1

    xs = sort(d.keys())
    ys1 = [d[x] for x in xs]
    ys1 = np.array(ys1)

    all = []
    for k in xs :
        all += [k]*int(d[k])
    # print int(sum(d.values())), len(all)
    assert len(all) == int(sum(d.values()))
    # print all[0:10], all[-11:-1]
    for secs in [1.0, 10.0, 100.0] :
        print file, secs, stats.percentileofscore(all, secs, kind='weak')
    
    ys1 /= np.sum(ys1)
    # print 'ys1', ys1[0:10], ys1[-11:-1]
    # print 'xs', xs[0:10], xs[-11:-1]
    cys1 = np.cumsum(ys1)
    # print 'cys1', cys1[0:10], cys1[-11:-1]

    plt.plot(xs, cys1, linestyle, color=color)

plot_cdf('../../../data/wwwse.inf.tu-dresden.de-access_log-interarrival', '-', '#222222')
plot_cdf('../../../data/usask_access_log-interarrival', '--', '#555555')

plot_cdf('../../../data/calgary_access_log-interarrival', '-.', '#888888')

# http://matplotlib.sourceforge.net/mpl_examples/pylab_examples/spine_placement_demo.py
for loc, spine in ax1.spines.iteritems():
    if loc in ['left','bottom']:
        pass
    elif loc in ['right','top']:
        spine.set_color('none') # don't draw spine
    else:
        raise ValueError('unknown spine location: %s'%loc)

# turn off ticks where there is no spine
ax1.xaxis.set_ticks_position('bottom')
ax1.yaxis.set_ticks_position('left')

ax1.set_xscale('log')

plt.xlabel('Inter-arrival time [seconds]')
plt.ylabel('CDF')
plt.legend(['Dresden', 'Saskatchewan', 'Calgary'], loc=4)
leg = plt.gca().get_legend()
ltext = leg.get_texts()
plt.setp(ltext, fontsize=fontsize)
leg.draw_frame(False)

plt.savefig('interarrival-cdf')

# plot(X, Y)
# plot(X,CY,'r--')
