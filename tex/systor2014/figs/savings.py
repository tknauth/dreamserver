import collections
import itertools
import math
import matplotlib
import matplotlib.ticker
matplotlib.use('PDF')
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
import pylab
import re
import fileinput
import numpy as np
from pylab import *

fontsize = 7

pylab.rc("font", family="serif")
pylab.rc("font", size=fontsize)
pylab.rc("font", weight="medium")

width = 7.0
height = 2

pylab.rc("figure.subplot", left=(30/72.27)/width)
# pylab.rc("figure.subplot", right=(width-10/72.27)/width)
pylab.rc("figure.subplot", bottom=(25/72.27)/height)
# pylab.rc("figure.subplot", top=(height-20/72.27)/height)

matplotlib.rcParams['lines.linewidth']=0.5
matplotlib.rcParams['axes.linewidth']=0.5

import matplotlib.pyplot as plt

fig = plt.figure(figsize=(width, height))
ax1 = fig.add_subplot(111)

xs = []
ys = []
with open('savings.csv') as f :
    for line in f.readlines() :
        line.strip()
        (x, y) = line.split()
        xs.append(x)
        ys.append(y)


plt.plot(xrange(24*60*60), [80]*24*60*60, color='#000000', linewidth=1.0)

colors = {'eager' : '#e0ecf4', 'lazy' : '#9ebcda', 'lazyws' : '#8856a7'}

plt.plot(xs, ys, color=colors['lazyws'])

# http://matplotlib.sourceforge.net/mpl_examples/pylab_examples/spine_placement_demo.py
for loc, spine in ax1.spines.iteritems():
    if loc in ['left','bottom']:
        pass
    elif loc in ['right','top']:
        spine.set_color('none') # don't draw spine
    else:
        raise ValueError('unknown spine location: %s'%loc)

# turn off ticks where there is no spine
ax1.xaxis.set_ticks_position('bottom')
ax1.yaxis.set_ticks_position('left')

ax1.get_xaxis().set_major_formatter(matplotlib.ticker.FuncFormatter(lambda x, p : '%d'%(x/60/60)))
ax1.xaxis.set_major_locator(matplotlib.ticker.FixedLocator([x*2*60*60 for x in range(13)]))

# ax1.set_xscale('log')
plt.ylim(0,80)
plt.xlim(0,24*60*60-1)

plt.xlabel('Time of day [hour]')
plt.ylabel('\# VMs')

# Put label on figure instead of a legend.
ax1.text(3*60*60, 72, 'always on', fontsize=fontsize)
ax1.text(2*60*60, 18, '60s timeout', fontsize=fontsize)

# plt.legend(['always-on', '60s timeout'], loc=4)
# leg = plt.gca().get_legend()
# ltext = leg.get_texts()
# plt.setp(ltext, fontsize=fontsize)
# leg.draw_frame(False)

plt.savefig('savings')

# plot(X, Y)
# plot(X,CY,'r--')
