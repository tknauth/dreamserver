import math
import matplotlib
matplotlib.use('PDF')
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
import pylab

fontsize = 7

pylab.rc("font", family="serif")
pylab.rc("font", size=fontsize)

width = 3.2
height = width/1.3

# pylab.rc("figure.subplot", left=(30/72.27)/width)
# pylab.rc("figure.subplot", right=(width-10/72.27)/width)
pylab.rc("figure.subplot", bottom=(25/72.27)/height)
# pylab.rc("figure.subplot", top=(height-20/72.27)/height)

matplotlib.rcParams['lines.linewidth']=0.5
matplotlib.rcParams['axes.linewidth']=0.5

import matplotlib.pyplot as plt

fig = plt.figure(figsize=(width, height))
ax1 = fig.add_subplot(111)

# fns=['wwwse-thresh-downtime.csv']
# fs=map(open, fns)
# # header=f.readline().strip().split()

# BEGIN {
#     thresh=$thresh;sum=0;transitions=0} {if (\$2>thresh) {sum+=(\$2-thresh)*\$1; transistions+=}} END{print thresh, sum}"

def plot_single_site(filename, trace_length_days, marker) :
    data = []
    for line in open(filename, 'r') :
        data.append(map(int, line.strip().split()))

    xs = []
    ys1 = []
    ys2 = []
    fraction_of_total_requests = []
    for thresh in range(60,480+1,60) :
        accumulated_time_sec = 0
        transistions = 0
        total_requests = 0
        for (occurrences, interarrivaltime) in data :
            total_requests += occurrences
            if interarrivaltime > thresh :
                accumulated_time_sec += (interarrivaltime - thresh) * occurrences
                transistions += occurrences
        xs.append(thresh)
        ys1.append(accumulated_time_sec)
        ys2.append(transistions)
        fraction_of_total_requests.append(transistions/float(total_requests))

    print xs
    print ys1
    print ys2
    print map(lambda x : '%2.4f'%(x), fraction_of_total_requests)

    color=['#222222', '#555555', '#999999']

    ys1_in_days = map(lambda x : x/float(60*60*24), ys1)
    ys1_relative = map(lambda x : x/float(trace_length_days), ys1_in_days)
    plt.plot(xs, ys1_relative, linestyle='', color=color[0], marker=marker)

name2file = {'Dresden' : 'wwwse.inf.tu-dresden.de-access_log-interarrival-hist',
             'Saskatchewan' : 'usask_access_log-interarrival-hist',
             'Calgary' : 'calgary_access_log-interarrival-hist'}
name2marker = {'Dresden' : 'v',
               'Saskatchewan' : 'o',
               'Calgary' : 's'}

trace_names = ['Dresden', 'Saskatchewan', 'Calgary']
name2trace_length= {'Dresden' : 318.0, 'Saskatchewan' : 214.0, 'Calgary' : 353.0}

for name in trace_names :
    plot_single_site('../../../data/'+name2file[name], name2trace_length[name], marker=name2marker[name])

# http://matplotlib.sourceforge.net/mpl_examples/pylab_examples/spine_placement_demo.py
for loc, spine in ax1.spines.iteritems():
    if loc in ['left','bottom']:
        pass
    elif loc in ['right','top']:
        spine.set_color('none') # don't draw spine
    else:
        raise ValueError('unknown spine location: %s'%loc)

# turn off ticks where there is no spine
ax1.xaxis.set_ticks_position('bottom')
ax1.yaxis.set_ticks_position('left')

plt.xlabel('shutdown timeout [seconds]')
plt.ylabel('fraction of idle time over entire trace')

# ax2 = ax1.twinx()
# ax2.plot(xs, ys2, 'r.')
# ax2.set_ylabel('transitions')

# anon_trace_names = ['AnonymousCity']+trace_names[1:]
plt.legend(trace_names, loc=1, numpoints=1)
leg = plt.gca().get_legend()
leg.draw_frame(False)
ltext = leg.get_texts()
plt.setp(ltext, fontsize=fontsize)
# plt.setp(leg.get_frame(), linewidth=0.5)
plt.savefig('timeout-vs-savings')
