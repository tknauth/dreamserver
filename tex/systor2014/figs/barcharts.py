import math
import matplotlib
matplotlib.use('PDF')
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
import pylab
import sys
import subprocess as sp
import os.path

fontsize = 7

pylab.rc("font", family="serif")
pylab.rc("font", size=fontsize)
matplotlib.rcParams['ps.useafm'] = True
matplotlib.rcParams['pdf.use14corefonts'] = True
matplotlib.rcParams['text.usetex'] = True

# width = 6.5
# height = 2.0
width = 3.2
height = width/1.3

pylab.rc("figure.subplot", left=(32/72.27)/width)
pylab.rc("figure.subplot", right=(width-10/72.27)/width)
pylab.rc("figure.subplot", bottom=(25/72.27)/height)
pylab.rc("figure.subplot", top=(height-22/72.27)/height)

matplotlib.rcParams['lines.linewidth']=0.5
matplotlib.rcParams['axes.linewidth']=0.5

import matplotlib.pyplot as plt

def parse_input(filename) :
    vals = []
    headers = []
    for line in open(filename) :
        line = line.strip()
        fields = line.split()
        
        if len(headers) == 0 :
            headers = fields
            continue

        kv = {}
        for (i, field) in enumerate(fields) :
            kv[headers[i]] = field
        vals.append(kv)
    return vals

def get_block_count(block_file) :
    p = sp.Popen('wc -l %s | cut -f1 -d" "'%(block_file), shell=True, stdout=sp.PIPE)
    (out, err) = p.communicate()
    return int(out.strip())

def remove_error_bars_from_legend(ax1) :
    handles, labels = ax1.get_legend_handles_labels()
    final_handles = []
    final_labels = []
    for (handle, label) in zip(handles, labels) :
        if label == 'None' : continue
        final_handles.append(handle)
        final_labels.append(label)
    return (final_handles, final_labels)

def replace_list_item(l, old_item, new_item) :
    result = []
    for elem in l :
        if elem == old_item : result.append(new_item)
        else : result.append(elem)
    return result

plot_order = ['stock', 'lazy', 'lazyws']
appliances = ['django', 'jenkins', 'owncloud', 'rubis', 'trac', 'wordpress', 'mediawiki']
method2color={'stock' : '#e0ecf4', 'lazy' : '#9ebcda', 'lazyws' : '#8856a7'}
hatch=['', '/', '\\', 'x', '']
# xs may be a list of list. This creates a multi-plot figure.
def plot_single_figure(bars, errors, disk, storage, xlabel='', ylabel='') :
    fig = plt.figure(figsize=(width, height))
    ax1 = fig.add_subplot(111)

    ind = 0.2 + np.arange(len(bars.values()[0]))
    bar_width = 0.25
    rects = []
    for (i, k) in enumerate(plot_order) :
        label = k
        if k == 'stock' : label = 'eager'
        plt.bar(ind+bar_width*i, bars[k], bar_width, color=method2color[k], label=label,
                linewidth=0.5,
                yerr=errors[k], ecolor='black', capsize=2) # parameters related to error bars

        (ymin, ymax) = ax1.get_ylim()
        for (x, y, yerr) in zip(ind+bar_width*i, bars[k], errors[k]) :
            ax1.text(x+0.04, y+0.015*ymax+yerr, '%.1f'%(y), rotation=270, verticalalignment='bottom')

        # if k != 'stock' :
        #     for (x, y1, y2) in zip(ind+bar_width*i, bars['stock'], bars[k]) :
        #         ax1.text(x+0.03, 0.015*ymax, '%d\\%%'%((y2-y1)/y1*100), rotation=270, verticalalignment='bottom')

    ylims = {('direct','hdd') : (0,19),
             ('nbd', 'hdd') : (0,24),
             ('direct', 'ssd') : (0,5),
             ('nbd', 'ssd') : (0,7)}

    # turn off ticks where there is no spine
    ax1.xaxis.set_ticks_position('none')
    ax1.yaxis.set_ticks_position('none')
    # 1.5 because there is three bars! in general: 0.5 * number of
    # bars
    ax1.set_xticks(ind+1.5*bar_width)
    app2xticklabel = {'django' : 'Django', 'jenkins' : 'Jen-\nkins',
                      'mediawiki' : 'Media-\nwiki', 'owncloud' : 'Own-\ncloud',
                      'rubis' : 'Rubis', 'trac' : 'Trac',
                      'wordpress' : 'Word-\npress'}
    ax1.set_xticklabels([app2xticklabel[app] for app in sorted(appliances)])
    # ax1.set_yticks([6,12,18,24])
    ax1.set_ylim(ylims[(storage,disk)])
    # ax1.set_xlim(0, 33)
    # ax1.set_yscale('log')

    plt.xlabel(xlabel)
    plt.ylabel(ylabel)

    # http://matplotlib.sourceforge.net/mpl_examples/pylab_examples/spine_placement_demo.py
    for loc, spine in ax1.spines.iteritems():
        if loc in ['left','bottom']:
            pass
        elif loc in ['right','top']:
            spine.set_color('none') # don't draw spine
        else:
            raise ValueError('unknown spine location: %s'%loc)

    # Remove legend entries related to error bars
    # cf. http://stackoverflow.com/questions/14297714/matplotlib-dont-show-errorbars-in-legend
    (handles, labels) = remove_error_bars_from_legend(ax1)
    # rename "lazyws" to "hybrid"
    labels = replace_list_item(labels, 'lazyws', 'hybrid')
    plt.legend(handles, labels,
               bbox_to_anchor=(0.5, 1.17), loc="upper center", numpoints=1, ncol=3)
    leg = plt.gca().get_legend()
    leg.draw_frame(False)
    ltext = leg.get_texts()
    plt.setp(ltext, fontsize=fontsize)
    plt.setp(leg.get_frame(), linewidth=0.5)

    plt.savefig('resumetime-bar-'+disk+'-'+storage)

filename = '20140212T172643-time-to-first-request.py.csv'
if len(sys.argv) > 1 and os.path.exists(sys.argv[1]) :
    filename = sys.argv[1]
vals = parse_input(filename)

for storage in ['direct', 'nbd'] :
    for disk in ['hdd', 'ssd'] :
        vals_filtered = filter(lambda kv : kv['disk'] == disk, vals)
        vals_filtered = filter(lambda kv : kv['storage'] == storage, vals_filtered)
        vals_filtered = filter(lambda kv : kv['action'] == 'resume', vals_filtered)

        bars = {}
        errors = {}
        for qemu in set([kv['qemu'] for kv in vals_filtered]) :
            bars[qemu] = filter(lambda kv : kv['qemu'] == qemu,
                                  vals_filtered)
            synctimes_per_appliance = []
            errors_per_appliance = []
            for appliance in sorted(set([kv['appliance'] for kv in bars[qemu]])) :
                synctime_sec = filter(lambda kv : kv['appliance'] == appliance,
                                      bars[qemu])
                synctime_sec = [float(kv['time_s']) for kv in synctime_sec]
                
                synctimes_per_appliance.append(np.mean(synctime_sec))
                errors_per_appliance.append(np.std(synctime_sec))

                print disk, qemu, appliance, synctimes_per_appliance[-1], errors_per_appliance[-1]
            bars[qemu] = synctimes_per_appliance
            errors[qemu] = errors_per_appliance

        plot_single_figure(bars, errors, disk, storage, '', 'Time [s]')
