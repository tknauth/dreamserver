import collections
import math
import matplotlib
matplotlib.use('PDF')
from mpl_toolkits.mplot3d import Axes3D
import pylab
import re
import fileinput
import numpy as np
from pylab import *
import time
import matplotlib.mlab as mlab
import sys
import operator

fontsize = 7

pylab.rc("font", family="serif")
pylab.rc("font", size=fontsize)
pylab.rc("font", weight="medium")
matplotlib.rcParams['ps.useafm'] = True
matplotlib.rcParams['pdf.use14corefonts'] = True
matplotlib.rcParams['text.usetex'] = True

width = 3.2
height = width/1.3

pylab.rc("figure.subplot", left=(33/72.27)/width)
# pylab.rc("figure.subplot", right=(width-10/72.27)/width)
pylab.rc("figure.subplot", bottom=(25/72.27)/height)
# pylab.rc("figure.subplot", top=(height-20/72.27)/height)

matplotlib.rcParams['lines.linewidth']=0.5
matplotlib.rcParams['axes.linewidth']=0.5

import matplotlib.pyplot as plt

colors = {'eager' : '#9ebcda', 'lazy' : '#e0ecf4', 'lazyws' : '#8856a7', 'lazybg' : '#8856a7'}
attr_order = [(i,j) for i in ['hdd', 'ssd'] for j in ['eager', 'lazybg']]

def parse_access_log(fname) :
    f = open(fname, 'r')

    time_offset = None
    xs = []
    ys = []

    for line in f :
        line = line.strip()
        fields = line.split()

        ys.append(float(fields[11]))
        time_string = fields[0]+' '+fields[1]
        time_string = time_string.strip('"')
        time_fmt_string = '%Y-%m-%d %H:%M:%S'
        if not time_offset :
            time_offset = time.strptime(time_string, time_fmt_string)
            print time_offset
        t = time.strptime(time_string, time_fmt_string)
        # print time
        # print time.mktime(time_offset), time.mktime(t)
        xs.append(time.mktime(t) - time.mktime(time_offset))

    return (xs, ys)

def plot_cdf(xs, linestyle, color, **kwargs) :
    d = collections.defaultdict(float)
    for x in xs :
        d[x] += 1

    xs = sort(d.keys())
    ys1 = [d[x] for x in xs]
    ys1 = np.array(ys1)
    ys1 /= np.sum(ys1)
    cys1 = np.cumsum(ys1)

    # Reduce the number of data points to create a more readable
    # graph. logspace distributes the data points evenly on a
    # logarithmic scale.
    # idx = np.logspace(0, np.log10(len(xs)), 20).astype('int')-1
    # xs = operator.itemgetter(idx)(np.array(xs))
    # cys1 = operator.itemgetter(idx)(np.array(cys1))

    plt.plot(xs, cys1, linestyle, color=color, **kwargs)

def pcentile(vs, q) :
    vs.sort()
    idx = int(len(vs)*(q/100))
    return vs[idx]

def create_latency_cdf(storage) :

    fig = plt.figure(figsize=(width, height))
    ax1 = fig.add_subplot(111)

    experiment = '20131031182553'
    if len(sys.argv) > 1 :
        experiment = sys.argv[1]

    log_fns = map(lambda (disk,resume) : '%s-access_log_%03d_resume_%s_%s_%s'%(experiment, 0, resume, disk, storage), attr_order)

    marker = ['', '', '', '']
    linestyle = ['-', '--', '--', '-']
    # annotate = {'hdd/eager'   : [(30, 0.9), (0,-20)],
    #             'hdd/lazybg'  : [(6,0.85), (0,-20)],
    #             'ssd/eager'   : [(2,0.8), (-20,20)],
    #             'ssd/lazybg'  : [(2.5,0.75), (-35,10)]}
    percentiles = {}
    for (i, log) in enumerate(log_fns) :
        (xs, ys) = parse_access_log(log)
        ys = map(lambda x : x/1000., ys)
        print 'min/max/avg', np.min(ys), np.max(ys), np.mean(ys)
        # print '99th/95th/90/50th percentile', [np.percentile(ys, x) for x in [99,95,90,50]]
        # print '99th/95th/90th percentile', [pcentile(ys, x) for x in [99,95,90]]
        percentiles[attr_order[i]] = [np.percentile(ys, x) for x in [50,90,95,99]]
        qemu = attr_order[i][1]

        plot_cdf(ys, linestyle=linestyle[i], color=colors[qemu],
                 marker=marker[i], markersize=2.0)
        
        # label = '/'.join(attr_order[i])
        # (xy, xytext) = annotate[label]
        # plt.annotate(label, xy=xy, arrowprops=dict(arrowstyle="->",linewidth=0.5),
        #              textcoords='offset points', xytext=xytext)

        # print ys
        # plt.annotate('90/95/99th: %.0f/%.0f/%.0f'%(np.percentile(ys, 90.),
        #                                            np.percentile(ys, 95.),
        #                                            np.percentile(ys, 99.)), xy=(1,0.5-i*0.1)) 

    print storage
    for i in range(len(attr_order)) :
        print '/'.join(attr_order[i]), '&', ' & '.join(map(lambda d : '%.1f'%(d), percentiles[attr_order[i]])), '\\\\'


    # http://matplotlib.sourceforge.net/mpl_examples/pylab_examples/spine_placement_demo.py
    for loc, spine in ax1.spines.iteritems():
        if loc in ['left','bottom']:
            pass
        elif loc in ['right','top']:
            spine.set_color('none') # don't draw spine
        else:
            raise ValueError('unknown spine location: %s'%loc)

    # turn off ticks where there is no spine
    ax1.xaxis.set_ticks_position('bottom')
    ax1.yaxis.set_ticks_position('left')
    ax1.set_ylim(0, 1)
    # ax1.set_xlim(-3, 10)
    ax1.set_xscale('log')

    plt.xlabel('Latency [ms]')
    plt.ylabel('CDF')

    # Re-label lazybg to lazy. Reduces confusion for the reader. Also
    # reduces the plot number down to 4 (from 6).
    legend_text = [disk+'/'+resume for (disk, resume) in attr_order]
    legend_text = map(lambda s : s.replace('lazybg', 'lazy'), legend_text)
    plt.legend(legend_text,
               loc='lower right', numpoints=1)

    leg = plt.gca().get_legend()
    leg.draw_frame(False)
    ltext = leg.get_texts()
    plt.setp(ltext, fontsize=fontsize)
    plt.setp(leg.get_frame(), linewidth=0.5)

    plt.savefig('%s-latency-%s-cdf'%(experiment, storage))

    # plot(X, Y)
    # plot(X,CY,'r--')


# create_time_series_plot()
# create_latency_histogram()
create_latency_cdf('direct')
# create_latency_cdf('nbd')
