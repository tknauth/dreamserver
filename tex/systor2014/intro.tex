\section{Introduction}

%% What is the problem?
Cloud computing, i.e., renting IT resources for indefinite time spans, offers many benefits.
The most touted of which is probably the flexibility to add and release resources at will based on ones computing needs.
However, we posit that current cloud provider offerings are ill-suited for lower-end customers,
especially considering user-facing, interactive services with frequent idle periods.
Cloud providers should allow resources to be acquired and released even for time frames of a couple of minutes.
While the current practice of cloud providers is to charge users by the hour,
there is no fundamental technical reason preventing the adoption of more fine grained billing cycles of minutes or even seconds.

%% \cite{das2010litegreen}

%% Why is it interesting and important?
{\sn} demonstrates that just-in-time deployment of stateful virtual machines,
sometimes in less than one second,
is possible with today's technologies.
This allows lower-end customers to deploy their services ``in the cloud'' without requiring the services to run continuously.
Temporarily switching off idle services has benefits for the cloud provider and customer:
the cloud provider saves resources while the service is off,
requiring fewer physical machines to host services,
possibly saving money by powering down physical servers to save power.
On the other side, the customer saves money because he is not billed for periods when his service is suspended.

%% The request distribution of our target services have frequent inter-arrival times of several minutes.
%% Disabling the services during those idle periods would allow the customer to save money by cutting costs.
%% On the provider side, it may enable more flexible scheduling as resources are explicitly transitioned between on and off states.

%% Why hasn't it been solved before? (Or, what's wrong with previous
%% proposed solutions? How does mine differ?)
The key challenge is to reactivate services quickly in response to incoming requests.
As we expect humans to interact with these services,
fast response times are essential.
Currently, it takes several minutes to start a new instance on, e.g., Amazon EC2,
while resuming from a checkpoint is not even supported by Amazon's API.
However, even existing implementations to resume execution from a checkpoint suffer a high initial delay.
This delay grows linearly with the checkpoint size, reaching into tens of seconds.
%% We build on previous work~\cite{zhang2013esx, zhang2011workingset} to cut resume times to a minimum.
{\sn} achieves fast VM resurrection by applying results from previous work~\cite{zhang2013esx, zhang2011workingset, knauth2013fast-resume, zhu2011twinkle}.

%% What are the key components of my approach and results? Also
%% include any specific limitations.
With {\sn}, we propose a system architecture that supports the on-demand deployment of virtualized services.
{\sn} suspends idle virtual machines and only resurrects them on the next incoming request.
This is completely transparent to the request issuer, i.e., the client,
except for a slight latency increase on the initial request.
Applying existing techniques and adapting them to the open-source virtual machine emulator qemu/kvm,
{\sn} resumes stateful VMs in less than one second from local storage,
and in slightly more than one second from remote storage.

%% To this end, we modified existing open-source components, Apache and qemu/kvm,
%% to support the on-demand provisioning of virtual machines.
%% Our Apache proxy is aware of the backend server's on/off state.
%% The proxy enables the backend if a request for a suspended backend server arrives.
%% Resuming an entire operating system virtual machine within a few seconds is possible by adapting qemu's default resume implementation.
%% We implemented a \emph{lazy resume} mechanism that drastically cuts the time to reactivate a suspended virtual machine;
%% sometimes as low as 1.1 seconds.

%% \begin{enumerate}
%% \item When to transition service into idle state?
%% Deterministic timeout or prediction based?
%% \item Architecture: load balancer keeps state of which instance is up or down.
%% \item How fast can we resume service from idle state?
%% \item How much money/energy can we save?
%% \end{enumerate}

In summary, our contributions are as follows:
\begin{itemize}
\item We motivate the need and potential of truly on-demand cloud services where services are suspended even for minute-long idle intervals (Section~\ref{sec:problem}).
\item We describe our lazy virtual machine resume implementation, VM resume set estimator, and suspend-aware HTTP proxy (Section~\ref{sec:architecture}).
\item We evaluate the speed with which we can resurrect virtual machines in a variety of settings,
including seven different applications, storage technologies and locations, as well as different VM resume techniques (Section~\ref{sec:eval}).
\item We share all code and data with the scientific community at \url{http://bitbucket.org/tknauth/dreamserver}.
\end{itemize}
