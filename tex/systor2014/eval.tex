\section{Evaluation}
\label{sec:eval}

The evaluation examines three techniques to resume a virtual machine:
(a)~eager resume, (b)~lazy resume, and (c)~hybrid resume.
We focus on empirically determining the following characteristics of our {\sn} prototype.

\begin{enumerate}
\item How does the checkpoint size affect the resume time?
\item How fast can a VM respond to the initial HTTP request when woken up from resume?
How large is the difference between the three resume strategies?
\item How does lazy resume affect the guest's overall execution performance?
Answering the initial request quickly is important,
but so is the continued execution performance after the resume.
\item We compare different storage techniques, HDD vs. SSD,
and storage locations, direct attached vs. networked,
with respect to their impact on the resume time.
\item What is the net benefit of suspending VMs?
We analytically determine the reduction in running VMs based on the workload traces used to motivate our study.
\end{enumerate}

%% Before we present our results,
%% we describe the hardware and software setup used during the experiments.

\begin{figure}
  \includegraphics{figs/20140212T120002-time-to-first-request-direct-annotated}
  \caption{Time to answer the first HTTP request.
Lower is better.
The VM hosts a nginx/django server and uses locally attached storage.
Eager resume times increase linearly with checkpoint size
while lazy resume times stay constant.}
  \label{fig:resumetime-vs-statesize-direct}
\end{figure}

\subsection{Hardware and Software Setup}

Our virtual machine host has a 4-core AMD Phenom II processor, 12~GB of main memory, and two hard drives.
One drive is a 120~GB SSD from Intel (model SSDSC2CT12),
while the second drive is a conventional 2~TB spinning disk from Samsung (model HD204UI).
The host operating system is Ubuntu 12.04.03,
while the guests run a variant of Ubuntu 12.04 adapted for virtualized environments.
In addition to the VM host, we have a second machine with a 6-core AMD Phenom II processor, 8~GB RAM,
and the same hard drive models.
The second machine serves as the networked storage target for experiments which involve networked storage.
The two machines are connected via a single switched gigabit Ethernet line.

Some measurements involve a third machine,
which hosts the reverse-proxy (cf. Figure~\ref{fig:arch}) and runs the RUBiS workload generator.
This setup most closely resembles the overall {\sn} architecture.
%% Measuring the time it takes the requests to pass through the proxy on their way to the backend server gives the complete end-to-end latency.
%% However, as the time to successfully answer the request is dominated by the time it takes to resume the instance,
%% most of the measurements do not make use of the full setup.
We use the reverse-proxy setup only to measure the impact of lazy resume in the VM's continued execution performance (Section~\ref{sec:effect-on-continued-execution}).
Otherwise, we issue resume commands and measure request latency directly on the virtual machine host.
%% The network latency added by going through the reverse proxy is in the range of single digit milliseconds,
%% while the resume time is measured in seconds.

%% \subsection{Software Setup}

%% virtual machine configuration
In total we use seven different web application VMs for our evaluation.
Each VM runs Ubuntu 12.04 tailored for virtual environments.
One VM has an nginx/django setup that allows us to control the VM's memory consumption for experiments where this is required.
Another VM contains the full RUBiS software stack, i.e.,
a MySQL database, PHP, and an Apache web server.
RUBiS~\cite{rubis} is a web application, modelled after eBay,
where users can buy and sell items.
The RUBiS server-side components are complemented by a client-side load generator.
Using a complete web application, instead of only static content,
gives us a more realistic picture of the expected performance.
A third VM, prepared by us, contains a Mediawiki into which we imported a dump of the Spanish Wikipedia.

In addition to the VMs prepared by us,
we also downloaded four additional,
representative appliances from Bitnami~\footnote{\url{http://bitnami.com/}}.
%% Bitnami's business is to bundle and configure popular open-source software packages into VM images.
%% Users download the images, sometimes called bundles or appliances,
%% from Bitnami and deploy them on their own infrastructure.
%% Very little technical knowledge is required to setup a private instance of, e.g.,
%% the well-known blogging platform Wordpress.
Out of the hundreds of appliances offered by Bitnami,
we chose the following four:
(1)~Wordpress, a popular blogging platform, % Wordpress version 3.6.1-2
(2)~Trac, a lean software project management environment, including a bug tracker, wiki, and ticketing system, % (version 1.0.1-1)
(3)~OwnCloud, a data synchronization, file sharing, and cloud storage web application, and % (version 5.0.12-1)
(4)~Jenkins, a continuous integration framework. % (version 1.533)

\begin{figure}
  \includegraphics{figs/20131031141316-latency-direct-cdf-annotated}
  \caption{Latency distribution for different resume strategies and storage media.
The latency distributions are very similar and visually almost indistinguishable.
}
  \label{fig:rubis-latency-cdf}
\end{figure}

%% Each appliance is distributed in VMware's native virtual machine disk format~(vmdk).
%% We converted it to qemu's qcow2 format before running our benchmarks.
%% Additionally, we installed an acpid daemon to be able to shutdown the virtual machines using libvirt.
%% In the compressed qcow2 format, the VM disks take up between 1.1~GB (Trac) and 1.5~GB (Jenkins).

The resume times shown here represent the average over ten runs.
Each run is executed with a cold buffer cache on the VM host, i.e.,
all the checkpointed data is read from disk.
For scenarios involving remote storage,
we also clear the cache of the remote storage target.
Instead of wiping the host's cache entirely,
we re-mount the partition, which stores the guest's virtual disk as well as the memory checkpoint.
The unmount/mount cycle only clears VM-related data,
preserving the qemu binary and libraries.

%% \begin{itemize}
%% \item We clear the cache by unmounting and mounting the partition (as opposed to echo 1/2/3 | tee /proc/sys/.../drop\_cache)
%% \item the guest disk and memory image are stored on the same disk/partition (and are also flushed)
%% \end{itemize}

%% \begin{figure}
%%   \includegraphics{figs/20140212T120002-time-to-first-request-nbd}
%%   \caption{Time to answer the first HTTP request.
%% The VM hosts a Django/nginx server.
%% The checkpoint and guest disk are stored on the network.
%% Resume times are slightly higher than for locally attached storage.
%% %% Lazy resume is independent of checkpoint size.
%% }
%%   \label{fig:resumetime-vs-statesize-remote-nbd}
%% \end{figure}

\subsection{Time to First HTTP Response}
\label{sec:eval:django-direct}

We start our evaluation by establishing a baseline for how fast we can re-activate suspended services.
To this end, we measure how much time passes between triggering the resume and receiving the first reply.
Figure~\ref{fig:resumetime-vs-statesize-direct} illustrates the relationship between resume time and checkpoint size.
The resumed VM runs a nginx/django service.
We observe two things:
first, with lazy resume the time to answer an HTTP request stays approximately the same for growing checkpoint sizes,
while eager resume takes time proportional to the checkpoint size.
The hybrid strategy also has a resume time independent of the checkpoint's size.
We omitted hybrid resume from the figure for better readability.
Second, solid state drives offer a definite advantage over spinning disks in this scenario.
The main advantage of SSDs is their capability to complete significantly more I/O operations per second~(IOPS).
While spinning disks may complete hundreds of IOPS,
solid state disks offer tens of thousands of IOPS.
For example, the data sheet of the SSD we use for our evaluation lists 25000 random reads per second~\footnote{\url{http://ark.intel.com/products/67287/Intel-SSD-330-Series-120GB-SATA-6Gbs-25nm-MLC}}.
With 60 random 4~KiB reads for the HDD~\footnote{\url{http://www.storagereview.com/samsung_spinpoint_f4eg_review_hd204ui}} that is a difference of more than two orders of magnitude.
Lazy resume benefits from the higher IOPS count,
because the previously sequential I/O pattern of eager resume is replaced by an I/O pattern with significantly less spatial locality.
In addition, the SSD's higher sequential read bandwidth is apparent in the graph.
The \emph{hdd/eager} curve has a steeper slope than the \emph{ssd/eager} curve,
indicating that the resume time increases faster for hdd/eager than it does for ssd/eager with larger checkpoints.
For example, it takes 8.6 seconds to resume a 1.5~GB checkpoint stored on an SSD.
Restoring an equivalently sized checkpoint from HDD takes 15.3 seconds.

We argue that storing memory checkpoints on SSD is a perfect example how SSDs can be selectively deployed within the data center.
Unconditionally replacing HDDs with SSDs is very costly as the capacity/price ratio is still much more favorable for traditional spinning disks.
However, even a single 128~GB SSD provides enough storage to hold the memory image of 32 virtual machines with 4~GB per VM.
We expect further resume time improvements by using PCIe-based SSDs instead of the more common SATA drives,
although we have not done any measurements yet.

%% It takes less time to sequentially read a checkpoint from SSD than from HDD because of the SSD's higher throughput.
%% Again, based on the spec sheet, the SSD offers 500 MB/s in sequential read speed,
%% compared to 140 MB/s.
%% The time to resume depends on other factors too,
%% which is why it still takes 9.5 seconds to resume from a checkpoint sized 1.5~GB and stored on an SSD.

%% {\color{red}{\bf Check if Fig 5 uses lazy or hybrid!}}

%% \begin{figure}
%%   \includegraphics{figs/20131031141316-latency-nbd-cdf}
%%   \caption{Latency distribution for RUBiS workload.
%% Checkpoint and VM disk are accessed over the network.
%% Lazy resume negatively affects the guest's performance.}
%%   \label{fig:rubis-latency-cdf-nbd}
%% \end{figure}

\subsection{Effect on Continued Execution}
\label{sec:effect-on-continued-execution}

The reduction in the time to answer the first HTTP request demonstrates an important benefit of lazy resume.
The improvement is enabled by deferring work, reading data from disk, to the future.
Instead of a large penalty at the beginning, there are many smaller penalties distributed over a larger time frame.
The smaller penalties are page faults occurring whenever the guest accesses a region of its memory that has not been read from disk yet.

To gauge the prolonged effect of lazily resuming guests,
we measured HTTP request latencies over longer time frames (minutes).
We resumed a guest running the RUBiS application and subjected it to 7 minutes of traffic generated by RUBiS' own workload generator.
The generator models the typical behavior of users on an auction web site, e.g.,
browse items, post comments, and bid for items.
We repeated this experiment for eager and lazy resume from HDD and SSD.
%% Figure~\ref{fig:rubis-latency-cdf} illustrates the latency distribution for each.
%% Lazy resume effectively reduces the time to resume a virtual machine.
%% Instead of reading the entire guest's memory before starting the execution,
%% only the most crucial parts are.
%% The remaining in-memory state is read piecemeal and on-demand; stretched out over time, or maybe never.
%% The resume penalty, which eager resume pays at the beginning of resuming a VM,
%% is stretched over time for lazy resume.
%% Instead of paying a huge penalty once,
%% lazy resume trades this for many smaller penalties over time.

%% To measure the effect of continued page faults as the guest executes after a resume,
%% we used the following experimental setup:
%% We start our RUBiS virtual machine and subject it to one run of the workload generator.
%% This makes sure that the VM's page cache is warmed up.
%% Afterward, we suspend the virtual machine, clear the host's cache, and resume the VM.
%% A second run of the RUBiS workload generator follows,
%% for which we measure the request latency for every single HTTP request.
%% The request latency is recorded by the reverse proxy,
%% as opposed to the workload generator.

%% hdd/eager & 1.5 & 21.9 & 65.2 & 11630.0 \\
%% hdd/lazy & 11.3 & 187.8 & 6050.4 & 52847.0 \\
%% hdd/lazybg & 1.6 & 11.1 & 16.4 & 173.0 \\
%% ssd/eager & 1.4 & 6.9 & 11.6 & 34.5 \\
%% ssd/lazy & 1.6 & 7.6 & 11.9 & 41.6 \\
%% ssd/lazybg & 1.6 & 7.8 & 12.0 & 42.0 \\

As can be seen in Figure~\ref{fig:rubis-latency-cdf},
the latency distribution shows little divergence between the two resume strategies.
%% The 95th percentile latency for hdd/eager is 65~ms, compared to 6000~ms for hdd/lazy, i.e.,
%% an increase of two orders of magnitude.
The reason why eager and lazy resume perform similar is that lazy resume employs a background thread to pre-fault guest pages before they are accessed by the VM itself.
The pre-faulting access pages sequentially to maximize I/O throughput.
This improves the guest's performance because a page is already present in memory when a guest eventually accesses it.
Even the different storage technologies do not have a large impact on the latency distribution,
i.e., the curves for HDD and SSD also look almost identical.
This indicates that an initially higher latency due to the resume process is hardly noticeable when looking at the overall latency distribution.
The latency distribution of hybrid resume is very similar to lazy resume,
which is why we did not include it in the figure.

%% Figure~\ref{fig:rubis-latency-cdf} shows four cumulative distribution functions~(CDF) of response latencies for the above setup.
%% We show one CDF for each of the combinations of eager/lazy resume and HDD/SSD backed VMs.
%% {\color{red}
%% The experimental data confirms our intuition about the impact of lazy resume on the continued execution.
%% The curves for eager resume (hdd/eager and ssd/eager) show a slightly better latency distribution than for lazy resume,
%% especially so for latencies above 10 milliseconds.
%% In the 10~ms to 200~ms range, eagerly resuming from SSD shows the best latency distribution, i.e.,
%% the CDF curve ``is ahead'' of all the others;
%% followed by eagerly resuming from HDD.
%% Third comes lazily resuming from SSD,
%% while lazily resuming from HDD has the least favorable latency distribution.
%% This perception is confirmed when we look at the $n$th percentile latency which we list in Table~\ref{tab:percentile-rubis-hdd}.

%% As noted previously by others too,
%% changes and optimizations to a system typically have a greater affect on the tail latency,
%% for better or worse, than they do on the average.
%% This also seems to be the case here.

%% While the effect of lazy resume is visible in our measurements,
%% we argue that it is within acceptable bounds.
%% Studies have shown that end-user perceived latencies when interacting with web services routinely exceed one second or more~\cite{meenan2013howfast}.
%% A few additional milliseconds will hardly matter in these cases.
%% \texttt{TODO: plot latency of VM with cold cache.
%% would be good to demonstrate benefit of start vs resume guest.}
%% }

\subsection{Resume from Remote Storage}

When it comes to designing a compute infrastructure, the architect has to choose between direct attached or remote storage.
Direct attached storage~(DAS) has the benefit of typically being cheaper than remote storage solutions.
No additional network capacity or infrastructure must be allocated to provide persistent storage.
In the context of virtual machines and their migration, however,
a centralized storage pool makes VM migrations easier~\cite{bradford2007live}.
Instead of transferring (potentially) hundreds of gigabytes of on-disk data between the migration source and destination,
only the comparatively small in-memory state must be copied.
The reduced migration overhead stemming from a centralized storage architecture allows to migrate more often and at a lower cost.

%% \begin{table}
%%   \begin{center}
%%     \begin{tabular}{r|r|r|r|r}
%% Percentile & 50 & 90 & 95 & 99\\
%%            & [ms] & [ms] & [ms] & [ms]\\
%%       \hline
%% hdd/eager & 1.4 & 6.9 & 11.8 & 44.8 \\
%% hdd/lazy & 11.1 & 198.9 & 6106.1 & 39925.9 \\
%% hdd/lazybg & 1.6 & 8.2 & 12.7 & 66.5 \\
%% ssd/eager & 1.4 & 6.9 & 11.7 & 43.7 \\
%% ssd/lazy & 1.6 & 7.8 & 12.2 & 44.0 \\
%% ssd/lazybg & 1.6 & 9.7 & 13.3 & 66.4 \\
%%     \end{tabular}
%%   \end{center}
%%   \caption{Latency percentiles for different resume strategies when resuming over the network.}
%%   \label{tab:percentile-rubis-hdd}
%% \end{table}

Due to the attractive properties of a centralized storage pool,
we also investigate resuming a VM over the network.
Among the many remote storage technologies available,
e.g., AoE, iSCSI, and nbd,
we chose the latter for our evaluation.
The network block device~(nbd) was introduced into the Linux kernel in 1998 and is considered a mature technology.
%% One noteworthy fact about nbd is its use of TCP as a transport protocol.
%% Lighter alternatives exist, e.g., AoE uses plain Ethernet frames,
%% but we do not expect the higher packet processing overhead to noticeably impact our results.

We repeated the same set of experiments as described in Section~\ref{sec:eval:django-direct}.
The only difference is in how the VMs access their data and checkpoint image.
%% As can be seen in Figure~\ref{fig:resumetime-vs-statesize-remote-nbd},
%% resuming our nginx/Django service takes 1.3 seconds in the best case, i.e.,
%% when the state is stored on SSD.
While we omitted a figure akin to Figure~\ref{fig:resumetime-vs-statesize-direct} due to space constraints,
we find that resume times increase across the board as we move from directly attached disks to networked storage.
This was expected because of the additional hardware- and software components introduced into the setup.
We also notice that resuming large checkpoints, e.g., 1.5~GB,
takes proportionally longer over the network because the networking equipment caps the bandwidth at 1 gigabit per second.
With directly attached disks, the throughput, even for spinning disks,
was higher than what a single gigabit link is able to transfer.
This is also the reason why the eager resume curves diverge less with increasing checkpoint sizes than they did for directly attached storage.
At 1.5~GB, the difference between eagerly resuming from HDD and SSD is only 2.2 seconds,
compared to 6.7 seconds for directly attached disks.
Relatively speaking, lazy resume reduces the resume time by roughly the same factor for networked and direct storage:
resuming from a 1.5~GB checkpoint, lazy resume from remote HDD responds 2.5x faster compared to eager resume.
On SSD, lazy resume is 7x faster than eager.
For local storage, the factors are 2.2x and 7.1x.

%% Looking at the continued execution performance in Figure~\ref{fig:rubis-latency-cdf-nbd}, we notice that a lazy resume from HDD greatly increases the request processing latency.
%% The other strategies all show a similar performance.
%% The 99th percentile latency for all strategies except hdd/lazy is around 50~ms.
%% This is even better than for the hdd/eager and hdd/lazybg with direct attached storage.
%% We attribute this to the fact that the nbd server does a better job at buffering reads and writes than Linux for directly attached storage.
%% Confirming this assumption would need a more thorough investigation.

\newpage
\subsection{Resuming Appliances}

So far, our evaluation focused exclusively on two out of seven applications, RUBiS and nginx/django.
We now present data for the entire range of applications.
As before, we vary the resume strategy, storage technology and location.
We start by looking at the difference between resuming from local HDD and SSD.

\begin{figure}
  \includegraphics{figs/20140404T103121-resumetime-bar-hdd-direct}
  \caption{Resume times for seven applications from local HDD.
Lower is better. Error bars show one standard deviation.}
  \label{fig:resumetime-bar-hdd-direct}
\end{figure}

%% Although web applications typically consist of three components,
%% web server, application logic, and database,
%% there is enough variety to warrant a look at other applications.
%% In addition to RUBiS, we investigated the following four applications:
%% Wordpress, OwnCloud, Trac, and Jenkins.
%% The measurements show the time passed between resuming the application and receiving a successful answer for the login page.
%% The four applications are more demanding than our previous RUBiS example in the sense that we now also look at the difference between the two resume strategies presented in the implementation section.
%% We present results for the default eager resume, lazy resume, and lazy resume set (\verb+hybrid+) resume.
%% Lazy resume set resume reads the guest's working set eagerly,
%% while the non-resume set pages are read lazily.
%% As will become apparent shortly,
%% resuming the appliances lazily from HDD actually prolongs the resume time compared to eager resume.
%% As before, we evaluate different disk types (HDD/SSD) and connection technologies (direct/networked storage).

%% Before we discuss how lazy resume compares to eager resume,
%% let us first look at the baseline resume time for each appliance.
Figure~\ref{fig:resumetime-bar-hdd-direct} shows the times to resume from local HDD.
%% The difference between the appliances is already apparent:
We can make a number of observations:
first, pure lazy resume from HDD is always slower than eager resume.
However, the measurements were done with fresh instances which had a small memory footprint.
Referring back to Figure~\ref{fig:resumetime-vs-statesize-direct},
we see that an eager resume can take up to 15 seconds or more.
Hence, eager resume will only be faster than pure lazy resume for small checkpoint sizes up to a few hundred megabytes.
The hybrid resume decreases the initial delay for some applications, e.g., Django, Jenkins, and Rubis,
while for other applications, e.g., Trac, it has no positive effect.
Again, this must be viewed in light of the comparatively small checkpoint size.
The VM checkpoints vary between 230~MB for Trac, to 490~MB for Jenkins,
whereas a long running VM accumulates more state, up to the VM's configured maximum memory size, e.g., 2~GB.
The eager resume times for a 2~GB checkpoint in our setup is over 20 seconds.
Compared to this baseline, lazy and hybrid resume for any of the applications are faster than the 20 second baseline.

%% Switching from eager to lazy resume on HDDs, in Figure~\ref{fig:resumetime-bar-hdd-direct}, we see
%% that it actually takes longer to resume.
%% Although lazy resume drastically reduces the volume of data that must be read from disk,
%% the spatial non-locality of the data reverses the gains we achieve by reducing the volume.
%% A naive lazy resume more than doubles the resume time for the Jenkins, OwnCloud, and Wordpress appliance,
%% clearly not the desired result.

Among the applications, Django resumes fastest from local HDD with an average resume time of 1.6 seconds.
While this is an improvement over the eager resume baseline and might be an acceptable initial delay,
there is still room for improvement.
By using SSDs instead of HDDs the resume times, even for a pure lazy resume,
drop below those of an eager resume.
Figure~\ref{fig:resumetime-bar-ssd-direct} shows eager resume times between 3.0 and 4.0 seconds,
compared to lazy resume times of 0.7 and 2.2 seconds.
When combining local SSDs with hybrid resume,
we are able to push the resume time below one second, for the Django, Jenkins, and Rubis applications.
Looking at the difference between lazy and hybrid,
it becomes apparent that even SSDs benefit from sequential access:
hybrid is faster than lazy resume across all applications.

With SSDs we are getting very close to what is possibly achievable with qemu at the moment.
Even with a hot cache, i.e., the entire checkpoint is in memory before resuming the VM,
we have a consistent initial delay between 0.5 and 0.6 seconds.
Combining hybrid resume with flash-based storage we achieve almost optimal resume times for three appliances:
Django, Jenkins, and Rubis.

%% However, naive lazy resume from SSD (Figure~\ref{fig:resumetime-bar-ssd-direct}) decreases the resume time for each appliance.
%% For example, the previous three appliances, which saw the highest increase for lazy resume,
%% have their resume times cut by almost 40\%.
%% In other cases, e.g., RUBiS, a naive resume from SSD only decreases the resume time marginally by 0.3 seconds.

\begin{figure}
  \includegraphics{figs/20140404T103121-resumetime-bar-ssd-direct}
  \caption{Resume times for seven applications from local SSD.
Lower is better. Error bars show one standard deviation.}
  \label{fig:resumetime-bar-ssd-direct}
\end{figure}

%% However, even SSDs benefit from sequential access.
%% With hybrid resume (\verb+hybrid+), we combined the best of both eager and lazy resume.
%% Lazy resume set resume improves the initial response time for each appliance regardless of the disk type.
%% On spinning disks, the reduction varies between 20\% for Jenkins (4.7 seconds),
%% up to 57\% for RUBiS (1.9 seconds).
%% For appliances on SSD, the hybrid resume drops by up to 60\%,
%% e.g., resuming Wordpress drops from 4.8 seconds to 1.7 seconds.

%% \begin{itemize}
%% \item measured resume set size; working set size also determines how
%%   by how much the checkpoint size increases. That information is
%%   basically stored twice.
%% \end{itemize}

Moving from local to remote storage the resulting response times are higher but analogous compared to resuming from local storage.
The extra hard- and software components introduce initial delay.
Also, the gigabit LAN interface limits the throughput when resuming from SSD,
i.e., the local SSD connected via SATA offers a higher throughput than can be effectively transmitted over gigabit Ethernet.
Spinning disks impede pure lazy resume because of their low IOPS count.
The hybrid resume times from remote HDD are between 2.3 seconds for Django and 5.9 seconds for Mediawiki (Figure~\ref{fig:resumetime-bar-hdd-nbd}).
Remote SSDs are much better, resulting in hybrid resume times between 1.0 seconds for Django and 2.8 seconds for Mediawiki (Figure~\ref{fig:resumetime-bar-ssd-nbd}).
We consider this an acceptable delay considering that a stateful VM was resurrected.

In this context we want to point to a recent article comparing the speed of different web sites~\cite{meenan2013howfast}.
According to the article, 50\% of page loads take 2 seconds and longer to complete, i.e.,
page load times on the order of a few seconds are still common enough to not irritate users or be considered abnormal.
We are also aware of studies between the relationship of page load times and lost revenue for online retailers.
However, we explicitly do not advocate to employ the techniques presented here in such a scenario.
We set our target on lower-end cloud customers,
where infrequent access warrants the shutdown of VMs to save resources and money.
If ones business model depends on a speedy browsing experience,
it is reasonable to pay more than customers with more flexible requirements.

%% The results for resuming from spinning disks are shown in Figure~\ref{fig:resumetime-bar-hdd-nbd}.
%% Compared to the resume times for locally attached spinning disks,
%% the resume times are longer across all appliances and resume strategies.
%% The remote storage adds between 0.7 (OwnCloud) and 0.9 seconds (Jenkins) to the resume time.
%% As noticed before, switching to a strategy where the entire memory is read lazily
%% actually increases the resume time.
%% This negative effect is only amplified by remotely attached storage.
%% All appliances take longer to resume lazily compared to qemu's standard eager resume.
%% The Jenkins appliance experiences the worst case increase among the five investigated appliances.
%% Its resume time increases from 6.8 seconds to 16.9 seconds,
%% demonstrating that a simple lazy resume has a detrimental effect.
%% However, even in the best case scenario, Trac,
%% the resume time still increases by 1.7 seconds to 7.0 seconds.

%% {\color{red}Text about hybrid resume.
%% Must somehow incorporate the fact that resume set resume shows variable performance.
%% First resume is slow, second resume slowish, etc.
%% Stabilizes after 3-4 suspend/resume cycles.
%% Which numbers do we show? Average? Minimum? Range?
%% }

%% Switching to \verb+hybrid+ positively impacts the resume time.
%% VMs resume faster with the lazy resume set strategy than they do with eager resume.
%% The best improvement, in relative terms is for the RUBiS guest,
%% RUBiS benefits most, in relative terms:
%% its time to resume is more than halved from 5.3 seconds (eager) to 2.5 seconds (\verb+hybrid+).
%% As we use average values over five runs, it hides the fact that the \verb+hybrid+ strategy actually improves over time.
%% The resume set estimation improves over time
%% as more pages are added to the resume set.
%% For example, while the initial resume time for Jenkins is 9.7 seconds for hdd/nbd,
%% subsequent resumes finish in less than 5 seconds.

\begin{figure}
  \includegraphics{figs/20140404T103121-resumetime-bar-hdd-nbd}
  \caption{Resume times for seven applications from remote HDD over a gigabit connection. 
Lower is better. Error bars show one standard deviation.}
  \label{fig:resumetime-bar-hdd-nbd}
\end{figure}

\subsection{Savings Potential of Suspending VMs}

We finish the evaluation by analyzing the potential of periodically suspending idle VMs.
The overall goal is to reduce the total number of running VMs.
Explicitly decommissioning idle instances either results in multiplexing a larger number of instances with the same physical hardware or the ability to turn of vacant physical servers, e.g., to save energy.
In Section~\ref{sec:problem} we already hinted at the savings potential of lower end services that motivated our work.
Figure~\ref{fig:timeout-vs-savings} illustrated the accumulated idle time of a single service over an extended time period.
Pushing this idea further, we now analytically quantify the savings across a set of VMs.
To this end, we randomly selected 80 days of traffic from the Dresden trace.
We pretend that each of the 80 days represents an independent service and is hosted in a separate VM.
We further assume that each VM instance is suspended after 60 seconds of inactivity.
The result of this analysis is represented in Figure~\ref{fig:savings}.

We visually determine that the number of active VMs averages about 40 over the course of a 24 hour period.
This is half the number of active VMs compared to a static resource provisioning scenario where VMs are constantly on.
The accumulated VM runtime over the 24 hour period is reduced by 46\% due to suspending idle VMs.
As the data underlying the analysis in Figure~\ref{fig:savings} is selected randomly,
we manually confirmed that this is not just a fluke.
We repeatedly calculated the savings for various combinations and consistently achieved savings between 40-50\% with a 60 second timeout interval.

\begin{figure}
  \includegraphics{figs/20140404T103121-resumetime-bar-ssd-nbd}
  \caption{Resume times for seven applications from remote SSD over a gigabit connection.
Lower is better. Error bars show one standard deviation.}
  \label{fig:resumetime-bar-ssd-nbd}
\end{figure}

%% One interesting question that we leave for future work to address
%% is how to best curate the resume set.
%% Pages must be evicted from the resume set eventually to keep it from growing indefinitely.
%% We have to trade-off resume set size with the time it takes to read the working set from disk.
%% Without a purging mechanism,
%% the resume set may degenerate to the point where all the guest's pages belong to its working set.
%% Heuristic resume set estimation is a well studied problem in computer science~\cite{denning1968working}.
%% Improved estimation will decrease resume times even further~\cite{jiang2005clock}.
%% We leave investigating more advanced prediction techniques for future work.

\begin{figure*}
  \includegraphics{figs/savings}
  \caption{Simulating 80 independent VMs with an idle timeout of 60 seconds.
Suspending idle instances reduces the accumulated VM runtime by 46\%.}
  \label{fig:savings}
\end{figure*}
