\section{Architecture}
\label{sec:architecture}

{\sn} depends on two central building blocks:
(1)~a suspension-aware reverse proxy as well as
(2)~a mechanism to quickly resume inactive virtualized services.
Infrastructure providers routinely use operating system level virtual machines to isolate different users from each other.
Hence, when referring to virtualized services, we mean services running inside of an OS-level virtual machine.

\subsection{Reverse-Proxy}

The reverse-proxy is the central component strategically positioned between the end user and the backend services.
A reverse-proxy, as opposed to a forward-proxy, is located close to the origin servers.
%% The reverse-proxy, for example, distributes requests among a set of equivalent backend servers which would make it a load-balancing proxy.
A common use case for a reverse-proxy is load balancing by
distributing requests among a set of functionally equivalent backend servers.

\begin{figure}
\begin{center}
  \includegraphics{figs/arch-single-column}
  \caption{{\sn} architecture.
Requests pass through a proxy (1) which is aware of the on/off state of backend servers.
If the backend server is up, the proxy forwards the request immediately (2).
Otherwise, the proxy resumes the backend (3) and waits until the server is up again before forwarding the request (2).
The entire process is transparent to the client.}
  \label{fig:arch}
\end{center}
\end{figure}

Another responsibility, often assumed by a reverse proxy,
is forwarding of requests based on the domain name.
This type of forwarding is required whenever multiple domain names share a single IP address.
This setup is, for example, common with web hosting providers.
As IPv4 addresses are a rare commodity,
the hosting provider can serve multiple customers with only a single IP address.
However, individual customers still want to use their own HTTP server.
This is where the reverse proxy helps.
The reverse proxy listens on port 80 of the shared IP address, i.e.,
it sees every request destined for any of the domains serviced by this IP address.
The proxy inspects the \emph{Host} field of the HTTP request header
to determine which backend server should receive the current request.
The proxy has a mapping of domain names to (private) backend IP addresses.
While all domains handled by the proxy share a single public IP address,
each backend server has a unique private IP address.

{\sn} extends this setup by making the proxy aware of the backend servers' states.
Instead of unconditionally forwarding the request,
the proxy ensures that a customer's backend server is up before forwarding the request.
To this end, the proxy keeps a small amount of soft state.
For each domain, the proxy tracks whether the backend server is running or suspended.
Because the additional state is soft state,
bootstrapping is easy:
the proxy has a configuration file containing all the domain names and backend IP addresses it is responsible for.
To establish a backend's state, e.g., after the proxy failed,
the proxy tries to contact all of its backend servers.
Each responding server is considered awake while the others are suspended.

%% In addition to the on/off state of backend servers,
%% the proxy also keeps tracks whether servers have been merely suspended or actually shut down.
%% This is important when it comes to reactivating the server again.
%% If the server was suspended it can be resumed,
%% skipping the potentially long initialization.
%% If, on the other hand, it was shut down the virtual machine must be started from scratch.
%% As the evaluation will show later on,
%% resuming is much faster than booting a virtual machine from scratch.
%% The swiftness of activating the backend server directly impacts the user-perceived latency.

The proxy resumes backend servers as soon as a request for a suspended backend arrives.
A separate process, independent of the proxy, suspends idle backend servers.
The suspender process runs on the same machine as the reverse proxy and uses its statistics to determine idleness.
For example, for each backend server, the proxy records the time stamp of the most recent request.
Based on time stamp and the current time, we can easily determine how long the backend has been idle.
A straightforward strategy to suspend idle servers is to wait for a certain amount of time, say 30 seconds,
before suspending the backend.
Naturally, more elaborate schemes are possible, but we do not investigate them further here.
The more aggressive the strategy is, i.e., the more frequent a backend server is deactivated,
the higher the savings.
However, more frequent suspend/resume cycles may negatively impact the service's latency and, ultimately, user experience.

%% Determining (in)activity by monitoring application-layer requests is one example,
%% although this currently restricts the applicability of our prototype to monitor services communicating over HTTP.
Monitoring requests with an application-specific proxy is one possibility to determine service (in)activity.
However, this restricts the applicability of our prototype to services communicating over HTTP.
Each protocol we would want to support would potentially require its own application-specific proxy.
Ideally, we would prefer a solution that works independently of the application layer protocol.
We are currently working on a more generic solution employing Software Defined Networking~(SDN).
With SDN, it is possible to use {\sn} in a wider range of settings, i.e., for arbitrary application layer protocols.
As an example, imagine a personal ssh server ``in the cloud'',
that is only spawned when the user actually tries to connect to it.

\subsection{Lazy Resuming of Virtual Machines}

To realize our goal of provisioning virtualized resources within only a few seconds,
we build on extensive previous work in this area~\cite{zhang2013esx, zhang2011workingset, knauth2013fast-resume, zhu2011twinkle}.
The basic idea to speed up the resume process is to selectively read only portions of the on-disk state into memory (\emph{lazy resume}).
Exactly what is read depends on the memory pages accessed as part of the VM's workload.
The alternative is to read the entire state into memory before continuing execution (\emph{eager resume}).
However, because VM checkpoints can be multiple gigabytes in size,
resuming the VM eagerly incurs a high, tens of seconds, penalty which must be avoided.
Eager and lazy resume are analogous to pre- and postcopy migration~\cite{clark2005livemigration, hines2009post},
with the distinction that during migration the VM's in-memory state is not persisted to disk.

Even though lazy resume is attractive due to its low initial overhead,
it is currently not supported by the popular open-source virtualization tool qemu.
While the ideas and techniques of lazily resuming VMs are not new,
we are, to the best of our knowledge, the first to bring lazy resume support to qemu.
Next, we detail the design and implementation of our qemu-based implementation.

%% While the ideas have been previously explored and implemented in the context of a commercial virtualization product,
%% the fruits of that work are not available to the general public.
%% Hence, we decided to adapt the ideas and implement them as part of the open source virtual machine emulator \emph{qemu}.
%% We first present some background information on how suspend/resume is implemented in qemu,
%% before we detail our modifications.

\subsubsection{Qemu Suspend and Resume}

By default, qemu implements an \emph{eager} resume strategy.
Before the guest continues execution,
its entire memory content is restored.
This requires sequentially reading the entire memory image from stable storage.
Hence, the time to eagerly resume the guest linearly depends on the speed at which the system is able to read the suspend image from disk~\cite{knauth2013fast-resume, zhang2013esx, zhang2011workingset}.

The suspend image's size depends on two factors: (1)~the guest's configured memory size,
and (2)~the memory's compressibility.
%Both dependencies are straightforward.
The more memory a guest is configured with, the longer it will take to resume,
as more data must be read from disk.
The compressibility of the image depends on the activity level of the applications running inside the guest.
qemu employs a simple strategy to decrease the suspend image's size:
during the suspend process, qemu walks the guest's entire physical address space.
For each page, typically 4~KiB, qemu checks whether the page consists of a single byte pattern.
If this is the case, qemu writes the byte pattern together with the page offset to the suspend image.
Otherwise, the entire page is stored as-is on disk.
Although the check works for all possible single byte patterns,
in our tests the most prevalent compressible pages are zero-pages,
i.e., pages only containing zeros.
Due to compression, for example, a freshly booted Ubuntu 12.04 guest only takes up between 200--300~MB on disk, even though it is configured with 2~GiB of memory.
The suspend image's compressibility, however, decreases over time as applications usually make no effort to scrub memory before releasing it.
Also, Linux aggressively uses available memory as a file system cache,
further increasing the checkpoint size.
Compressing zero-pages can be seen as a crude heuristic to detect which pages are actually used by the guest.

Resuming a virtual machine is analogous to suspending it.
Instead of walking the guest's physical address range,
qemu sequentially reads the suspend image.
Compressed pages must be ``uncompressed'' by \verb+memset()+ing the appropriate page with the byte pattern stored in the suspend image.
Uncompressed pages are simply copied from disk into memory.
An eager resume can take anywhere from a few seconds for small checkpoints up to tens of seconds for multi-gigabyte checkpoints (cf. Figure~\ref{fig:resumetime-vs-statesize-direct}).

\subsubsection{Lazy Resume in Qemu}

%% Our idea to break the linear relationship between suspend image size and resume time was to implement a \emph{lazy resume} mechanism to replace the default eager resume.
%% Instead of reading the entire memory image before resuming the execution,
%% we let the guest's memory access pattern decide which pages are important and should be read from disk.

When designing the lazy resume functionality for qemu we had two main goals:
(1)~it should be entirely in userspace, and
(2)~reuse as much functionality as possible from the Linux host OS.
The former is easy to justify for all the benefits that come from not touching the kernel, i.e.,
faster to develop, easier to debug and maintain.
The second constraint is motivated by the fact that qemu guests are just normal processes from the host operating system's point of view.
This means the guest has access to all libraries and host OS APIs as any other userspace process.

A natural candidate to implement lazy resume was to leverage Linux's \verb+mmap()+ system call.
\verb+mmap()+ allows a program to blend files, or parts thereof,
into a process's address space.
Whenever the process accesses memory within the mapped region,
the OS makes sure the corresponding memory page reflects the contents of the underlying file.
This is done either by reading the block from disk, or, by simply updating the page table entry,
if the block is already in memory, e.g., due to prefetching,

It seems like \verb+mmap()+ would be the ideal candidate for a lazy resume mechanisms.
This could be done by replacing the existing memory allocation of qemu,
with a call to \verb+mmap()+ using the VM's checkpoint as the backing file for the mapped region.
We would have to disable checkpoint compression to be able to map the checkpoint file,
but this would be acceptable.
As 99\% of the compressible pages contained zeroes,
the lack of compression could be compensated for by using a \emph{sparse file} for the checkpoint.
With sparse files, the empty, i.e., all zero, blocks/pages do not actually occupy disk space.
Sparse files are a common feature in modern file systems, such as ext3, which are frequently used on Linux.
In summary, we do not expect an increase in disk I/O activity when moving from compressed checkpoints to uncompressed sparse files.
However, we do gain the ability to use \verb+mmap()+ with the new uncompressed checkpoint file.

\subsubsection{mmap() Idiosyncrasies}

While \verb+mmap()+ allows us to implement lazy resume easily,
\verb+mmap()+ has some interesting idiosyncrasies on Linux.
Every mapping is either shared or private
where the pages of a shared mapping may be visible and accessible by multiple processes at the same time.
As the names suggest, shared mappings allow the mapping to exist in
more than one process, e.g., to be used for shared memory communication.
While we are not interested in sharing pages among multiple processes,
the ``sharedness'' also dictates how modifications to the mapped region are propagated to the backing file.

While modification to a private mapping are never automatically written to the backing file,
a shared mapping entails periodic disk updates.
The automatic propagation of updates to disk for shared mappings is useful as it essentially implements the functionality required for writing a new checkpoint.
However, we do not require \emph{periodic} updates, i.e.,
by default modifications are written to disk every 30 seconds.
Ideally, it should be possible to write out modifications only when explicitly requested, e.g.,
when suspending the guest again.
For example, the \verb+mmap()+ system call on FreeBSD has a \emph{nosync} option.
Specifying \emph{nosync} for a file-backed memory mapped region disables the periodic propagation of in-memory changes to disk.
With \emph{nosync}, the changes are only written back to disk when the mapping is dismantled or the user explicitly requests it by calling \verb+msync()+.

% A possible work around in Linux may be to declare the mapping as private and use other means to determine which pages must be written back to disk on a subsequent suspend.
% On Linux dirty information for each frame is available in user-space by examining the contents of the virtual file \verb+/proc/kpageflags+.
% However, usually only root has permissions to read this file.

Regarding the issues raised above,
our prototype uses a shared mapping because remapping of pages, covered in the next section,
is not supported for private mappings.
We cope with the potential gratuitous disk I/O, caused by periodic write-backs,
by adjusting parameters related to the operation of the virtual memory subsystem in Linux.
%% First, we set the virtual memory dirty ratio to 100\%.
%% The percentage defines the amount of dirty data a process may accumulate before a kernel thread automatically flushes data to disk in the background.
The three parameters are \emph{dirty\_ratio}, \emph{dirty\_writeback\_centisecs}, and \emph{dirty\_background\_ratio}.
By setting them to 100, 0, and 99, respectively,
we maximize the amount of dirty memory Linux allows before starting automatic writeback~\footnote{More information on the virtual memory parameters is available at \url{https://www.kernel.org/doc/Documentation/sysctl/vm.txt}}.

%% We changed the way by which qemu allocates memory for the guest's RAM with a call to \verb+mmap()+.
%% A potential limitation of \verb+mmap()+ is the linear relationship between the backing file's content and the memory mapped region.
%% That is, accessing the $n$th page of the mapped region results in reading the $n$th block from disk.
%% Due to compression, however,
%% there is no one-to-one relationship between the on-disk image and the guest's physical address space.
%% One possible solution is to disable compression.
%% Without compression, the suspend image's size is identical to the guest's configured memory size.
%% The downside of disabling compression is obvious:
%% suspending the virtual machine takes longer because more data is written to disk.
%% Furthermore, the suspend image would take up more disk space.
%% However, this need not be true.
%% Modern file systems, e.g., ext3, support \emph{sparse files}.

%% The idea behind sparse files is that their logical and actual size can be different.
%% For example, while a sparse file's size may be reported as 2~GiB,
%% it occupies significantly less than 2~GiB of actual disk space.
%% When reading a sparse file, the logical ``holes'' are filled with zeroes.
%% This aligns nicely with the observation made earlier
%% that 99\% of the compressible pages contain only zeroes.
%% Forgoing compression and utilizing sparse files,
%% we gain the ability to use \verb+mmap()+ to lazily resume the guest.

\subsubsection{Resume Set Estimation}

As \citet{zhang2013esx} noted in their prior work,
a key factor to quickly restore a guest is disk access locality.
With eager restore, the image is read sequentially from disk,
resulting in maximum I/O bandwidth.
A lazy resume, where data is read selectively on access,
will have much less spatial locality for the disk accesses.
The reduced locality and I/O performance is most visible when resuming from conventional spinning disks,
as they have a stringent limit on the number of I/O operations per second.

After the initial benchmarks involving spinning disks were rather discouraging,
we decided to abandon our pure lazy resume implementation in favor of a hybrid approach.
The hybrid approach reads the guest's resume set sequentially before starting the execution.
The remaining pages are read lazily while the guest is running.
When suspending a guest, qemu appends the resume set to the guest's image.
Our resume set estimator uses the pages accessed during the first few seconds after a resume as a prediction for the pages that will be accessed during the next resume.
Other predictors are possible, but do not fit our use case well:
\citet{zhang2011workingset} trace the VM's execution past the point when it is suspended.
First, the cloud provider would have little incentive to let the VM run longer than necessary.
The whole point is to save resources and evict the VM as quickly as possible.
Further, the decision to evict is based on the VM's inactivity.
Recording accesses during the VM's idle time will reveal only a small subset of the pages accessed during the next resume.

On resume, we map the first $n$ bytes of the checkpoint file into the qemu process,
where $n$ is the guest's memory size in bytes.
We then use the \verb+remap_file_pages()+ system call to break the linear mapping.
The pages belonging to the guest's resume set are remapped to their respective location past the first $n$ bytes.
Figure~\ref{fig:remap} illustrates this.
After remapping the resume set pages, the mapped region is still contiguous in the qemu process's address space.
% However, the pages belonging to the not necessarily contiguous resume set,
% are also contiguous on disk.
% Before starting the guest's execution we pre-fault the resume set pages,
% to make sure they are already loaded in memory.
During the remap, we sequentially read the checkpoint file's ``appendix''.
Once the VM accesses a resume set page,
this will only trigger a minor page fault because the corresponding disk block is already present in the host's buffer cache.

\begin{figure}
  \includegraphics{figs/remap}
  \caption{The VM's resume set is appended to the suspend image.
Storing the resume set out-of-band for sequential access especially benefits the I/O throughput on spinning hard disks.}
  \label{fig:remap}
\end{figure}

While qemu resumes the guest's resume set eagerly,
it reads the non-resume set pages lazily, i.e.,
only when and if they are accessed.
Lazy resume reduces the time until the guest starts executing again,
but may have a negative effect on the continued execution.
Therefore, we also added an option to trigger an eager resume of the guest's memory \emph{in the background}.
%% Pre-faulting pages in the background forces Linux to load the guest's memory image from disk.
A thread walks the guest's memory sequentially,
forcing data to be read from disk.
This way, we combine the best of both eager and lazy resume:
lazy resume reduces the time until the guest can service its first request after a resume,
while eagerly reading the image in the background reduces the impact on the guest's continued execution.


% \begin{itemize}
% \item \verb+mmap()+ MMAP\_SYNC vs. MMAP\_NOSYNC
% \item custom suspend file format
% \end{itemize}

% \begin{itemize}
% \item don't have to touch the kernel/kvm module
% \item done entirely in user-space
% \end{itemize}

% suspend time also decreases thanks to \verb+mmap()+; provide a figure detailing decreased suspend time
