#!/usr/bin/env python

from pyinotify import EventsCodes
import glob
import os
import pyinotify
import re
import shlex
import subprocess
import sys
import time

# Monitor directories /tmp/192.168.2.* for delete events. If file 'on'
# is deleted, trigger rsync of virtual machine's disk and ram file.

def parse_and_print_output(out, domain, file, duration_sec) :
    out = out.replace('\n', ' ')
    sent = re.match('.*sent (\d+) bytes.*', out).group(1)
    received = re.match('.*received (\d+) bytes.*', out).group(1)
    size = re.match('.*total size is (\d+).*', out).group(1)
    print >> open('rsync.log', 'a+'), \
        'epoch_time=%s'%(time.time()), \
        'local_time=%s'%(time.strftime('%Y-%m-%d:%H:%M:%S')), \
        'domain=%s'%(domain), \
        'file=%s'%(file), \
        'sent_bytes=%s'%(sent), \
        'received_bytes=%s'%(received), \
        'size_bytes=%s'%(size), \
        'duration_sec=%d'%(duration_sec)

def rsync_file(domain, file) :
    # cmd = 'ssh 192.168.2.2 rsync -v /home/thomas/a 192.168.2.3:~'
    # cmd = 'ssh 192.168.2.2 rsync -v %s 192.168.2.3:~'%(file)
    begin_sec = time.time()
    cmd = 'ssh 141.76.44.150 rsync -v %s 141.76.44.130:~'%(file)
    print cmd
    p = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE)
    out, err = p.communicate()
    end_sec = time.time()
    duration_sec = end_sec - begin_sec

    parse_and_print_output(out, domain, file, duration_sec)

def rsync_domain(domain) :
    files = ['/home/thomas/%s.qcow2'%(domain),
             '/var/tmp/%s-dump'%(domain)]  
    for f in files :
        rsync_file(domain, f)

class EventProcessor(pyinotify.ProcessEvent) :
    def process_IN_CREATE(self, event) :
        print 'create %s'%(os.path.join(event.path, event.name))
    def process_IN_MODIFY(self, event) :
        print 'modify %s'%(os.path.join(event.path, event.name))
    def process_IN_DELETE(self, event) :
        print 'delete %s %s'%(event.path, event.name)
        if event.name == 'on' :
            domain = event.path.split('/')[2].split(':')[0]
            rsync_domain(domain)

wm = pyinotify.WatchManager()
mask = EventsCodes.FLAG_COLLECTIONS['OP_FLAGS']['IN_DELETE']
notifier = pyinotify.Notifier(wm, EventProcessor())
for dir in glob.glob('/tmp/192.168.2.*') :
    print 'add_watch %s'%(dir)
    wdd = wm.add_watch(dir, mask, rec=True)

while True :
    try :
        notifier.process_events()
        if notifier.check_events() :
            notifier.read_events()
    except KeyboardInterrupt :
        notifier.stop()
        break
