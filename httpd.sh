#!/bin/bash

# append this to httpd.conf

for id in $(seq 1 60) ; do
        echo "<VirtualHost *:8080>"
        echo "ProxyRequests Off"
        echo "ServerName $(printf %04d $id).localdomain"
        echo "<Proxy *>"
        echo "Order deny,allow"
        echo "Allow from all"
        echo "</Proxy>"
        echo "ProxyPass / http://192.168.2.$((100+id))/"
        echo "<Location />"
        echo "ProxyPassReverse /"
        echo "</Location>"
        echo "</VirtualHost>"
done
